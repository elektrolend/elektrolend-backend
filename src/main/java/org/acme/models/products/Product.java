package org.acme.models.products;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Getter
@Setter
@NoArgsConstructor
@Entity(name = "product")
public class Product extends PanacheEntityBase {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "categoryID")
    public Category category;

    @Column(nullable = false)
    public String name;

    @Column(nullable = false)
    public String description;

    @OneToMany(mappedBy = "product")
    public List<Price> priceList = new ArrayList<>();

    @OneToMany(mappedBy = "product")
    public List<Specification> specificationList = new ArrayList<>();

    @Column(nullable = false)
    public Integer deposit;

    @Column(nullable = false)
    public Integer insurance;

    @Column
    public String imageUrl;

    @Column
    public Boolean isAvailable;

}
