package org.acme.models.products;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@EqualsAndHashCode
@Getter
@Setter
@NoArgsConstructor
@Entity(name = "specification")
public class Specification extends PanacheEntityBase {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    @Column(nullable = false)
    public String key;

    @Column(nullable = false)
    public String value;

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "productID")
    public Product product;

}
