package org.acme.models.orders;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.acme.models.products.Product;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.time.LocalDate;

@EqualsAndHashCode(callSuper = true)
@Getter
@Setter
@NoArgsConstructor
@Entity(name = "order_product")
public class OrderProduct extends PanacheEntityBase {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "officeBoxIDStart")
    public OfficeBox officeBoxStart;

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "officeBoxIDEnd")
    public OfficeBox officeBoxEnd;

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "orderID")
    public Order order;

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "productID")
    public Product product;

    @Column(nullable = false)
    public OrderProductState orderProductState;

    @Column(nullable = false)
    public LocalDate orderBeginDate;

    @Column(nullable = false)
    public LocalDate orderEndDate;

    @Column(nullable = false)
    public Integer price;

    @Column(nullable = false)
    public ServiceSecurityType serviceSecurityType;

    @Column(nullable = false)
    public String boxPassword;

}
