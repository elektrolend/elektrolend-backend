package org.acme.models.orders;

public enum ServiceSecurityType {

    INSURANCE,
    DEPOSIT

}
