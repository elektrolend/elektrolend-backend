package org.acme.models.accounts;

public enum EmployeeType {

    REGULAR,
    ADMIN

}
