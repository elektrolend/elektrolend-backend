package org.acme.mappers;

import org.acme.dto.PaginationDTO;
import org.acme.pagination.PageRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(config = QuarkusMappingConfig.class)
public interface PaginationMapper {

    @Mapping(source = "totalResults", target = "totalResults")
    PaginationDTO getPaginationDTOFromPageRequest(PageRequest pageRequest, int totalResults);

}
