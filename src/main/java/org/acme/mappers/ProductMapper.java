package org.acme.mappers;

import org.acme.dto.products.ProductCreateUpdateDTO;
import org.acme.dto.products.ProductDTO;
import org.acme.models.products.Category;
import org.acme.models.products.Price;
import org.acme.models.products.Product;
import org.acme.models.products.Specification;
import org.acme.repository.products.CategoryRepository;
import org.acme.utils.DateUtil;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Mapper(config = QuarkusMappingConfig.class)
public interface ProductMapper {

    CategoryRepository categoryRepository = new CategoryRepository();

    @Mapping(target = "categoryID", source = "product.category.id")
    @Mapping(target = "price", source = "priceList", qualifiedByName = "findLastPrice")
    @Mapping(target = "specificationsMap", source = "specificationList", qualifiedByName = "listAllSpecifications")
    ProductDTO getProductDTOFromProduct(Product product);

    @Mapping(target = "category", source = "categoryID", qualifiedByName = "fromProductCategory")
    Product getProductFromProductDTO(ProductDTO productDTO);

    @Mapping(target = "category", source = "categoryID", qualifiedByName = "fromProductCategory")
    @Mapping(target = "priceList", ignore = true)
    @Mapping(target = "id", ignore = true)
    Product getProductFromProductCreateUpdateDTO(ProductCreateUpdateDTO productCreateUpdateDTO);

    @Named("fromProductCategory")
    static Category fromProductCategory(Long id) {
        return categoryRepository.findById(id);
    }

    @Named("findLastPrice")
    static Integer findLastPrice(List<Price> prices) {
        return prices
                .stream()
                .filter(price -> DateUtil.isDateFromPastOrToday(price.beginDate))
                .sorted(Comparator.comparing(Price::getBeginDate).reversed())
                .collect(Collectors.toList())
                .stream()
                .findFirst()
                .map(price -> price.pricePerDayBrutto)
                .orElse(null);
    }

    @Named("listAllSpecifications")
    static Map<String, String> listAllSpecifications(List<Specification> specificationList) {
        return specificationList.stream()
                .collect(Collectors.toMap(Specification::getKey, Specification::getValue));
    }

}
