package org.acme.mappers;

import org.acme.dto.orders.OrderInfoDTO;
import org.acme.dto.orders.OrderProductDTO;
import org.acme.dto.orders.OrderProductStateDTO;
import org.acme.dto.orders.OrderProductWithoutOrderDTO;
import org.acme.dto.orders.UserOrderDTO;
import org.acme.models.orders.OfficeBox;
import org.acme.models.orders.Order;
import org.acme.models.orders.OrderProduct;
import org.acme.models.products.Product;
import org.acme.repository.orders.OfficeBoxRepository;
import org.acme.repository.orders.OrderRepository;
import org.acme.repository.products.ProductRepository;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.List;

@Mapper(config = QuarkusMappingConfig.class)
public interface OrderProductMapper {

    OrderRepository orderRepository = new OrderRepository();
    ProductRepository productRepository = new ProductRepository();
    OfficeBoxRepository officeBoxRepository = new OfficeBoxRepository();

    @Mapping(target = "orderID", source = "order.id")
    @Mapping(target = "productID", source = "product.id")
    @Mapping(target = "officeBoxIDStart", source = "officeBoxStart.id")
    @Mapping(target = "officeBoxIDEnd", source = "officeBoxEnd.id")
    OrderProductDTO getOrderProductDTOPFromOrderProduct(OrderProduct orderProduct);

    @Mapping(target = "order", source = "orderID", qualifiedByName = "findOrderById")
    @Mapping(target = "product", source = "productID", qualifiedByName = "findProductById")
    @Mapping(target = "officeBoxStart", source = "officeBoxIDStart", qualifiedByName = "findOfficeBoxById")
    @Mapping(target = "officeBoxEnd", source = "officeBoxIDEnd", qualifiedByName = "findOfficeBoxById")
    OrderProduct getOrderProductFromOrderProductDTO(OrderProductDTO orderProductDTO);

    @Named("findOrderById")
    static Order findOrderById(Long id) {
        return orderRepository.findById(id);
    }

    @Named("findProductById")
    static Product findProductById(Long id) {
        return productRepository.findById(id);
    }

    @Named("findOfficeBoxById")
    static OfficeBox findOfficeBoxById(Long id) {
        return officeBoxRepository.findById(id);
    }

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "officeBoxStart", source = "officeBoxIDStart", qualifiedByName = "getOfficeBox")
    @Mapping(target = "officeBoxEnd", source = "officeBoxIdEnd", qualifiedByName = "getOfficeBox")
    @Mapping(target = "product", source = "orderInfoDTO.productID", qualifiedByName = "getProductFromOrderInfoDTO")
    @Mapping(target = "orderBeginDate", source = "orderInfoDTO.orderBeginDate")
    @Mapping(target = "orderEndDate", source = "orderInfoDTO.orderEndDate")
    @Mapping(target = "serviceSecurityType", source = "orderInfoDTO.serviceSecurityType")
    OrderProduct gerOrderProductFromOrderInfoDTO(Long officeBoxIDStart, Long officeBoxIdEnd, Order order, OrderInfoDTO orderInfoDTO);

    @Mapping(target = "productID", source = "product.id")
    @Mapping(target = "orderProductID", source = "orderProduct.id")
    OrderInfoDTO getOrderInfoDTOFromOrderProduct(OrderProduct orderProduct);

    @Named("getOfficeBox")
    static OfficeBox getOfficeBox(Long officeBoxID) {
        return officeBoxRepository.findById(officeBoxID);
    }

    @Named("getProductFromOrderInfoDTO")
    static Product getProductFromOrderInfoDTO(Long productID) {
        return productRepository.findById(productID);
    }

    @Mapping(target = "orderID", source = "order.id")
    UserOrderDTO getUserOrderDTOFromOrderInfoDTO(Order order, List<OrderInfoDTO> orderInfoList);

    @Mapping(target = "officeBoxIDStart", source = "orderProduct.officeBoxStart.id")
    @Mapping(target = "officeBoxIDEnd", source = "orderProduct.officeBoxEnd.id")
    @Mapping(target = "productID", source = "orderProduct.product.id")
    @Mapping(target = "deposit", source = "orderProduct.product.deposit")
    OrderProductWithoutOrderDTO getOrderProductWithoutOrderDTOFromOrderProduct(OrderProduct orderProduct);

    @Mapping(target = "id", source = "orderProduct.id")
    @Mapping(target = "orderProductState", source = "orderProduct.orderProductState")
    OrderProductStateDTO getOrderProductStateDTO(OrderProduct orderProduct, String productName, String customerName);

}
