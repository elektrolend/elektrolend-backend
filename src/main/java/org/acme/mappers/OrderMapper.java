package org.acme.mappers;

import io.vertx.ext.web.handler.impl.HttpStatusException;
import org.acme.dto.orders.OrderDTO;
import org.acme.dto.orders.OrderManyProductsDTO;
import org.acme.dto.orders.OrderProductWithoutOrderDTO;
import org.acme.dto.orders.OrderWithOrderProductsDTO;
import org.acme.models.accounts.Customer;
import org.acme.models.orders.Order;
import org.acme.repository.accounts.CustomerRepository;
import org.apache.http.HttpStatus;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.List;

@Mapper(config = QuarkusMappingConfig.class)
public interface OrderMapper {

    CustomerRepository customerRepository = new CustomerRepository();

    @Mapping(target = "customerID", source = "customer.id")
    OrderDTO getOrderDTOFromOrder(Order order);

    @Mapping(target = "customer", source = "customerID", qualifiedByName = "findCustomerByUserId")
    Order getOrderFromOrderManyProductsDTO(OrderManyProductsDTO orderManyProductsDTO);

    @Named("findCustomerByUserId")
    static Customer findCustomerByUserId(Long id) {
        return customerRepository.findCustomerByUserIdOptional(id)
                .orElseThrow(() -> new HttpStatusException(HttpStatus.SC_NOT_FOUND));
    }

    @Mapping(target = "orderProductList", source = "orderProductWithoutOrderDTOList")
    OrderWithOrderProductsDTO getOrderWithOrderProductsFromOrderWithOrderProductsDTO(Order order, List<OrderProductWithoutOrderDTO> orderProductWithoutOrderDTOList);

}
