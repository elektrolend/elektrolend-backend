package org.acme.mappers;

import org.acme.dto.accounts.StateDTO;
import org.acme.models.accounts.Country;
import org.acme.models.accounts.State;
import org.acme.repository.accounts.CountryRepository;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

@Mapper(config = QuarkusMappingConfig.class)
public interface StateMapper {

    CountryRepository countryRepository = new CountryRepository();

    @Mapping(target = "countryID", source = "country.id")
    StateDTO getStateDTOFromState(State state);

    @Mapping(target = "country", source = "countryID", qualifiedByName = "findCountryId")
    State getStateFromStateDTO(StateDTO stateDTO);

    @Named("findCountryId")
    static Country findCountryId(Long id) {
        return countryRepository.findById(id);
    }
}
