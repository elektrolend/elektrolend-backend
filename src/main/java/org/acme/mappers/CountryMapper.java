package org.acme.mappers;

import org.acme.dto.accounts.CountryDTO;
import org.acme.models.accounts.Country;
import org.mapstruct.Mapper;

@Mapper(config = QuarkusMappingConfig.class)
public interface CountryMapper {

    CountryDTO getCountryDTOFromCountry(Country country);

    Country getCountryFromCountryDTO(CountryDTO countryDTO);

}
