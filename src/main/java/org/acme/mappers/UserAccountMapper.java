package org.acme.mappers;

import org.acme.dto.accounts.UserAccountDTO;
import org.acme.dto.accounts.UserDetailsDTO;
import org.acme.dto.accounts.UserLoggedDTO;
import org.acme.dto.accounts.UserLoginDTO;
import org.acme.models.accounts.Customer;
import org.acme.models.accounts.State;
import org.acme.models.accounts.UserAccount;
import org.acme.repository.accounts.StateRepository;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

@Mapper(config = QuarkusMappingConfig.class)
public interface UserAccountMapper {

    StateRepository stateRepository = new StateRepository();

    UserAccountDTO getUserAccountDTOFromUserAccount(UserAccount userAccount);

    @Mapping(target = "state", source = "state", qualifiedByName = "createStringState")
    @Mapping(target = "nickname", source = "userAccount.nickname")
    @Mapping(target = "country", source = "state", qualifiedByName = "getCountryFromState")
    UserDetailsDTO getUserDetailsDTOFromCustomer(Customer customer);

    UserLoggedDTO getUserLoggedDTOFromUserLoginDTO(UserLoginDTO userLoginDTO);

    @Named("getCountryFromState")
    static String getCountryFromState(State state) {
        return state.country.name;
    }

    @Named("createStringState")
    static String createStringState(State state) {
        return state.name;
    }

}
