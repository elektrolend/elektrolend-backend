package org.acme.mappers;

import io.vertx.ext.web.handler.impl.HttpStatusException;
import org.acme.dto.orders.ComplaintCreateDTO;
import org.acme.dto.orders.ComplaintDTO;
import org.acme.models.orders.Complaint;
import org.acme.models.orders.ComplaintPhoto;
import org.acme.models.orders.OrderProduct;
import org.acme.repository.orders.ComplaintPhotoRepository;
import org.acme.repository.orders.OrderProductRepository;
import org.apache.http.HttpStatus;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.List;
import java.util.stream.Collectors;

@Mapper(config = QuarkusMappingConfig.class)
public interface ComplaintMapper {

    ComplaintPhotoRepository complaintPhotoRepository = new ComplaintPhotoRepository();
    OrderProductRepository orderProductRepository = new OrderProductRepository();

    @Mapping(target = "imageUrls", source = "complaint.id", qualifiedByName = "getPhotos")
    @Mapping(target = "orderProductID", source = "orderProduct.id")
    @Mapping(target = "id", source = "complaint.id")
    @Mapping(target = "refund", source = "complaint.refund")
    @Mapping(target = "date", source = "complaint.date")
    @Mapping(target = "description", source = "complaint.description")
    @Mapping(target = "isConsidered", source = "complaint.isConsidered")
    @Mapping(target = "newBoxID", source = "orderProduct.officeBoxEnd.id")
    @Mapping(target = "newBoxPassword", source = "orderProduct.boxPassword")
    @Mapping(target = "customerID", source = "orderProduct.id", qualifiedByName = "getCustomer")
    @Mapping(target = "customerName", source = "orderProduct.id", qualifiedByName = "getCustomerName")
    @Mapping(target = "productName", source = "orderProduct.id", qualifiedByName = "getProductName")
    ComplaintDTO getComplaintDTOFromComplaint(Complaint complaint, OrderProduct orderProduct);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "orderProduct", source = "orderProductID", qualifiedByName = "getOrderProduct")
    @Mapping(target = "refund", ignore = true)
    @Mapping(target = "date", ignore = true)
    @Mapping(target = "isConsidered", ignore = true)
    Complaint getComplaintFromComplaintCreateDTO(ComplaintCreateDTO complaintDTO);

    @Named("getPhotos")
    static List<String> getPhotos(Long complaintID) {
        return complaintPhotoRepository.findAll().stream()
                .filter(photo -> photo.complaint.id.equals(complaintID))
                .map(ComplaintPhoto::getUrl)
                .collect(Collectors.toList());
    }

    @Named("getOrderProduct")
    static OrderProduct getOrderProduct(Long id) {
        return orderProductRepository
                .findByIdOptional(id)
                .orElseThrow(() -> new HttpStatusException(HttpStatus.SC_NOT_FOUND));
    }

    @Named("getCustomer")
    static Long getCustomer(Long orderProductID) {
        return orderProductRepository
                .findById(orderProductID)
                .getOrder()
                .getCustomer()
                .getId();
    }

    @Named("getCustomerName")
    static String getCustomerName(Long orderProductID) {
        return orderProductRepository
                .findById(orderProductID)
                .getOrder()
                .getCustomer()
                .getName();
    }

    @Named("getProductName")
    static String getProductName(Long orderProductID) {
        return orderProductRepository
                .findById(orderProductID)
                .getProduct()
                .getName();
    }

}
