package org.acme.mappers;

import org.acme.dto.products.PriceDTO;
import org.acme.models.products.Price;
import org.acme.models.products.Product;
import org.acme.repository.products.ProductRepository;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

@Mapper(config = QuarkusMappingConfig.class)
public interface PriceMapper {

    ProductRepository productRepository = new ProductRepository();

    @Mapping(target = "productID", source = "product.id")
    PriceDTO getPriceDTOFromPrice(Price price);

    @Mapping(target = "product", source = "productID", qualifiedByName = "fromPriceProduct")
    Price getPriceFromPriceDTO(PriceDTO priceDto);

    @Named("fromPriceProduct")
    static Product fromPriceProduct(Long id) {
        return productRepository.findById(id);
    }

}
