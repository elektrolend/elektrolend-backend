package org.acme.mappers;

import org.acme.dto.accounts.EmployeeDTO;
import org.acme.dto.accounts.EmployeeDetailsDTO;
import org.acme.models.accounts.Employee;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(config = QuarkusMappingConfig.class)
public interface EmployeeMapper {

    @Mapping(target = "nickname", source = "userAccount.nickname")
    EmployeeDTO getEmployeeDTOFromEmployee(Employee employee);

    Employee getEmployeeFromEmployeeDTO(EmployeeDTO employeeDTO);

    @Mapping(target = "nickname", source = "userAccount.nickname")
    EmployeeDetailsDTO fromEmployee(Employee employee);

}
