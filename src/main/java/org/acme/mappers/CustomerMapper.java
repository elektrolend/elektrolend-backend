package org.acme.mappers;

import org.acme.dto.accounts.CustomerDTO;
import org.acme.models.accounts.Customer;
import org.acme.models.accounts.State;
import org.acme.repository.accounts.StateRepository;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

@Mapper(config = QuarkusMappingConfig.class)
public interface CustomerMapper {

    StateRepository stateRepository = new StateRepository();

    @Mapping(target = "state", source = "state.name")
    @Mapping(target = "country", source = "state.country.name")
    @Mapping(target = "nickname", source = "userAccount.nickname")
    CustomerDTO getCustomerDTOFromCustomer(Customer customer);

    @Mapping(target = "state", source = "state", qualifiedByName = "findState")
    Customer getCustomerFromCustomerDTO(CustomerDTO customerDTO);

    @Named("findState")
    static State findState(String state) {
        return stateRepository.findByNameOptional(state).orElse(null);
    }

}
