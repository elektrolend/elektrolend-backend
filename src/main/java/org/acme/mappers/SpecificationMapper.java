package org.acme.mappers;

import org.acme.dto.products.SpecificationDTO;
import org.acme.models.products.Product;
import org.acme.models.products.Specification;
import org.acme.repository.products.ProductRepository;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

@Mapper(config = QuarkusMappingConfig.class)
public interface SpecificationMapper {

    ProductRepository productRepository = new ProductRepository();

    @Mapping(target = "productID", source = "product.id")
    SpecificationDTO getSpecificationDTOFromSpecification(Specification specification);

    @Mapping(target = "product", source = "productID", qualifiedByName = "fromPriceProduct")
    Specification getSpecificationFromSpecificationDTO(SpecificationDTO specificationDto);

    @Named("fromPriceProduct")
    static Product fromPriceProduct(Long id) {
        return productRepository.findById(id);
    }

}
