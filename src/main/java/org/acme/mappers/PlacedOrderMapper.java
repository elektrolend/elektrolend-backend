package org.acme.mappers;

import org.acme.dto.orders.OrderProductInfoDTO;
import org.acme.dto.orders.PlacedOrderDTO;
import org.acme.models.orders.OrderProduct;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.ArrayList;
import java.util.List;

@Mapper(config = QuarkusMappingConfig.class)
public interface PlacedOrderMapper {

    @Mapping(target = "orderID", source = "id")
    @Mapping(target = "orderProductInfoList", source = "orderProductList", qualifiedByName = "createOrderProductInfoList")
    PlacedOrderDTO toDto(Long id, List<OrderProduct> orderProductList);

    @Named("createOrderProductInfoList")
    static List<OrderProductInfoDTO> createOrderProductInfoList(List<OrderProduct> orderProductList) {
        List<OrderProductInfoDTO> list = new ArrayList<>();
        orderProductList.forEach(orderProduct ->
                list.add(new OrderProductInfoDTO(orderProduct.id, orderProduct.officeBoxStart.id, orderProduct.officeBoxEnd.id, orderProduct.boxPassword)));
        return list;
    }

}