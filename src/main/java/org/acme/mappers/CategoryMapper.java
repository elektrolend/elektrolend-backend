package org.acme.mappers;

import org.acme.dto.products.CategoryDTO;
import org.acme.models.products.Category;
import org.mapstruct.Mapper;

@Mapper(config = QuarkusMappingConfig.class)
public interface CategoryMapper {

    CategoryDTO getCategoryDTOFromCategory(Category category);

    Category getCategoryFromCategoryDTO(CategoryDTO categoryDTO);

}
