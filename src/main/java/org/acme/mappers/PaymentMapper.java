package org.acme.mappers;

import org.acme.dto.orders.PaymentAccountDTO;
import org.acme.dto.orders.PaymentDTO;
import org.acme.models.orders.Order;
import org.acme.models.orders.Payment;
import org.acme.repository.orders.OrderRepository;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

@Mapper(config = QuarkusMappingConfig.class)
public interface PaymentMapper {

    OrderRepository orderRepository = new OrderRepository();

    @Mapping(target = "orderID", source = "order.id")
    PaymentDTO getPaymentDTOFromPayment(Payment payment);

    @Mapping(target = "order", source = "orderID", qualifiedByName = "findOrderById")
    Payment getPaymentFromPaymentDTO(PaymentDTO paymentDTO);

    PaymentAccountDTO getPaymentAccountDTO(String paymentAccount);

    @Named("findOrderById")
    static Order findOrderById(Long id) {
        return orderRepository.findById(id);
    }

}
