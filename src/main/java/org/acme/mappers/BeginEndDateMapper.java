package org.acme.mappers;

import org.acme.dto.orders.BeginEndDateDTO;
import org.mapstruct.Mapper;

import java.time.LocalDate;

@Mapper(config = QuarkusMappingConfig.class)
public interface BeginEndDateMapper {

    BeginEndDateDTO getBeginEndDateDTOFromLocalDates(LocalDate beginDate, LocalDate endDate);

}
