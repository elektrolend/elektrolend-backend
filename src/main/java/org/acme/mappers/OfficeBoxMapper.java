package org.acme.mappers;

import org.acme.dto.orders.OfficeBoxDTO;
import org.acme.dto.orders.OfficeBoxDailyDetailDTO;
import org.acme.models.accounts.Customer;
import org.acme.models.orders.OfficeBox;
import org.acme.models.orders.OrderProduct;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

@Mapper(config = QuarkusMappingConfig.class)
public interface OfficeBoxMapper {

    OfficeBoxDTO getOfficeBoxDTOFromOfficeBox(OfficeBox officeBox);

    OfficeBox getOfficeBoxFromOfficeBoxDTO(OfficeBoxDTO officeBoxDTO);

    @Mapping(target = "orderProductID", source = "orderProduct.id")
    @Mapping(target = "productID", source = "orderProduct.product.id")
    @Mapping(target = "productName", source = "orderProduct.product.name")
    @Mapping(target = "customerName", source = "orderProduct", qualifiedByName = "findCustomer")
    OfficeBoxDailyDetailDTO getOfficeBoxDailyDetailDTO(OrderProduct orderProduct, Long officeBoxID);

    @Named("findCustomer")
    static String findCustomer(OrderProduct orderProduct) {
        Customer customer = orderProduct.getOrder().getCustomer();
        return customer.getName() + " " + customer.getSurname();
    }

}
