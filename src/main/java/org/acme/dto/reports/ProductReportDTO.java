package org.acme.dto.reports;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class ProductReportDTO {

    private String productName;
    private Integer rentalNumbers;

}
