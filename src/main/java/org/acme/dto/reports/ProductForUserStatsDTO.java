package org.acme.dto.reports;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class ProductForUserStatsDTO {

    private String productName;
    private int totalSum;

}
