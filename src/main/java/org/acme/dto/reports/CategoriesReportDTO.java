package org.acme.dto.reports;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class CategoriesReportDTO {

    private List<CategoryReportDTO> categoryReportDTOList;
    private Double categoriesSum;

}
