package org.acme.dto.reports;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class UserStatsDTO {

    private String customerName;
    private int totalSum;
    private List<ProductForUserStatsDTO> productListForUserStatsDTO;

}
