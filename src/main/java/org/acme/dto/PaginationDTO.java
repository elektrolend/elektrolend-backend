package org.acme.dto;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@EqualsAndHashCode
public class PaginationDTO {

    private int pageNum;
    private int pageSize;
    private int totalResults;

}
