package org.acme.dto.products;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.acme.dto.PaginationDTO;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class ProductsDTOWithPagination {

    private List<ProductDTO> productDTOList;
    private PaginationDTO paginationDTO;

}
