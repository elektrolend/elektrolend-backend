package org.acme.dto.products;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class SpecificationDTO {

    private Long id;
    private Long productID;
    private String key;
    private String value;

}
