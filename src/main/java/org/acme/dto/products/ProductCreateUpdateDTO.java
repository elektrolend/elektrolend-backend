package org.acme.dto.products;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class ProductCreateUpdateDTO {

    private Long categoryID;
    private String name;
    private String description;
    private Map<String, String> specificationsMap;
    private Integer deposit;
    private Integer insurance;
    private String imageUrl;
    private Boolean isAvailable;

}
