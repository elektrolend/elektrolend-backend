package org.acme.dto.products;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class CategoryDTO {

    private Long id;
    private String name;
    private String description;
    private String imageUrl;

}
