package org.acme.dto.accounts;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class UserProfileDTO {

    private Long id;
    private String name;
    private String surname;
    private String phone;
    private String email;

}
