package org.acme.dto.accounts;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.acme.dto.PaginationDTO;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class CustomersDTOWithPagination {

    private List<CustomerDTO> customerDTOList;
    private PaginationDTO paginationDTO;

}
