package org.acme.dto.accounts;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.acme.dto.PaginationDTO;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class EmployeesDTOWithPagination {

    private List<EmployeeDTO> employeeDTOList;
    private PaginationDTO paginationDTO;

}
