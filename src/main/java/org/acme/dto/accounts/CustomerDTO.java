package org.acme.dto.accounts;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CustomerDTO {

    private Long id;
    private String nickname;
    private String password;
    private String phone;
    private String email;
    private String street;
    private String homeNumber;
    private String localNumber;
    private String city;
    private String zipCode;
    private String state;
    private String country;
    private String name;
    private String surname;

}
