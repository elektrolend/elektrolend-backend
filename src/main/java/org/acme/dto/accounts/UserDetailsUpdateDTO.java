package org.acme.dto.accounts;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class UserDetailsUpdateDTO {

    private String email;
    private String name;
    private String surname;
    private String phone;
    private String street;
    private String homeNumber;
    private String localNumber;
    private String zipCode;
    private String city;
    private String state;
    private String country;

}
