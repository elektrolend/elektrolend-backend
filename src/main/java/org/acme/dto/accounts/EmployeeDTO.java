package org.acme.dto.accounts;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.acme.models.accounts.EmployeeType;

@Getter
@Setter
@NoArgsConstructor
public class EmployeeDTO {

    private Long id;
    private EmployeeType employeeType;
    private String nickname;
    private String password;
    private String phone;
    private String email;
    private String name;
    private String surname;

}
