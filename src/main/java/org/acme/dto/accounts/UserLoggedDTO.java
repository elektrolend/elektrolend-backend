package org.acme.dto.accounts;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class UserLoggedDTO {

    private Long id;
    private String nickname;
    private String accountType;

}
