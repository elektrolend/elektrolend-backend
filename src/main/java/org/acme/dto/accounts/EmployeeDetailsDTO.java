package org.acme.dto.accounts;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class EmployeeDetailsDTO {

    private Long id;
    private String nickname;
    private String email;
    private String name;
    private String surname;
    private String phone;

}
