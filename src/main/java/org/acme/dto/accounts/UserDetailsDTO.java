package org.acme.dto.accounts;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class UserDetailsDTO extends EmployeeDetailsDTO {

    private String street;
    private String homeNumber;
    private String localNumber;
    private String zipCode;
    private String city;
    private String state;
    private String country;

}
