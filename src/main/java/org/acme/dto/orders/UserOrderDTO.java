package org.acme.dto.orders;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class UserOrderDTO {

    private Long orderID;
    private LocalDate orderDate;
    private List<OrderInfoDTO> orderInfoList;

}
