package org.acme.dto.orders;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class ComplaintCreateDTO {

    private Long orderProductID;
    private String description;
    private List<String> imageUrls;

}
