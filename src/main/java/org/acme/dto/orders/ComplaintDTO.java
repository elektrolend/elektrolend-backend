package org.acme.dto.orders;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class ComplaintDTO {

    private Long id;
    private Long orderProductID;
    private Integer refund;
    private LocalDate date;
    private String description;
    private List<String> imageUrls;
    private Boolean isConsidered;
    private Long newBoxID;
    private String newBoxPassword;
    private Long customerID;
    private String customerName;
    private String productName;

}
