package org.acme.dto.orders;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.acme.models.orders.OrderProductState;
import org.acme.models.orders.ServiceSecurityType;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
public class OrderProductDTO {

    private Long id;
    private Long officeBoxIDStart;
    private Long officeBoxIDEnd;
    private Long orderID;
    private Long productID;
    private OrderProductState orderProductState;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate orderBeginDate;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate orderEndDate;
    private Integer price;
    private ServiceSecurityType serviceSecurityType;
    private Integer deposit;
    private String boxPassword;

}
