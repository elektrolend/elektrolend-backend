package org.acme.dto.orders;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.acme.dto.PaginationDTO;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class UserOrderWithPaginationDTO {

    private List<UserOrderDTO> userOrderDTOList;
    private PaginationDTO paginationDTO;

}
