package org.acme.dto.orders;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.acme.models.orders.OrderProductState;

@Getter
@Setter
@NoArgsConstructor
public class OrderProductStatusDTO {

    private Long orderProductID;
    private OrderProductState orderProductState;

}

