package org.acme.dto.orders;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
public class BeginEndDateDTO {

    private LocalDate beginDate;
    private LocalDate endDate;

}
