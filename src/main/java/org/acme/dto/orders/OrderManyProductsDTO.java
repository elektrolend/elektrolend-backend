package org.acme.dto.orders;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class OrderManyProductsDTO {

    private Long customerID;
    private List<OrderInfoDTO> orderInfoList;

}
