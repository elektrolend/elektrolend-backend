package org.acme.dto.orders;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
public class OrderDTO {

    private Long id;
    private Long customerID;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate orderDate;

}