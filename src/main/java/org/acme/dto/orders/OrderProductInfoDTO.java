package org.acme.dto.orders;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OrderProductInfoDTO {

    private Long orderProductID;
    private Long officeBoxIDStart;
    private Long officeBoxIDEnd;
    private String officeBoxPassword;

}
