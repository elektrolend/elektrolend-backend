package org.acme.dto.orders;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class PlacedOrderDTO {

    private Long orderID;
    private List<OrderProductInfoDTO> orderProductInfoList;

}
