package org.acme.dto.orders;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class OfficeBoxDailyDetailDTO {

    private Long officeBoxID;
    private Long orderProductID;
    private Long productID;
    private String productName;
    private String customerName;

}
