package org.acme.dto.orders;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class OfficeBoxDTO {

    private Long id;
    private String code;

}
