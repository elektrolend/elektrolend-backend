package org.acme.dto.orders;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class OrderWithOrderProductsDTO {

    private OrderDTO order;
    private List<OrderProductWithoutOrderDTO> orderProductList;

}