package org.acme.dto.orders;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.acme.models.orders.OrderProductState;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OrderProductStateDTO {

    private Long id;
    private String productName;
    private String customerName;
    private OrderProductState orderProductState;

}
