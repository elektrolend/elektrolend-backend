package org.acme.dto.orders;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class PaymentDTO {

    private Long id;
    private Float value;
    private Date date;
    private Long orderID;
    private String paymentAccount;

}
