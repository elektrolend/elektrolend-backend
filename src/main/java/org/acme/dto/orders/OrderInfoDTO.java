package org.acme.dto.orders;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.acme.models.orders.OfficeBox;
import org.acme.models.orders.OrderProductState;
import org.acme.models.orders.ServiceSecurityType;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
public class OrderInfoDTO {

    private Long productID;
    private Long orderProductID;
    private LocalDate orderBeginDate;
    private LocalDate orderEndDate;
    private ServiceSecurityType serviceSecurityType;
    private OrderProductState orderProductState;
    private int orderProductPrice;
    private String boxPassword;
    private OfficeBox officeBoxStart;
    private OfficeBox officeBoxEnd;

}
