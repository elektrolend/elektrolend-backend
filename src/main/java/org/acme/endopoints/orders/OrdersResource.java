package org.acme.endopoints.orders;

import io.vertx.ext.web.handler.impl.HttpStatusException;
import org.acme.dto.orders.OrderManyProductsDTO;
import org.acme.dto.orders.PlacedOrderDTO;
import org.acme.repository.accounts.CustomerRepository;
import org.acme.services.orders.OrderService;
import org.acme.utils.ResponseUtil;
import org.apache.http.HttpStatus;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/orders")
@Produces(MediaType.APPLICATION_JSON)
public class OrdersResource {

    @Inject
    OrderService orderService;

    @Inject
    CustomerRepository customerRepository;

    @GET
    @RolesAllowed({"customer", "admin", "regular"})
    public Response getOrders() {
        return Response.ok(orderService.getOrders()).build();
    }

    @GET
    @Path("/{id}")
    @RolesAllowed({"customer", "admin", "regular"})
    public Response getOrderById(@PathParam("id") Long id) {
        try {
            return Response.ok(orderService.getOrderById(id)).build();
        } catch (HttpStatusException ex) {
            return ResponseUtil.createResponseWithMessage(
                    Response.Status.NOT_FOUND,
                    "Order o podanym id nie istnieje"
            );
        }
    }

    @POST
    @Path("/create-order")
    @Transactional
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed("customer")
    public Response createOrder(OrderManyProductsDTO orderManyProductsDTO) {
        if (customerRepository.findCustomerByUserIdOptional(orderManyProductsDTO.getCustomerID()).isEmpty()) {
            return ResponseUtil.createResponseWithMessage(
                    Response.Status.BAD_REQUEST,
                    "Użytkownik z podanym id nie istnieje"
            );
        }

        if (orderManyProductsDTO.getOrderInfoList().isEmpty()) {
            return ResponseUtil.createResponseWithMessage(
                    Response.Status.BAD_REQUEST,
                    "Nie można złożyć zamówienia bez produktów"
            );
        }

        if (!orderService.areAllProductsAvailable(orderManyProductsDTO.getOrderInfoList())) {
            return ResponseUtil.createResponseWithMessage(
                    Response.Status.BAD_REQUEST,
                    "Nie można złożyć zamówienia. Jeden lub więcej produktów jest niedostępne"
            );
        }

        List<String> notAvailableProducts = orderService.getNotAvailableProducts(orderManyProductsDTO);
        if (!notAvailableProducts.isEmpty()) {
            return ResponseUtil.createResponseWithMessage(
                    Response.Status.BAD_REQUEST,
                    "Produkt(y) " + notAvailableProducts + " jest niedostępny (niedostępne) w podanych datach"
            );
        }

        try {
            PlacedOrderDTO orderCreated = orderService.createOrder(orderManyProductsDTO);
            return Response.ok(orderCreated).build();
        } catch (HttpStatusException ex) {
            if (ex.getStatusCode() == HttpStatus.SC_NOT_FOUND) {
                return ResponseUtil.createResponseWithMessage(
                        Response.Status.NOT_FOUND,
                        "Użytkownik z podanym id nie istnieje"
                );
            }
            if (ex.getStatusCode() == HttpStatus.SC_CONFLICT) {
                return ResponseUtil.createResponseWithMessage(
                        Response.Status.CONFLICT,
                        "Nie ma wystarczającej liczby skrytek do obsłużenia zamówienia"
                );
            }
            return ResponseUtil.createResponseWithMessage(
                    Response.Status.INTERNAL_SERVER_ERROR,
                    "Błąd serwera"
            );
        }
    }

    @DELETE
    @Path("/{id}")
    @Transactional
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({"admin", "regular"})
    public Response deleteOrder(@PathParam(("id")) Long id) {
        if (orderService.deleteOrder(id)) {
            return Response.noContent().build();
        }
        return Response.status(Response.Status.BAD_REQUEST).build();
    }

}
