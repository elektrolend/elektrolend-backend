package org.acme.endopoints.orders;

import io.vertx.ext.web.handler.impl.HttpStatusException;
import org.acme.dto.orders.OrderProductChangeStatusDTO;
import org.acme.dto.orders.OrderProductDTO;
import org.acme.services.orders.OrderProductService;
import org.apache.http.HttpStatus;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;

@Path("/order-products")
@Produces(MediaType.APPLICATION_JSON)
public class OrderProductResource {

    @Inject
    OrderProductService orderProductService;

    @GET
    @PermitAll
    public Response getOrderProducts() {
        return Response.ok(orderProductService.getOrderProducts()).build();
    }

    @GET
    @Path("/{id}")
    @PermitAll
    public Response getById(@PathParam("id") Long id) {
        try {
            return Response.ok(orderProductService.getById(id)).build();
        } catch (Exception e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @GET
    @Path("/statuses")
    @RolesAllowed({"customer", "admin", "regular"})
    public Response getOrderProductWithStates() {
        return Response.ok(orderProductService.getOrderProductWithStates()).build();
    }

    @POST
    @Transactional
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({"customer", "admin", "regular"})
    public Response addOrderProduct(OrderProductDTO orderProductDTO) {
        try {
            OrderProductDTO orderProduct = orderProductService.addOrderProduct(orderProductDTO);
            return Response.created(URI.create("/order-products/" + orderProduct.getId())).build();
        } catch (Exception e) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @POST
    @Path("/status")
    @Transactional
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({"customer", "admin", "regular"})
    public Response changeOrderProductsStatus(OrderProductChangeStatusDTO orderProductChangeStatusDTO) {
        try {
            orderProductService.changeOrderProductsStatus(orderProductChangeStatusDTO);
        } catch (HttpStatusException ex) {
            if (ex.getStatusCode() == HttpStatus.SC_NOT_FOUND) {
                return Response.status(HttpStatus.SC_NOT_FOUND)
                        .entity("Jeden z obiektów nie istnieje")
                        .build();
            }
            if (ex.getStatusCode() == HttpStatus.SC_CONFLICT) {
                return Response.status(HttpStatus.SC_CONFLICT)
                        .entity("Nie można zmienić na podany status jednego z produktów")
                        .build();
            }
        }
        return Response.ok("Zmieniono statusy dla podanej listy produktów").build();
    }

    @DELETE
    @Path("/{id}")
    @Transactional
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({"admin", "regular"})
    public Response deleteOrderProduct(@PathParam(("id")) Long id) {
        if (orderProductService.deleteOrderProduct(id)) {
            return Response.noContent().build();
        }
        return Response.status(Response.Status.BAD_REQUEST).build();
    }

}

