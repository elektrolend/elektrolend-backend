package org.acme.endopoints.orders;

import org.acme.dto.orders.OfficeBoxDTO;
import org.acme.services.orders.OfficeBoxService;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.time.LocalDate;
import java.util.Objects;

@Path("/office-boxes")
@Produces(MediaType.APPLICATION_JSON)
public class OfficeBoxResource {

    @Inject
    OfficeBoxService officeBoxService;

    @GET
    @RolesAllowed({"admin", "regular"})
    public Response getOfficeBoxes() {
        return Response.ok(officeBoxService.getOfficeBoxes()).build();
    }

    @GET
    @Path("/statuses")
    @RolesAllowed({"admin", "regular"})
    public Response getOfficeBoxesStatuses(@QueryParam("date") String stringDate) {
        LocalDate date = Objects.nonNull(stringDate) ? LocalDate.parse(stringDate) : LocalDate.now();
        return Response.ok(officeBoxService.getOfficeBoxesStatuses(date)).build();
    }

    @POST
    @Transactional
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({"admin", "regular"})
    public Response addOfficeBox(OfficeBoxDTO officeBoxDTO) {
        try {
            OfficeBoxDTO officeBox = officeBoxService.addOfficeBox(officeBoxDTO);
            return Response.created(URI.create("/office-boxes/" + officeBox.getId())).build();
        } catch (Exception e) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @DELETE
    @Path("/{id}")
    @Transactional
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({"admin", "regular"})
    public Response deleteOfficeBox(@PathParam(("id")) Long id) {
        if (officeBoxService.deleteOfficeBox(id)) {
            return Response.noContent().build();
        }
        return Response.status(Response.Status.BAD_REQUEST).build();
    }

}

