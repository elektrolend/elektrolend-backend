package org.acme.endopoints.orders;

import io.vertx.ext.web.handler.impl.HttpStatusException;
import org.acme.dto.orders.PaymentAccountDTO;
import org.acme.dto.orders.PaymentDTO;
import org.acme.services.orders.PaymentService;
import org.apache.http.HttpStatus;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;

@Path("/payments")
@Produces(MediaType.APPLICATION_JSON)
public class PaymentResource {

    @Inject
    PaymentService paymentService;

    @GET
    @RolesAllowed({"admin", "regular"})
    public Response getPayments() {
        return Response.ok(paymentService.getPayments()).build();
    }

    @POST
    @Transactional
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({"customer", "admin", "regular"})
    public Response addPayment(PaymentDTO paymentDTO) {
        try {
            PaymentDTO payment = paymentService.addPayment(paymentDTO);
            return Response.created(URI.create("/payments/" + payment.getId())).build();
        } catch (Exception e) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @GET
    @Path("/order-product/{id}")
    @RolesAllowed({"admin", "regular"})
    public Response getAccountByOrderProductId(@PathParam("id") Long orderProductID) {
        try {
            PaymentAccountDTO paymentAccountDTO = paymentService.getAccountByOrderProductId(orderProductID);
            return Response.ok(paymentAccountDTO).build();
        } catch (HttpStatusException ex) {
            if (ex.getStatusCode() == HttpStatus.SC_NOT_FOUND) {
                return Response.status(HttpStatus.SC_NOT_FOUND)
                        .entity("Dla podanego orderProduct: " + orderProductID + " nie znaleziono płatności")
                        .build();
            }
        }
        return Response.status(Response.Status.BAD_REQUEST).build();
    }

    @DELETE
    @Path("/{id}")
    @Transactional
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({"admin", "regular"})
    public Response deletePayment(@PathParam(("id")) Long id) {
        if (paymentService.deletePayment(id)) {
            return Response.noContent().build();
        }
        return Response.status(Response.Status.BAD_REQUEST).build();
    }

}
