package org.acme.endopoints.orders;

import io.vertx.ext.web.handler.impl.HttpStatusException;
import org.acme.services.orders.RefundService;
import org.acme.utils.ResponseUtil;
import org.apache.http.HttpStatus;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.PATCH;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/refunds")
@Produces(MediaType.APPLICATION_JSON)
public class RefundResource {

    @Inject
    RefundService refundService;

    @Path("/order-product/{id}")
    @PATCH
    @Transactional
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({"admin", "regular"})
    public Response returnRefundToCustomer(@PathParam("id") Long orderProductID) {
        try {
            refundService.returnRefundToCustomer(orderProductID);
            return Response.ok().build();
        } catch (HttpStatusException ex) {
            if (ex.getStatusCode() == HttpStatus.SC_NOT_FOUND) {
                return ResponseUtil.createResponseWithMessage(
                        Response.Status.NOT_FOUND,
                        "Order product o podanym id nie istnieje"
                );
            }
            if (ex.getStatusCode() == HttpStatus.SC_BAD_REQUEST) {
                return ResponseUtil.createResponseWithMessage(
                        Response.Status.CONFLICT,
                        "Nie można rozpatrzeć reklamacji. OrderProduct znajduje się w niewłaściwym stanie"
                );
            }
        }
        return Response.status(Response.Status.BAD_REQUEST).build();
    }

    @DELETE
    @Path("/{id}")
    @Transactional
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({"admin", "regular"})
    public Response deleteComplaint(@PathParam(("id")) Long id) {
        if (refundService.deleteRefund(id)) {
            return Response.noContent().build();
        }
        return Response.status(Response.Status.BAD_REQUEST).build();
    }

}
