package org.acme.endopoints.orders;

import io.vertx.ext.web.handler.impl.HttpStatusException;
import org.acme.dto.orders.ComplaintCreateDTO;
import org.acme.dto.orders.ComplaintDTO;
import org.acme.dto.orders.ComplaintRefundDTO;
import org.acme.services.orders.ComplaintPhotoService;
import org.acme.services.orders.ComplaintService;
import org.acme.utils.ResponseUtil;
import org.apache.http.HttpStatus;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/complaints")
@Produces(MediaType.APPLICATION_JSON)
public class ComplaintResource {

    @Inject
    ComplaintService complaintService;

    @Inject
    ComplaintPhotoService complaintPhotoService;

    @POST
    @Transactional
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed("customer")
    public Response createComplaint(ComplaintCreateDTO complaintCreateDTO) {
        try {
            ComplaintDTO complaintDTO = complaintService.createComplaint(complaintCreateDTO);
            return Response.ok(complaintDTO).build();
        } catch (HttpStatusException ex) {
            if (ex.getStatusCode() == HttpStatus.SC_NOT_FOUND) {
                return ResponseUtil.createResponseWithMessage(
                        Response.Status.NOT_FOUND,
                        "Order product o podanym id nie istnieje"
                );
            }
            if (ex.getStatusCode() == HttpStatus.SC_CONFLICT) {
                return ResponseUtil.createResponseWithMessage(
                        Response.Status.CONFLICT,
                        "Nie ma wystarczającej ilości skrytek. Prosimy o dostarczenie sprzętu do sklepu."
                );
            }
            if (ex.getStatusCode() == HttpStatus.SC_BAD_REQUEST) {
                return ResponseUtil.createResponseWithMessage(
                        Response.Status.CONFLICT,
                        "Nie można rozpatrzeć reklamacji. OrderProduct znajduje się w niewłaściwym stanie"
                );
            }
        }
        return Response.status(Response.Status.BAD_REQUEST).build();
    }

    @PATCH
    @Path("/{id}")
    @Transactional
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({"admin", "regular"})
    public Response considerComplaint(@PathParam(("id")) Long complaintID, ComplaintRefundDTO complaintRefundDTO) {
        try {
            ComplaintDTO complaintDTO = complaintService.considerComplaint(complaintID, complaintRefundDTO);
            return Response.ok(complaintDTO).build();
        } catch (HttpStatusException ex) {
            if (ex.getStatusCode() == HttpStatus.SC_NOT_FOUND) {
                return ResponseUtil.createResponseWithMessage(
                        Response.Status.NOT_FOUND,
                        "Reklamacja o podanym id nie istnieje"
                );
            }
            if (ex.getStatusCode() == HttpStatus.SC_CONFLICT) {
                return ResponseUtil.createResponseWithMessage(
                        Response.Status.CONFLICT,
                        "Wysokość zwrotu jest wyższa niż wpłacono za zamówiony produkt"
                );
            }
        }
        return Response.status(Response.Status.BAD_REQUEST).build();
    }

    @DELETE
    @Path("/{id}")
    @Transactional
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({"admin", "regular"})
    public Response deleteComplaint(@PathParam(("id")) Long id) {
        if (complaintService.deleteComplaint(id)) {
            return Response.noContent().build();
        }
        return Response.status(Response.Status.BAD_REQUEST).build();
    }

    @DELETE
    @Path("/photos/{id}")
    @Transactional
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({"admin", "regular"})
    public Response deleteComplaintPhoto(@PathParam(("id")) Long id) {
        if (complaintPhotoService.deleteComplaintPhoto(id)) {
            return Response.noContent().build();
        }
        return Response.status(Response.Status.BAD_REQUEST).build();
    }

    @GET
    @Path("/photos/{id}")
    @Transactional
    @RolesAllowed({"admin", "regular"})
    public Response getComplaintPhotos(@PathParam(("id")) Long complaintID) {
        try {
            return Response.ok(complaintPhotoService.getComplaintPhotos(complaintID)).build();
        } catch (HttpStatusException ex) {
            return ResponseUtil.createResponseWithMessage(
                    Response.Status.NOT_FOUND,
                    "Reklamacja o podanym id nie istnieje"
            );
        }
    }

    @GET
    @Transactional
    @RolesAllowed({"admin", "regular"})
    public Response getComplaints() {
        return Response.ok(complaintService.getAllComplaints()).build();
    }

    @GET
    @Path("/{id}")
    @Transactional
    @RolesAllowed({"admin", "regular"})
    public Response getComplaintById(@PathParam(("id")) Long id) {
        return Response.ok(complaintService.getComplaintById(id)).build();
    }

}
