package org.acme.endopoints.reports;

import org.acme.services.reports.ReportService;
import org.acme.utils.ResponseUtil;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Objects;

@Path("/reports")
@Produces(MediaType.APPLICATION_JSON)
public class ReportsResource {

    @Inject
    ReportService reportService;

    private static final String WRONG_DATE_FORMAT_MESSAGE = "Podano nieprawidłowy format dat. Prawidłowy format to YYYY-mm-DD";

    @GET
    @Path("/categories")
    @RolesAllowed({"admin", "regular"})
    public Response getCategoryReport(
            @QueryParam("from") String from,
            @QueryParam("to") String to) {

        try {
            LocalDate dateFrom = Objects.nonNull(from) ? LocalDate.parse(from) : LocalDate.of(2020, 1, 1);
            LocalDate dateTo = Objects.nonNull(to) ? LocalDate.parse(to) : LocalDate.now();

            return Response.ok(reportService.getCategoryReport(dateFrom, dateTo)).build();
        } catch (DateTimeParseException ex) {
            return ResponseUtil.createResponseWithMessage(Response.Status.BAD_REQUEST, WRONG_DATE_FORMAT_MESSAGE);
        }
    }

    @GET
    @Path("/order-products")
    @RolesAllowed({"admin", "regular"})
    public Response getOrderProductReport(
            @QueryParam("from") String from,
            @QueryParam("to") String to) {

        try {
            LocalDate dateFrom = Objects.nonNull(from) ? LocalDate.parse(from) : LocalDate.of(2020, 1, 1);
            LocalDate dateTo = Objects.nonNull(to) ? LocalDate.parse(to) : LocalDate.now();

            return Response.ok(reportService.getOrderProductReport(dateFrom, dateTo)).build();
        } catch (DateTimeParseException ex) {
            return ResponseUtil.createResponseWithMessage(Response.Status.BAD_REQUEST, WRONG_DATE_FORMAT_MESSAGE);
        }
    }

    @GET
    @Path("/user-loans")
    @RolesAllowed({"admin", "regular"})
    public Response getUserLoans(
            @QueryParam("from") String from,
            @QueryParam("to") String to) {

        try {
            LocalDate dateFrom = Objects.nonNull(from) ? LocalDate.parse(from) : LocalDate.of(2020, 1, 1);
            LocalDate dateTo = Objects.nonNull(to) ? LocalDate.parse(to) : LocalDate.now();

            return Response.ok(reportService.getUserLoans(dateFrom, dateTo)).build();
        } catch (DateTimeParseException ex) {
            return ResponseUtil.createResponseWithMessage(Response.Status.BAD_REQUEST, WRONG_DATE_FORMAT_MESSAGE);
        }
    }

}
