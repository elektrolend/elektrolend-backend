package org.acme.endopoints.accounts;

import io.vertx.ext.web.handler.impl.HttpStatusException;
import org.acme.dto.accounts.CustomerDTO;
import org.acme.dto.accounts.CustomerNewPasswordDTO;
import org.acme.dto.accounts.CustomerPasswordRestoringEmailDTO;
import org.acme.dto.accounts.UserAccountDTO;
import org.acme.dto.accounts.UserAccountUpdateDTO;
import org.acme.dto.accounts.UserDetailsDTO;
import org.acme.dto.accounts.UserDetailsUpdateDTO;
import org.acme.dto.accounts.UserLoggedDTO;
import org.acme.dto.accounts.UserLoginDTO;
import org.acme.dto.orders.UserOrderWithPaginationDTO;
import org.acme.models.orders.OrderProductState;
import org.acme.pagination.PageRequest;
import org.acme.repository.accounts.CustomerRepository;
import org.acme.services.accounts.CustomerService;
import org.acme.services.accounts.UserAccountService;
import org.acme.utils.PasswordUtil;
import org.acme.utils.ResponseUtil;
import org.apache.http.HttpStatus;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;

@Path("/users")
@Produces(MediaType.APPLICATION_JSON)
public class UserAccountsResource {

    private static final String USER_WITH_GIVEN_ID_NOT_EXISTS = "Użytkownik z podanym id nie istnieje";

    @Inject
    UserAccountService userAccountService;

    @Inject
    CustomerService customerService;

    @Inject
    CustomerRepository customerRepository;

    @GET
    @RolesAllowed({"admin", "regular"})
    public Response getUserAccounts() {
        return Response.ok(userAccountService.getUserAccounts()).build();
    }

    @POST
    @Path("/register")
    @Transactional
    @Consumes(MediaType.APPLICATION_JSON)
    @PermitAll
    public Response addUserAccount(CustomerDTO customerDTO) {
        if (PasswordUtil.isPasswordTooWeak(customerDTO.getPassword())) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        if (customerRepository.findByNicknameOptional(customerDTO.getNickname()).isPresent()) {
            return ResponseUtil.createResponseWithMessage(
                    Response.Status.CONFLICT,
                    "Użytkownik z podaną nazwą już istnieje"
            );
        }
        if (customerRepository.findByEmailOptional(customerDTO.getEmail()).isPresent()) {
            return ResponseUtil.createResponseWithMessage(
                    Response.Status.CONFLICT,
                    "Użytkownik z podanym adresem email już istnieje"
            );
        }
        if (customerRepository.findByPhoneOptional(customerDTO.getPhone()).isPresent()) {
            return ResponseUtil.createResponseWithMessage(
                    Response.Status.CONFLICT,
                    "Użytkownik z podanym numerem telefonu już istnieje"
            );
        }
        try {
            CustomerDTO customer = customerService.addCustomer(customerDTO);
            return Response.created(URI.create("user/register/" + customer.getId())).build();
        } catch (Exception ex) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @POST
    @Transactional
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/login")
    @PermitAll
    public Response login(UserLoginDTO userLoginDTO) {
        try {
            UserLoggedDTO userLogged = userAccountService.logUser(userLoginDTO);
            return Response.ok(userLogged).build();
        } catch (HttpStatusException ex) {
            if (ex.getStatusCode() == HttpStatus.SC_UNAUTHORIZED) {
                return ResponseUtil.createResponseWithMessage(
                        Response.Status.UNAUTHORIZED,
                        "Niepoprawne dane logowania"
                );
            }
            if (ex.getStatusCode() == HttpStatus.SC_NOT_ACCEPTABLE) {
                return ResponseUtil.createResponseWithMessage(
                        Response.Status.NOT_ACCEPTABLE,
                        "Należy podać kod potwierdzający ważność konta"
                );
            }
            if (ex.getStatusCode() == HttpStatus.SC_CONFLICT) {
                return ResponseUtil.createResponseWithMessage(
                        Response.Status.CONFLICT,
                        "Niepoprawny kod potwierdzający ważność konta"
                );
            }
        }
        return Response.status(Response.Status.BAD_REQUEST).build();
    }

    @GET
    @Path("/{id}")
    @PermitAll
    public Response getById(@PathParam("id") Long id) {
        try {
            return Response.ok(userAccountService.getById(id)).build();
        } catch (Exception e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @GET
    @Path("nickname/{nickname}")
    @PermitAll
    public Response getByNickname(@PathParam("nickname") String nickname) {
        try {
            return Response.ok(userAccountService.getByNickname(nickname)).build();
        } catch (Exception e) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @DELETE
    @Path("/id/{id}")
    @Transactional
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({"admin", "regular"})
    public Response deleteUserAccount(@PathParam("id") Long id) {
        if (userAccountService.deleteUserAccount(id)) {
            return Response.noContent().build();
        }
        return Response.status(Response.Status.BAD_REQUEST).build();
    }

    @PUT
    @Path("/{id}")
    @Transactional
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({"customer", "admin", "regular"})
    public Response updateUserAccount(@PathParam("id") Long id, UserDetailsUpdateDTO userDetailsUpdateDTO) {
        UserDetailsDTO userDetails = new UserDetailsDTO();
        try {
            userDetails = userAccountService.updateUserDetails(id, userDetailsUpdateDTO);
        } catch (HttpStatusException ex) {
            if (ex.getStatusCode() == HttpStatus.SC_NOT_FOUND) {
                return ResponseUtil.createResponseWithMessage(
                        Response.Status.NOT_FOUND,
                        USER_WITH_GIVEN_ID_NOT_EXISTS
                );
            }
        }
        return Response.ok(userDetails).build();
    }

    @PATCH
    @Path("/login/{id}")
    @Transactional
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({"customer", "admin", "regular"})
    public Response updateUserAccount(@PathParam(("id")) Long id, UserAccountUpdateDTO userAccountUpdateDTO) {
        try {
            UserAccountDTO userAccount = userAccountService.updateUserAccount(id, userAccountUpdateDTO);
            return Response.ok(userAccount).build();
        } catch (HttpStatusException ex) {
            if (ex.getStatusCode() == HttpStatus.SC_NOT_FOUND) {
                return ResponseUtil.createResponseWithMessage(
                        Response.Status.NOT_FOUND,
                        USER_WITH_GIVEN_ID_NOT_EXISTS
                );
            }
            if (ex.getStatusCode() == HttpStatus.SC_CONFLICT) {
                return ResponseUtil.createResponseWithMessage(
                        Response.Status.CONFLICT,
                        "Podany nickname jest zajęty"
                );
            }
            if (ex.getStatusCode() == HttpStatus.SC_BAD_REQUEST) {
                return ResponseUtil.createResponseWithMessage(
                        Response.Status.BAD_REQUEST,
                        "Podane hasło jest za słabe"
                );
            }
            return ResponseUtil.createResponseWithMessage(
                    Response.Status.INTERNAL_SERVER_ERROR,
                    "Błąd serwera"
            );
        }
    }

    @GET
    @Path("/{id}/orders")
    @RolesAllowed({"customer", "admin", "regular"})
    public Response getUserOrders(
            @PathParam("id") Long id,
            @BeanParam PageRequest pageRequest,
            @QueryParam("orderProductState") OrderProductState orderProductState) {
        UserOrderWithPaginationDTO userOrderDTOList = null;
        try {
            userOrderDTOList = userAccountService.getUserOrders(id, pageRequest, orderProductState);
        } catch (HttpStatusException ex) {
            if (ex.getStatusCode() == HttpStatus.SC_NOT_FOUND) {
                return ResponseUtil.createResponseWithMessage(
                        Response.Status.NOT_FOUND,
                        USER_WITH_GIVEN_ID_NOT_EXISTS
                );
            }
            if (ex.getStatusCode() == HttpStatus.SC_BAD_REQUEST) {
                return ResponseUtil.createResponseWithMessage(
                        Response.Status.BAD_REQUEST,
                        "Numer strony oraz jej rozmiar muszą być dodatnimi liczbami"
                );
            }
        }

        if (userOrderDTOList == null) {
            return ResponseUtil.createResponseWithMessage(
                    Response.Status.NO_CONTENT,
                    " Użytkownik nie posiada żadnych zamówień"
            );
        }

        return Response.ok(userOrderDTOList).build();
    }

    @POST
    @Transactional
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/password-restoring-email")
    @PermitAll
    public Response passwordRestoring(CustomerPasswordRestoringEmailDTO customerPasswordRestoringEmailDTO) {
        try {
            customerService.sendEmailPasswordRestoring(customerPasswordRestoringEmailDTO);
            return Response.ok().build();
        } catch (HttpStatusException ex) {
            if (ex.getStatusCode() == HttpStatus.SC_NOT_FOUND) {
                return ResponseUtil.createResponseWithMessage(
                        Response.Status.NOT_FOUND,
                        "Nie ma użytkownika o podanym mailu"
                );
            }
        }
        return Response.status(Response.Status.BAD_REQUEST).build();
    }

    @POST
    @Transactional
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/password-restoring")
    @PermitAll
    public Response passwordRestoring(CustomerNewPasswordDTO customerNewPasswordDTO) {
        try {
            customerService.setNewPassword(customerNewPasswordDTO);
            return Response.ok().build();
        } catch (HttpStatusException ex) {
            if (ex.getStatusCode() == HttpStatus.SC_CONFLICT) {
                return ResponseUtil.createResponseWithMessage(
                        Response.Status.CONFLICT,
                        "Podano niepoprawne dane do odzyskania hasła"
                );
            }
            if (ex.getStatusCode() == HttpStatus.SC_NOT_FOUND) {
                return ResponseUtil.createResponseWithMessage(
                        Response.Status.NOT_FOUND,
                        "Użytkownik nie istnieje"
                );
            }
            if (ex.getStatusCode() == HttpStatus.SC_UNAUTHORIZED) {
                return ResponseUtil.createResponseWithMessage(
                        Response.Status.UNAUTHORIZED,
                        "Niepoprawny kod do odzyskania hasła"
                );
            }
            if (ex.getStatusCode() == HttpStatus.SC_NOT_ACCEPTABLE) {
                return ResponseUtil.createResponseWithMessage(
                        Response.Status.NOT_ACCEPTABLE,
                        "Podano za słabe hasło"
                );
            }
        }
        return Response.status(Response.Status.BAD_REQUEST).build();
    }

}
