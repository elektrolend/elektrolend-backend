package org.acme.endopoints.accounts;

import org.acme.dto.accounts.CountryDTO;
import org.acme.services.accounts.CountryService;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;

@Path("/countries")
@Produces(MediaType.APPLICATION_JSON)
public class CountriesResource {

    @Inject
    CountryService countryService;

    @GET
    @PermitAll
    public Response getCountries() {
        return Response.ok(countryService.getCountries()).build();
    }

    @POST
    @Transactional
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({"admin", "regular"})
    public Response addCountry(CountryDTO countryDTO) {
        try {
            CountryDTO country = countryService.addCountry(countryDTO);
            return Response.created(URI.create("/countries/" + country.getId())).build();
        } catch (Exception e) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @GET
    @Path("/{id}")
    @PermitAll
    public Response getById(@PathParam("id") Long id) {
        try {
            return Response.ok(countryService.getById(id)).build();
        } catch (Exception e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }


    @DELETE
    @Path("/{id}")
    @Transactional
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({"admin", "regular"})
    public Response deleteCategory(@PathParam(("id")) Long id) {
        if (countryService.deleteCountry(id)) {
            return Response.noContent().build();
        }
        return Response.status(Response.Status.BAD_REQUEST).build();
    }

}
