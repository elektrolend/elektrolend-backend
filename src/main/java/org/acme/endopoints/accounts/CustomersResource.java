package org.acme.endopoints.accounts;

import org.acme.pagination.PageRequest;
import org.acme.services.accounts.CustomerService;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/customers")
@Produces(MediaType.APPLICATION_JSON)
public class CustomersResource {

    @Inject
    CustomerService customerService;

    @GET
    @RolesAllowed({"admin", "regular"})
    public Response getCustomers(@BeanParam PageRequest pageRequest) {
        return Response.ok(customerService.getCustomers(pageRequest)).build();
    }

    @GET
    @Path("/{id}")
    @RolesAllowed({"customer", "admin", "regular"})
    public Response getById(@PathParam("id") Long id) {
        try {
            return Response.ok(customerService.getById(id)).build();
        } catch (Exception e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }


    @DELETE
    @Path("/{id}")
    @Transactional
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({"admin", "regular"})
    public Response deleteCustomer(@PathParam(("id")) Long id) {
        if (customerService.deleteCustomer(id)) {
            return Response.noContent().build();
        }
        return Response.status(Response.Status.BAD_REQUEST).build();
    }

}
