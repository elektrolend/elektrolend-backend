package org.acme.endopoints.accounts;

import org.acme.dto.accounts.EmployeeDTO;
import org.acme.pagination.PageRequest;
import org.acme.repository.accounts.EmployeeRepository;
import org.acme.services.accounts.EmployeeService;
import org.acme.utils.PasswordUtil;
import org.acme.utils.ResponseUtil;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;

@Path("/employees")
@Produces(MediaType.APPLICATION_JSON)
public class EmployeesResource {

    @Inject
    EmployeeService employeeService;
    @Inject
    EmployeeRepository employeeRepository;

    @GET
    @RolesAllowed({"admin", "regular"})
    public Response getEmployees(@BeanParam PageRequest pageRequest) {
        return Response.ok(employeeService.getEmployees(pageRequest)).build();
    }

    @POST
    @Transactional
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addEmployee(EmployeeDTO employeeDTO) {
        if (PasswordUtil.isPasswordTooWeak(employeeDTO.getPassword())) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        if (employeeRepository.findByEmailOptional(employeeDTO.getEmail()).isPresent()) {
            return ResponseUtil.createResponseWithMessage(
                    Response.Status.CONFLICT,
                    "Użytkownik z podanym adresem email już istnieje"
            );
        }
        if (employeeRepository.findByNicknameOptional(employeeDTO.getNickname()).isPresent()) {
            return ResponseUtil.createResponseWithMessage(
                    Response.Status.CONFLICT,
                    "Użytkownik z podaną nazwą już istnieje"
            );
        }
        if (employeeRepository.findByPhoneOptional(employeeDTO.getPhone()).isPresent()) {
            return ResponseUtil.createResponseWithMessage(
                    Response.Status.CONFLICT,
                    "Użytkownik z podanym numerem telefonu już istnieje"
            );
        }
        try {
            EmployeeDTO employee = employeeService.addEmployee(employeeDTO);
            return Response.created(URI.create("/employees/" + employee.getId())).build();
        } catch (Exception e) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @GET
    @Path("/{id}")
    public Response getById(@PathParam("id") Long id) {
        try {
            return Response.ok(employeeService.getById(id)).build();
        } catch (Exception e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @DELETE
    @Path("/{id}")
    @Transactional
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteEmployee(@PathParam(("id")) Long id) {
        if (employeeService.deleteEmployee(id)) {
            return Response.noContent().build();
        }
        return Response.status(Response.Status.BAD_REQUEST).build();
    }

}
