package org.acme.endopoints.products;

import io.vertx.ext.web.handler.impl.HttpStatusException;
import org.acme.dto.products.ProductActivationDTO;
import org.acme.dto.products.ProductCreateUpdateDTO;
import org.acme.dto.products.ProductDTO;
import org.acme.pagination.PageRequest;
import org.acme.services.orders.OrderService;
import org.acme.services.products.ProductService;
import org.acme.utils.ResponseUtil;
import org.apache.http.HttpStatus;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;

@Path("/products")
@Produces(MediaType.APPLICATION_JSON)
public class ProductsResource {

    @Inject
    ProductService productService;

    @Inject
    OrderService orderService;

    @GET
    @PermitAll
    public Response getProducts(
            @QueryParam("categoryId") Long categoryId,
            @QueryParam("availability") @DefaultValue("true") Boolean availability,
            @BeanParam PageRequest pageRequest) {
        return Response.ok(productService.getProducts(pageRequest, categoryId, availability)).build();
    }


    @GET
    @Path("/search-page")
    @PermitAll
    public Response searchProducts(
            @QueryParam("productName") String productName,
            @QueryParam("priceFrom") Long priceFrom,
            @QueryParam("priceTo") Long priceTo,
            @QueryParam("categoryId") Long categoryId,
            @QueryParam("sortDescending") Boolean sortDescending,
            @QueryParam("availability") @DefaultValue("true") Boolean availability,
            @BeanParam PageRequest pageRequest) {
        return Response.ok(
                productService.searchProducts(
                        productName,
                        priceFrom,
                        priceTo,
                        categoryId,
                        sortDescending,
                        availability,
                        pageRequest
                )
        ).build();
    }

    @GET
    @Path("/{id}")
    @PermitAll
    public Response getById(@PathParam("id") Long id) {
        try {
            return Response.ok(productService.getById(id)).build();
        } catch (Exception e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @GET
    @Path("/{id}/availability")
    @PermitAll
    public Response getBusyDatesForProduct(@PathParam("id") Long id) {
        try {
            return Response.ok(orderService.getBusyDatesForProduct(id)).build();
        } catch (Exception e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @POST
    @Transactional
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({"admin", "regular"})
    public Response addProduct(ProductCreateUpdateDTO productCreateUpdateDTO) {
        try {
            ProductDTO product = productService.addProduct(productCreateUpdateDTO);
            return Response.created(URI.create("/products/" + product.getId())).build();
        } catch (Exception e) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @POST
    @Transactional
    @Path("/availability/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({"admin", "regular"})
    public Response changeAvailabilityProduct(
            @PathParam("id") Long productID,
            ProductActivationDTO productActivationDTO) {
        try {
            boolean isChanged = productService.changeProductAvailability(productID, productActivationDTO.getIsAvailable());
            String message = "Nie udało się zmienić statusu dla produktu o id: " + productID;
            if (isChanged && Boolean.TRUE.equals(productActivationDTO.getIsAvailable())) {
                message = "Produkt o id: " + productID + " został aktywowany";
            } else if (isChanged && Boolean.FALSE.equals(productActivationDTO.getIsAvailable())) {
                message = "Produkt o id: " + productID + " został zdezaktywowany";
            }
            return Response.ok(message).build();
        } catch (Exception e) {
            return ResponseUtil.createResponseWithMessage(
                    Response.Status.NO_CONTENT,
                    "Product o podanym ID nie istnieje"
            );
        }
    }

    @PUT
    @Transactional
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({"admin", "regular"})
    public Response updateProduct(@PathParam(("id")) Long id, ProductCreateUpdateDTO productCreateUpdateDTO) {
        try {
            ProductDTO product = productService.updateProduct(id, productCreateUpdateDTO);
            return Response.ok(product).build();
        } catch (HttpStatusException ex) {
            if (ex.getStatusCode() == HttpStatus.SC_NOT_FOUND) {
                return ResponseUtil.createResponseWithMessage(
                        Response.Status.NOT_FOUND,
                        "Produkt z podanym id nie istnieje"
                );
            }
            return ResponseUtil.createResponseWithMessage(
                    Response.Status.INTERNAL_SERVER_ERROR,
                    "Błąd serwera"
            );
        }
    }

    @DELETE
    @Transactional
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({"admin", "regular"})
    public Response deleteProduct(@PathParam(("id")) Long id) {
        if (productService.deleteProduct(id)) {
            return Response.noContent().build();
        }
        return Response.status(Response.Status.BAD_REQUEST).build();
    }

}
