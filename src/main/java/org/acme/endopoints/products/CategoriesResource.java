package org.acme.endopoints.products;

import org.acme.dto.products.CategoryDTO;
import org.acme.services.products.CategoryService;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;

@Path("/categories")
@Produces(MediaType.APPLICATION_JSON)
public class CategoriesResource {

    @Inject
    CategoryService categoryService;

    @GET
    @PermitAll
    public Response getCategories() {
        return Response.ok(categoryService.getCategories()).build();
    }

    @POST
    @Transactional
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({"regular", "admin"})
    public Response addCategory(CategoryDTO categoryDTO) {
        try {
            CategoryDTO category = categoryService.addCategory(categoryDTO);
            return Response.created(URI.create("/categories/" + category.getId())).build();
        } catch (Exception e) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @GET
    @Path("/{id}")
    @PermitAll
    public Response getById(@PathParam("id") Long id) {
        try {
            return Response.ok(categoryService.getById(id)).build();
        } catch (Exception e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }


    @DELETE
    @Path("/{id}")
    @Transactional
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({"regular", "admin"})
    public Response deleteCategory(@PathParam(("id")) Long id) {
        if (categoryService.deleteCategory(id)) {
            return Response.noContent().build();
        }
        return Response.status(Response.Status.BAD_REQUEST).build();
    }

}
