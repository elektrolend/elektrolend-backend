package org.acme.endopoints.products;

import org.acme.dto.products.PriceDTO;
import org.acme.services.products.PriceService;
import org.acme.utils.DateUtil;
import org.acme.utils.ResponseUtil;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.time.LocalDate;

@Path("/prices")
@Produces(MediaType.APPLICATION_JSON)
public class PricesResource {

    @Inject
    PriceService priceService;

    @GET
    @PermitAll
    public Response getPrices() {
        return Response.ok(priceService.getPrices()).build();
    }

    @POST
    @Transactional
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({"admin", "regular"})
    public Response addPrice(PriceDTO priceDTO) {
        if (priceDTO.getBeginDate() == null) {
            return ResponseUtil.createResponseWithMessage(
                    Response.Status.BAD_REQUEST,
                    "Data początkowa musi być podana"
            );
        }
        if (priceDTO.getEndDate() != null
                && !DateUtil.checkIfDatesAreChronological(priceDTO.getBeginDate(), priceDTO.getEndDate())) {
            return ResponseUtil.createResponseWithMessage(
                    Response.Status.BAD_REQUEST,
                    "Data początkowa powinna być przed datą końcową ceny."
            );
        }

        if (priceDTO.getBeginDate().isBefore(LocalDate.now())) {
            return ResponseUtil.createResponseWithMessage(
                    Response.Status.BAD_REQUEST,
                    "Data początkowa i końcowa nie może być przeszłą datą."
            );
        }
        try {
            PriceDTO priceCreated = priceService.addPrice(priceDTO);
            return Response.created(URI.create("/prices/" + priceCreated.getId())).build();
        } catch (Exception e) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @DELETE
    @Transactional
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({"admin", "regular"})
    public Response deletePrice(@PathParam(("id")) Long id) {
        if (priceService.deletePrice(id)) {
            return Response.noContent().build();
        }
        return Response.status(Response.Status.BAD_REQUEST).build();
    }

}
