package org.acme.endopoints.products;

import org.acme.dto.products.SpecificationDTO;
import org.acme.repository.products.SpecificationRepository;
import org.acme.services.products.SpecificationService;
import org.acme.utils.ResponseUtil;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;

@Path("/specifications")
@Produces(MediaType.APPLICATION_JSON)
public class SpecificationResource {

    @Inject
    SpecificationService specificationService;

    @Inject
    SpecificationRepository specificationRepository;

    @GET
    @PermitAll
    public Response getSpecifications() {
        return Response.ok(specificationService.getSpecifications()).build();
    }

    @POST
    @Transactional
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({"admin", "regular"})
    public Response addSpecification(SpecificationDTO specificationDTO) {
        if (specificationRepository.isSpecificationDuplicated(specificationDTO)) {
            return ResponseUtil.createResponseWithMessage(
                    Response.Status.BAD_REQUEST,
                    "Taka specyfikacja już istnieje."
            );
        }
        SpecificationDTO specification = specificationService.addSpecification(specificationDTO);
        return Response.created(URI.create("/specifications/" + specification.getId())).build();
    }

    @DELETE
    @Transactional
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({"admin", "regular"})
    public Response deleteSpecification(@PathParam(("id")) Long id) {
        if (specificationService.deleteSpecification(id)) {
            return Response.noContent().build();
        }
        return Response.status(Response.Status.BAD_REQUEST).build();
    }

}
