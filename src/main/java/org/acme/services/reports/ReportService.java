package org.acme.services.reports;

import lombok.AllArgsConstructor;
import org.acme.dto.reports.CategoriesReportDTO;
import org.acme.dto.reports.CategoryReportDTO;
import org.acme.dto.reports.OrderProductReportDTO;
import org.acme.dto.reports.ProductForUserStatsDTO;
import org.acme.dto.reports.ProductReportDTO;
import org.acme.dto.reports.ProductsReportDTO;
import org.acme.dto.reports.UserStatsDTO;
import org.acme.dto.reports.UsersStatsDTO;
import org.acme.models.orders.OrderProduct;
import org.acme.models.orders.ServiceSecurityType;
import org.acme.repository.accounts.CustomerRepository;
import org.acme.repository.orders.OrderProductRepository;
import org.acme.repository.products.ProductRepository;
import org.acme.utils.DateUtil;

import javax.enterprise.context.ApplicationScoped;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;

@AllArgsConstructor
@ApplicationScoped
public class ReportService {

    OrderProductRepository orderProductRepository;
    ProductRepository productRepository;
    CustomerRepository customerRepository;

    public CategoriesReportDTO getCategoryReport(LocalDate dateFrom, LocalDate dateTo) {
        List<OrderProduct> orderProductsInGivenDatesList = findAllOrderProductByGivenDates(dateFrom, dateTo);
        Map<String, Map<String, List<OrderProduct>>> orderProductsGroupedByCategoryMap = groupOrderProductsByCategory(orderProductsInGivenDatesList);
        Map<String, Map<String, Double>> categoriesWithSummedOrderProducts = sumOrderProducts(orderProductsGroupedByCategoryMap);

        return createCategoryReport(categoriesWithSummedOrderProducts);
    }

    public ProductsReportDTO getOrderProductReport(LocalDate dateFrom, LocalDate dateTo) {
        List<OrderProduct> orderProductList = findAllOrderProductByGivenDates(dateFrom, dateTo);
        Map<Long, List<OrderProduct>> orderProductGroupedByProductIdMap = groupOrderProductsByProductId(orderProductList);

        return createProductReport(orderProductGroupedByProductIdMap);
    }

    public UsersStatsDTO getUserLoans(LocalDate dateFrom, LocalDate dateTo) {
        List<OrderProduct> orderProductList = findAllOrderProductByGivenDates(dateFrom, dateTo);
        Map<Long, List<OrderProduct>> orderProductsGroupedByUsers = groupOrderProductsByUsers(orderProductList);

        return createUsersStatsReport(orderProductsGroupedByUsers);
    }

    private List<OrderProduct> findAllOrderProductByGivenDates(LocalDate dateFrom, LocalDate dateTo) {
        return orderProductRepository.findAll()
                .stream()
                .filter(orderProduct -> DateUtil.checkIfDatesAreChronologicalOrEqual(dateFrom, orderProduct.orderBeginDate))
                .filter(orderProduct -> DateUtil.checkIfDatesAreChronologicalOrEqual(orderProduct.orderBeginDate, dateTo))
                .collect(Collectors.toList());
    }

    private Map<String, Map<String, List<OrderProduct>>> groupOrderProductsByCategory(List<OrderProduct> orderProductsInGivenDatesList) {
        return orderProductsInGivenDatesList
                .stream()
                .collect(groupingBy(orderProduct -> orderProduct.getProduct().getCategory().getName(),
                        groupingBy(orderProduct -> orderProduct.getProduct().getName())));
    }

    private Map<String, Map<String, Double>> sumOrderProducts(Map<String, Map<String, List<OrderProduct>>> orderProductsGroupedByCategoryMap) {
        Map<String, Map<String, Double>> categoriesWithSummedOrderProducts = new HashMap<>();

        for (Map.Entry<String, Map<String, List<OrderProduct>>> category : orderProductsGroupedByCategoryMap.entrySet()) {
            double sumOfOrderProductsInSpecificCategory = 0;
            Map<String, Double> summedOrderProductsInSpecificCategoryMap = new HashMap<>();
            for (Map.Entry<String, List<OrderProduct>> productName : category.getValue().entrySet()) {
                for (OrderProduct orderProduct : productName.getValue()) {
                    double totalProductsPrice = 0;
                    if (orderProduct.getServiceSecurityType() == ServiceSecurityType.DEPOSIT) {
                        totalProductsPrice -= orderProduct.getProduct().getDeposit();
                    }
                    totalProductsPrice += orderProduct.getPrice();
                    sumOfOrderProductsInSpecificCategory += totalProductsPrice;
                }
                summedOrderProductsInSpecificCategoryMap.put(productName.getKey(), sumOfOrderProductsInSpecificCategory);
            }
            categoriesWithSummedOrderProducts.put(category.getKey(), summedOrderProductsInSpecificCategoryMap);
        }

        return categoriesWithSummedOrderProducts;
    }

    private CategoriesReportDTO createCategoryReport(Map<String, Map<String, Double>> categoriesWithSummedOrderProducts) {
        double totalReportSum = 0;
        List<CategoryReportDTO> categoryReportDTOList = new ArrayList<>();

        for (Map.Entry<String, Map<String, Double>> category : categoriesWithSummedOrderProducts.entrySet()) {
            CategoryReportDTO categoryReportDTO = new CategoryReportDTO();
            categoryReportDTO.setCategoryName(category.getKey());
            double specificCategorySum = 0;
            List<OrderProductReportDTO> orderProductReportDTOList = new ArrayList<>();
            for (Map.Entry<String, Double> productName : category.getValue().entrySet()) {
                OrderProductReportDTO orderProductReportDTO = new OrderProductReportDTO();
                orderProductReportDTO.setProductSum(productName.getValue());
                orderProductReportDTO.setProductName(productName.getKey());
                orderProductReportDTOList.add(orderProductReportDTO);
                totalReportSum += productName.getValue();
                specificCategorySum += productName.getValue();
            }
            categoryReportDTO.setCategorySum(specificCategorySum);
            categoryReportDTO.setOrderProductReportDTOList(orderProductReportDTOList);
            categoryReportDTOList.add(categoryReportDTO);
        }

        CategoriesReportDTO categoriesReportDTO = new CategoriesReportDTO();
        categoriesReportDTO.setCategoriesSum(totalReportSum);
        categoriesReportDTO.setCategoryReportDTOList(categoryReportDTOList);

        return categoriesReportDTO;
    }

    private Map<Long, List<OrderProduct>> groupOrderProductsByProductId(List<OrderProduct> orderProductsInGivenDatesList) {
        return orderProductsInGivenDatesList
                .stream()
                .collect(groupingBy(orderProduct -> orderProduct.getProduct().getId()));
    }

    private ProductsReportDTO createProductReport(Map<Long, List<OrderProduct>> orderProductGroupedByProductId) {
        ProductsReportDTO productsReportDTO = new ProductsReportDTO();
        productsReportDTO.setProductReportDTOList(new ArrayList<>());

        for (Map.Entry<Long, List<OrderProduct>> orderProduct : orderProductGroupedByProductId.entrySet()) {
            ProductReportDTO productReportDTO = new ProductReportDTO();
            productReportDTO.setProductName(productRepository.findById(orderProduct.getKey()).getName());
            productReportDTO.setRentalNumbers(orderProduct.getValue().size());
            productsReportDTO.getProductReportDTOList().add(productReportDTO);
        }

        return productsReportDTO;
    }

    private Map<Long, List<OrderProduct>> groupOrderProductsByUsers(List<OrderProduct> orderProductsInGivenDatesList) {
        return orderProductsInGivenDatesList
                .stream()
                .collect(groupingBy(orderProduct -> orderProduct.getOrder().getCustomer().getId()));
    }

    private UsersStatsDTO createUsersStatsReport(Map<Long, List<OrderProduct>> orderProductsGroupedByUsers) {
        UsersStatsDTO usersStatsDTO = new UsersStatsDTO();
        usersStatsDTO.setUserStats(new ArrayList<>());

        orderProductsGroupedByUsers.forEach((customerID, orderProductList) -> {
            UserStatsDTO userStatsDTO = new UserStatsDTO();
            userStatsDTO.setCustomerName(customerRepository.findCustomerNameById(customerID));
            userStatsDTO.setTotalSum(calculateTotalSumOfOrderProducts(orderProductList));
            usersStatsDTO.getUserStats().add(userStatsDTO);

            List<ProductForUserStatsDTO> productsForUserStatsDTOList = new ArrayList<>();
            Map<Long, List<OrderProduct>> groupedOrderProductsById = groupOrderProductsByProductId(orderProductList);
            groupedOrderProductsById.forEach((productID, orderProducts) -> {
                ProductForUserStatsDTO productForUserStatsDTO = new ProductForUserStatsDTO();
                productForUserStatsDTO.setProductName(orderProducts.get(0).getProduct().getName());
                productForUserStatsDTO.setTotalSum(calculateTotalSumOfOrderProducts(orderProducts));
                productsForUserStatsDTOList.add(productForUserStatsDTO);
            });
            userStatsDTO.setProductListForUserStatsDTO(productsForUserStatsDTOList);

        });

        return usersStatsDTO;
    }

    private int calculateTotalSumOfOrderProducts(List<OrderProduct> orderProductList) {
        return orderProductList.stream()
                .map(orderProduct -> {
                    int deposit = 0;
                    if (orderProduct.getServiceSecurityType() == ServiceSecurityType.DEPOSIT) {
                        deposit = orderProduct.getProduct().getDeposit();
                    }
                    return orderProduct.getPrice() - deposit;
                })
                .reduce(0, Integer::sum);
    }

}
