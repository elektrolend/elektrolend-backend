package org.acme.services.orders;

import io.vertx.ext.web.handler.impl.HttpStatusException;
import lombok.AllArgsConstructor;
import org.acme.dto.orders.OrderProductChangeStatusDTO;
import org.acme.dto.orders.OrderProductDTO;
import org.acme.dto.orders.OrderProductStateDTO;
import org.acme.dto.orders.OrderProductStatesDTO;
import org.acme.dto.orders.OrderProductStatusDTO;
import org.acme.mappers.OrderProductMapper;
import org.acme.models.accounts.Customer;
import org.acme.models.orders.OfficeBox;
import org.acme.models.orders.OrderProduct;
import org.acme.models.orders.OrderProductState;
import org.acme.repository.orders.OfficeBoxRepository;
import org.acme.repository.orders.OrderProductRepository;
import org.acme.utils.PasswordUtil;
import org.apache.http.HttpStatus;

import javax.enterprise.context.ApplicationScoped;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@ApplicationScoped
public class OrderProductService {

    OrderProductMapper orderProductMapper;
    OrderProductRepository orderProductRepository;
    OfficeBoxRepository officeBoxRepository;
    OrderService orderService;

    public List<OrderProductDTO> getOrderProducts() {
        return orderProductRepository.findAll()
                .stream()
                .map(orderProduct -> orderProductMapper.getOrderProductDTOPFromOrderProduct(orderProduct))
                .collect(Collectors.toList());
    }

    public OrderProductDTO getById(Long id) {
        return orderProductMapper.getOrderProductDTOPFromOrderProduct(orderProductRepository.findByIdOptional(id)
                .orElseThrow(() -> new HttpStatusException(HttpStatus.SC_NOT_FOUND)));
    }

    public OrderProductDTO addOrderProduct(OrderProductDTO orderProductDTO) {
        OrderProduct orderProduct = orderProductMapper.getOrderProductFromOrderProductDTO(orderProductDTO);
        orderProductRepository.persist(orderProduct);
        return orderProductMapper.getOrderProductDTOPFromOrderProduct(orderProduct);
    }

    public boolean deleteOrderProduct(Long id) {
        return orderProductRepository.deleteById(id);
    }

    public boolean changeOrderProductsStatus(OrderProductChangeStatusDTO orderProductChangeStatusDTO) {
        List<OrderProductStatusDTO> productList = orderProductChangeStatusDTO.getProductList();
        if (productList.isEmpty()) {
            throw new HttpStatusException(HttpStatus.SC_NOT_FOUND);
        }
        try {
            productList.forEach(product -> {
                OrderProduct orderProduct = orderProductRepository.findByIdOptional(product.getOrderProductID())
                        .orElseThrow(() -> new HttpStatusException(HttpStatus.SC_NOT_FOUND));
                OrderProductState orderProductState = product.getOrderProductState();
                OrderProductStateService orderProductStateService = OrderProductStateService.getInstance();
                if (!orderProductStateService.isAvailableStatusChange(orderProduct.getOrderProductState(), orderProductState)) {
                    throw new HttpStatusException(HttpStatus.SC_CONFLICT);
                }
                orderProduct.setOrderProductState(orderProductState);
            });
        } catch (HttpStatusException ex) {
            throw new HttpStatusException(ex.getStatusCode());
        }
        return true;
    }

    public OrderProductDTO changeReturnInformation(Long orderProductId) {
        OrderProduct orderProduct = orderProductRepository
                .findByIdOptional(orderProductId)
                .orElseThrow(() -> new HttpStatusException(HttpStatus.SC_NOT_FOUND));

        orderProduct.setOfficeBoxEnd(getFirstAvailableOfficeBox(LocalDate.now().plusDays(1)));
        orderProduct.setOrderEndDate(LocalDate.now().plusDays(1));
        orderProduct.setBoxPassword(PasswordUtil.createBoxPassword());
        orderProductRepository.persist(orderProduct);

        return orderProductMapper.getOrderProductDTOPFromOrderProduct(orderProduct);
    }

    public void changeOrderProductStatus(Long orderProductID, OrderProductState state) {
        OrderProduct orderProduct = orderProductRepository.findById(orderProductID);

        boolean isChangeStatusPossible = OrderProductStateService.getInstance().isAvailableStatusChange(
                orderProduct.getOrderProductState(),
                state
        );

        if (isChangeStatusPossible) {
            orderProduct.setOrderProductState(state);
        } else {
            throw new HttpStatusException(HttpStatus.SC_BAD_REQUEST);
        }

        orderProductRepository.persist(orderProduct);
    }

    private OfficeBox getFirstAvailableOfficeBox(LocalDate date) {
        Long availableOfficeBox = orderProductRepository.findFirstAvailableOfficeBox(date);
        return officeBoxRepository.findById(availableOfficeBox);
    }

    public OrderProductStatesDTO getOrderProductWithStates() {
        List<OrderProduct> orderProductsList = orderProductRepository.findAll()
                .stream()
                .collect(Collectors.toList());

        OrderProductStatesDTO orderProductStatesDTO = new OrderProductStatesDTO();
        List<OrderProductStateDTO> orderProducts = new ArrayList<>();

        orderProductsList.forEach(orderProduct -> {
            String productName = orderProduct.getProduct().getName();
            Customer customer = orderProduct.getOrder().getCustomer();
            String customerName = customer.getName() + " " + customer.getSurname();
            orderProducts.add(orderProductMapper.getOrderProductStateDTO(orderProduct, productName, customerName));
        });
        orderProductStatesDTO.setOrderProducts(orderProducts);

        return orderProductStatesDTO;
    }

}
