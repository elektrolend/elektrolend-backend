package org.acme.services.orders;

import io.vertx.ext.web.handler.impl.HttpStatusException;
import lombok.AllArgsConstructor;
import org.acme.models.orders.OrderProductState;
import org.acme.models.orders.Refund;
import org.acme.repository.orders.OrderProductRepository;
import org.acme.repository.orders.RefundRepository;
import org.apache.http.HttpStatus;

import javax.enterprise.context.ApplicationScoped;
import java.time.LocalDate;

@AllArgsConstructor
@ApplicationScoped
public class RefundService {

    RefundRepository refundRepository;
    OrderProductStateService orderProductStateService;
    OrderProductRepository orderProductRepository;
    OrderProductService orderProductService;

    public void addRefund(Integer refundValue, Long orderProductID) {
        Refund refund = new Refund();
        refund.setRefundValue(refundValue);
        refund.setDate(LocalDate.now());
        refund.setOrderProduct(orderProductRepository.findById(orderProductID));
        refund.setReturnedToCustomer(false);

        refundRepository.persist(refund);
    }

    public void returnRefundToCustomer(Long orderProductID) {
        Refund refund = refundRepository
                .findByOrderProductID(orderProductID)
                .orElseThrow(() -> new HttpStatusException(HttpStatus.SC_NOT_FOUND));

        refund.setReturnedToCustomer(true);
        refundRepository.persist(refund);

        orderProductService.changeOrderProductStatus(orderProductID, OrderProductState.REFUND);
    }

    public boolean deleteRefund(Long id) {
        return refundRepository.deleteById(id);
    }

}
