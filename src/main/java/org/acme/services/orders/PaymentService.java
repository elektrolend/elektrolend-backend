package org.acme.services.orders;

import io.vertx.ext.web.handler.impl.HttpStatusException;
import lombok.AllArgsConstructor;
import org.acme.dto.orders.PaymentAccountDTO;
import org.acme.dto.orders.PaymentDTO;
import org.acme.mappers.PaymentMapper;
import org.acme.models.orders.Payment;
import org.acme.repository.orders.PaymentRepository;
import org.apache.http.HttpStatus;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@ApplicationScoped
public class PaymentService {

    PaymentMapper paymentMapper;
    PaymentRepository paymentRepository;

    public List<PaymentDTO> getPayments() {
        return paymentRepository.findAll().stream()
                .map(paymentMapper::getPaymentDTOFromPayment)
                .collect(Collectors.toList());
    }

    public PaymentDTO addPayment(PaymentDTO paymentDTO) {
        Payment payment = paymentMapper.getPaymentFromPaymentDTO(paymentDTO);
        paymentRepository.persist(payment);
        return paymentMapper.getPaymentDTOFromPayment(payment);
    }

    public PaymentAccountDTO getAccountByOrderProductId(Long orderProductID) {
        return paymentMapper.getPaymentAccountDTO(paymentRepository
                .findByOrderProductIdOptional(orderProductID)
                .orElseThrow(() -> new HttpStatusException(HttpStatus.SC_NOT_FOUND))
                .getPaymentAccount()
        );
    }

    public boolean deletePayment(Long id) {
        return paymentRepository.deleteById(id);
    }

}
