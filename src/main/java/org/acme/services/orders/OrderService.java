package org.acme.services.orders;

import io.vertx.ext.web.handler.impl.HttpStatusException;
import lombok.AllArgsConstructor;
import org.acme.dto.orders.BeginEndDateDTO;
import org.acme.dto.orders.OrderBusyDatesDTO;
import org.acme.dto.orders.OrderInfoDTO;
import org.acme.dto.orders.OrderManyProductsDTO;
import org.acme.dto.orders.OrderProductWithoutOrderDTO;
import org.acme.dto.orders.OrderWithOrderProductsDTO;
import org.acme.dto.orders.PlacedOrderDTO;
import org.acme.mappers.BeginEndDateMapper;
import org.acme.mappers.OrderMapper;
import org.acme.mappers.OrderProductMapper;
import org.acme.mappers.PlacedOrderMapper;
import org.acme.models.orders.Order;
import org.acme.models.orders.OrderProduct;
import org.acme.models.products.Product;
import org.acme.repository.orders.OfficeBoxRepository;
import org.acme.repository.orders.OrderProductRepository;
import org.acme.repository.orders.OrderRepository;
import org.acme.repository.products.ProductRepository;
import org.acme.services.products.ProductService;
import org.acme.utils.DateUtil;
import org.acme.utils.PasswordUtil;
import org.apache.http.HttpStatus;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

@AllArgsConstructor
@ApplicationScoped
@Transactional
public class OrderService {

    OrderMapper orderMapper;
    OrderRepository orderRepository;

    OrderProductRepository orderProductRepository;
    BeginEndDateMapper beginEndDateMapper;

    OfficeBoxRepository officeBoxRepository;
    OrderProductMapper orderProductMapper;
    PlacedOrderMapper placedOrderMapper;
    ProductService productService;
    ProductRepository productRepository;

    public List<OrderWithOrderProductsDTO> getOrders() {
        List<OrderWithOrderProductsDTO> orderWithOrderProductsDTO = new ArrayList<>();

        orderRepository.findAll().stream()
                .forEach(order -> orderWithOrderProductsDTO.add(getOrderWithOrderProductsFromOrder(order)));
        return orderWithOrderProductsDTO;
    }

    public OrderWithOrderProductsDTO getOrderById(Long id) {
        Order order = orderRepository.findByIdOptional(id).orElseThrow(() -> new HttpStatusException(HttpStatus.SC_NOT_FOUND));
        return getOrderWithOrderProductsFromOrder(order);
    }

    private OrderWithOrderProductsDTO getOrderWithOrderProductsFromOrder(Order order) {
        List<OrderProductWithoutOrderDTO> orderProductWithoutOrderDTOList = orderProductRepository
                .findOrderProductsByOrderId(order.id)
                .stream()
                .map(orderProduct -> orderProductMapper.getOrderProductWithoutOrderDTOFromOrderProduct(orderProduct))
                .collect(Collectors.toList());
        return orderMapper.getOrderWithOrderProductsFromOrderWithOrderProductsDTO(order, orderProductWithoutOrderDTOList);
    }

    public OrderBusyDatesDTO getBusyDatesForProduct(Long id) {
        Optional<ArrayList<OrderProduct>> orderProductsOptionalList =
                orderProductRepository.findFutureOrderProductByProductId(id);
        if (orderProductsOptionalList.isPresent()) {
            ArrayList<OrderProduct> orderProductsList = orderProductsOptionalList.get();
            List<BeginEndDateDTO> busyDates = new ArrayList<>();
            orderProductsList.forEach(orderProduct ->
                    busyDates.add(beginEndDateMapper.getBeginEndDateDTOFromLocalDates(orderProduct.orderBeginDate, orderProduct.orderEndDate)));
            return new OrderBusyDatesDTO(busyDates);
        } else {
            return new OrderBusyDatesDTO();
        }
    }

    public PlacedOrderDTO createOrder(OrderManyProductsDTO orderManyProductsDTO) {
        try {
            Order order = orderMapper.getOrderFromOrderManyProductsDTO(orderManyProductsDTO);
            order.orderDate = LocalDate.now();
            orderRepository.persist(order);

            List<OrderProduct> orderProductList = persistOrderProducts(orderManyProductsDTO, order);
            return placedOrderMapper.toDto(order.id, orderProductList);
        } catch (HttpStatusException ex) {
            throw new HttpStatusException(ex.getStatusCode());
        }
    }

    private List<OrderProduct> persistOrderProducts(OrderManyProductsDTO orderManyProductsDTO, Order order) {
        List<OrderProduct> orderProductList = new ArrayList<>();
        try {
            orderManyProductsDTO.getOrderInfoList().forEach(orderInfoDTO -> {
                Long officeBoxIDStart = orderProductRepository.findFirstAvailableOfficeBox(orderInfoDTO.getOrderBeginDate());
                Long officeBoxIDEnd = orderProductRepository.findFirstAvailableOfficeBox(orderInfoDTO.getOrderEndDate());
                OrderProduct orderProduct = orderProductMapper.gerOrderProductFromOrderInfoDTO(officeBoxIDStart, officeBoxIDEnd, order, orderInfoDTO);
                orderProduct.boxPassword = PasswordUtil.createBoxPassword();
                orderProduct.orderProductState = productService.startOrder();
                orderProduct.price = productService.calculatePrice(orderInfoDTO);
                orderProductRepository.persist(orderProduct);
                orderProductList.add(orderProduct);
            });
        } catch (HttpStatusException ex) {
            throw new HttpStatusException(ex.getStatusCode());
        }
        return orderProductList;
    }

    public List<String> getNotAvailableProducts(OrderManyProductsDTO orderManyProductsDTO) {
        Map<Long, Boolean> productsAvailability = new HashMap<>();
        orderManyProductsDTO.getOrderInfoList().forEach(
                product -> productsAvailability.put(product.getProductID(), isProductAvailable(product)));

        return productsAvailability.entrySet().stream()
                .filter(productID -> !productID.getValue())
                .map(Map.Entry::getKey)
                .collect(Collectors.toList())
                .stream()
                .map(productID -> productRepository.findById(productID))
                .map(product -> product.name)
                .collect(Collectors.toList());
    }


    private boolean isProductAvailable(OrderInfoDTO orderInfoDTO) {
        Optional<ArrayList<OrderProduct>> optionalOrderProductList =
                orderProductRepository.findFutureOrderProductByProductId(orderInfoDTO.getProductID());
        if (optionalOrderProductList.isEmpty()) {
            return true;
        } else {
            AtomicBoolean isAvailable = new AtomicBoolean(true);
            ArrayList<OrderProduct> orderProductsList = optionalOrderProductList.get();
            orderProductsList.forEach(orderProduct -> {
                if (!DateUtil.timeSpansCorrect(orderInfoDTO.getOrderBeginDate(), orderInfoDTO.getOrderEndDate(),
                        orderProduct.orderBeginDate, orderProduct.orderEndDate)) {
                    isAvailable.set(false);
                }
            });
            return isAvailable.get();
        }
    }

    public boolean deleteOrder(Long id) {
        return orderRepository.deleteById(id);
    }

    public boolean areAllProductsAvailable(List<OrderInfoDTO> orderManyProductsDTO) {
        for (OrderInfoDTO order : orderManyProductsDTO) {
            Product product = productRepository.findById(order.getProductID());
            if (Boolean.FALSE.equals(product.getIsAvailable())) {
                return false;
            }
        }
        return true;
    }

}
