package org.acme.services.orders;

import org.acme.models.orders.OrderProductState;

import javax.enterprise.context.ApplicationScoped;
import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
public final class OrderProductStateService {

    private static final class AvailableChange {
        private final OrderProductState from;
        private final OrderProductState to;

        AvailableChange(OrderProductState from, OrderProductState to) {
            this.from = from;
            this.to = to;
        }
    }

    private static OrderProductStateService instance;
    private final List<AvailableChange> availableChanges;

    private OrderProductStateService() {
        availableChanges = new ArrayList<>();
        availableChanges.add(new AvailableChange(OrderProductState.TO_REALIZATION, OrderProductState.PAID));
        availableChanges.add(new AvailableChange(OrderProductState.TO_REALIZATION, OrderProductState.CANCELED));
        availableChanges.add(new AvailableChange(OrderProductState.CANCELED, OrderProductState.FINISHED));
        availableChanges.add(new AvailableChange(OrderProductState.PAID, OrderProductState.IN_OFFICE_BOX));
        availableChanges.add(new AvailableChange(OrderProductState.IN_OFFICE_BOX, OrderProductState.RECEIVED));
        availableChanges.add(new AvailableChange(OrderProductState.IN_OFFICE_BOX, OrderProductState.FINISHED));
        availableChanges.add(new AvailableChange(OrderProductState.RECEIVED, OrderProductState.NOT_DAMAGED));
        availableChanges.add(new AvailableChange(OrderProductState.RECEIVED, OrderProductState.DAMAGED));
        availableChanges.add(new AvailableChange(OrderProductState.NOT_DAMAGED, OrderProductState.BACK_TO_OFFICE_BOX));
        availableChanges.add(new AvailableChange(OrderProductState.BACK_TO_OFFICE_BOX, OrderProductState.BACK_TO_OFFICE));
        availableChanges.add(new AvailableChange(OrderProductState.BACK_TO_OFFICE, OrderProductState.REFUND));
        availableChanges.add(new AvailableChange(OrderProductState.REFUND, OrderProductState.FINISHED));
        availableChanges.add(new AvailableChange(OrderProductState.DAMAGED, OrderProductState.IN_OFFICE_BOX_COMPLAINT));
        availableChanges.add(new AvailableChange(OrderProductState.IN_OFFICE_BOX_COMPLAINT, OrderProductState.COMPLAINT_FOR_CONSIDERATION));
        availableChanges.add(new AvailableChange(OrderProductState.COMPLAINT_FOR_CONSIDERATION, OrderProductState.COMPLAINT_ACCEPTED));
        availableChanges.add(new AvailableChange(OrderProductState.COMPLAINT_FOR_CONSIDERATION, OrderProductState.COMPLAINT_NOT_ACCEPTED));
        availableChanges.add(new AvailableChange(OrderProductState.COMPLAINT_ACCEPTED, OrderProductState.REFUND));
        availableChanges.add(new AvailableChange(OrderProductState.COMPLAINT_NOT_ACCEPTED, OrderProductState.FINISHED));
    }

    public static OrderProductStateService getInstance() {
        if (instance == null) {
            instance = new OrderProductStateService();
        }
        return instance;
    }

    public boolean isAvailableStatusChange(OrderProductState currentState, OrderProductState expectedState) {
        return availableChanges.stream()
                .anyMatch(availableChange -> availableChange.from == currentState && availableChange.to == expectedState);
    }

}
