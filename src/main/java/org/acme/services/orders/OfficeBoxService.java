package org.acme.services.orders;

import lombok.AllArgsConstructor;
import org.acme.dto.orders.OfficeBoxDTO;
import org.acme.dto.orders.OfficeBoxDailyDetailDTO;
import org.acme.mappers.OfficeBoxMapper;
import org.acme.models.orders.OfficeBox;
import org.acme.models.orders.OrderProduct;
import org.acme.repository.orders.OfficeBoxRepository;
import org.acme.repository.orders.OrderProductRepository;

import javax.enterprise.context.ApplicationScoped;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@AllArgsConstructor
@ApplicationScoped
public class OfficeBoxService {

    OfficeBoxMapper officeBoxMapper;
    OfficeBoxRepository officeBoxRepository;
    OrderProductRepository orderProductRepository;

    public List<OfficeBoxDTO> getOfficeBoxes() {
        return officeBoxRepository.findAll().stream()
                .map(officeBoxMapper::getOfficeBoxDTOFromOfficeBox)
                .collect(Collectors.toList());
    }

    public OfficeBoxDTO addOfficeBox(OfficeBoxDTO officeBoxDTO) {
        OfficeBox officeBox = officeBoxMapper.getOfficeBoxFromOfficeBoxDTO(officeBoxDTO);
        officeBoxRepository.persist(officeBox);
        return officeBoxMapper.getOfficeBoxDTOFromOfficeBox(officeBox);
    }

    public boolean deleteOfficeBox(Long id) {
        return officeBoxRepository.deleteById(id);
    }

    public List<OfficeBoxDailyDetailDTO> getOfficeBoxesStatuses(LocalDate date) {
        List<OrderProduct> orderProductList = orderProductRepository.findAllOrdersWithGivenDate(date);
        List<OfficeBox> officeBoxesList = officeBoxRepository.findAll().stream().collect(Collectors.toList());

        List<OfficeBoxDailyDetailDTO> officeBoxDailyDetailDTOList = new ArrayList<>();

        officeBoxesList.forEach(officeBox -> {
                    OrderProduct orderProduct = findOrderProductForGivenOfficeBox(officeBox.getId(), orderProductList);
                    if (orderProduct != null) {
                        officeBoxDailyDetailDTOList.add(
                                officeBoxMapper.getOfficeBoxDailyDetailDTO(orderProduct, officeBox.getId())
                        );
                    } else {
                        OfficeBoxDailyDetailDTO officeBoxDailyDetailDTO = new OfficeBoxDailyDetailDTO();
                        officeBoxDailyDetailDTO.setOfficeBoxID(officeBox.getId());
                        officeBoxDailyDetailDTOList.add(officeBoxDailyDetailDTO);
                    }
                }
        );

        return officeBoxDailyDetailDTOList;
    }

    private OrderProduct findOrderProductForGivenOfficeBox(Long officeBoxID, List<OrderProduct> orderProductList) {
        for (OrderProduct orderProduct : orderProductList)
            if (Objects.equals(orderProduct.getOfficeBoxStart().getId(), officeBoxID) ||
                    Objects.equals(orderProduct.getOfficeBoxEnd().getId(), officeBoxID)) {
                return orderProduct;
            }
        return null;
    }

}