package org.acme.services.orders;

import io.vertx.ext.web.handler.impl.HttpStatusException;
import lombok.AllArgsConstructor;
import org.acme.dto.orders.ComplaintPhotoDTO;
import org.acme.models.orders.Complaint;
import org.acme.models.orders.ComplaintPhoto;
import org.acme.repository.orders.ComplaintPhotoRepository;
import org.apache.http.HttpStatus;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@ApplicationScoped
public class ComplaintPhotoService {

    ComplaintPhotoRepository complaintPhotoRepository;

    public ComplaintPhotoDTO getComplaintPhotos(Long complaintID) {
        ComplaintPhotoDTO complaintPhotoDTO = new ComplaintPhotoDTO();
        List<ComplaintPhoto> complaintPhotoList = complaintPhotoRepository
                .findByComplaintIdOptional(complaintID)
                .orElseThrow(() -> new HttpStatusException(HttpStatus.SC_NOT_FOUND));

        complaintPhotoDTO.setImageUrls(complaintPhotoList.stream()
                .map(ComplaintPhoto::getUrl)
                .collect(Collectors.toList()));

        return complaintPhotoDTO;
    }

    public void createComplaintPhoto(Complaint complaint, String url) {
        ComplaintPhoto complaintPhoto = new ComplaintPhoto();
        complaintPhoto.setComplaint(complaint);
        complaintPhoto.setUrl(url);
        complaintPhotoRepository.persist(complaintPhoto);
    }

    public boolean deleteComplaintPhoto(Long id) {
        return complaintPhotoRepository.deleteById(id);
    }

}
