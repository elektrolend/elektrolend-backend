package org.acme.services.orders;

import io.vertx.ext.web.handler.impl.HttpStatusException;
import lombok.AllArgsConstructor;
import org.acme.dto.orders.ComplaintCreateDTO;
import org.acme.dto.orders.ComplaintDTO;
import org.acme.dto.orders.ComplaintRefundDTO;
import org.acme.dto.orders.OrderProductDTO;
import org.acme.mappers.ComplaintMapper;
import org.acme.models.orders.Complaint;
import org.acme.models.orders.OrderProductState;
import org.acme.models.orders.ServiceSecurityType;
import org.acme.repository.orders.ComplaintPhotoRepository;
import org.acme.repository.orders.ComplaintRepository;
import org.acme.repository.orders.OrderProductRepository;
import org.apache.http.HttpStatus;

import javax.enterprise.context.ApplicationScoped;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@ApplicationScoped
public class ComplaintService {

    ComplaintRepository complaintRepository;
    ComplaintMapper complaintMapper;
    ComplaintPhotoRepository complaintPhotoRepository;
    ComplaintPhotoService complaintPhotoService;
    OrderProductService orderProductService;
    OrderProductRepository orderProductRepository;
    OrderService orderService;
    RefundService refundService;

    public List<ComplaintDTO> getAllComplaints() {
        return complaintRepository.findAll().stream()
                .map(complaint -> complaintMapper.getComplaintDTOFromComplaint(complaint, complaint.getOrderProduct()))
                .collect(Collectors.toList());
    }

    public ComplaintDTO getComplaintById(Long id) {
        Complaint complaint = complaintRepository
                .findByIdOptional(id)
                .orElseThrow(() -> new HttpStatusException(HttpStatus.SC_NOT_FOUND));

        return complaintMapper.getComplaintDTOFromComplaint(complaint, complaint.getOrderProduct());
    }

    public ComplaintDTO createComplaint(ComplaintCreateDTO complaintCreateDTO) {
        OrderProductDTO orderProduct = orderProductService.changeReturnInformation(complaintCreateDTO.getOrderProductID());
        Complaint complaint = complaintMapper.getComplaintFromComplaintCreateDTO(complaintCreateDTO);
        complaint.setDate(LocalDate.now());
        complaint.setIsConsidered(false);
        complaintRepository.persist(complaint);

        orderProductService.changeOrderProductStatus(orderProduct.getId(), OrderProductState.DAMAGED);
        List<String> imageUrls = complaintCreateDTO.getImageUrls();
        imageUrls.forEach(url -> complaintPhotoService.createComplaintPhoto(complaint, url));

        return complaintMapper.getComplaintDTOFromComplaint(complaint, complaint.getOrderProduct());
    }

    public boolean deleteComplaint(Long id) {
        return complaintRepository.deleteById(id);
    }

    public ComplaintDTO considerComplaint(Long complaintID, ComplaintRefundDTO complaintRefundDTO) {
        Complaint complaint = complaintRepository
                .findByIdOptional(complaintID)
                .orElseThrow((() -> new HttpStatusException(HttpStatus.SC_NOT_FOUND)));

        if (complaintRefundDTO.getRefund() != null) {
            if (!isRefundAppropriate(complaint, complaintRefundDTO.getRefund())) {
                throw new HttpStatusException(HttpStatus.SC_CONFLICT);
            }
            complaint.setRefund(complaintRefundDTO.getRefund());

            refundService.addRefund(
                    complaintRefundDTO.getRefund(),
                    complaint.getOrderProduct().getId());

            orderProductService.changeOrderProductStatus(
                    complaint.getOrderProduct().getId(),
                    OrderProductState.COMPLAINT_ACCEPTED);
        } else {
            orderProductService.changeOrderProductStatus(
                    complaint.getOrderProduct().getId(),
                    OrderProductState.COMPLAINT_NOT_ACCEPTED);
        }

        complaint.setIsConsidered(true);
        complaintRepository.persist(complaint);

        return complaintMapper.getComplaintDTOFromComplaint(complaint, complaint.getOrderProduct());
    }

    boolean isRefundAppropriate(Complaint complaint, int refund) {
        Integer price = complaint.getOrderProduct().getPrice();
        if (complaint.getOrderProduct().getServiceSecurityType() == ServiceSecurityType.INSURANCE) {
            price -= complaint.getOrderProduct().getProduct().getInsurance();
        }
        return refund <= price;
    }

}
