package org.acme.services.accounts;

import io.vertx.ext.web.handler.impl.HttpStatusException;
import lombok.AllArgsConstructor;
import org.acme.dto.accounts.StateDTO;
import org.acme.mappers.StateMapper;
import org.acme.models.accounts.State;
import org.acme.repository.accounts.StateRepository;
import org.apache.http.HttpStatus;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@ApplicationScoped
public class StateService {

    StateMapper stateMapper;
    StateRepository stateRepository;

    public List<StateDTO> getStates() {
        return stateRepository.findAll().stream()
                .map(stateMapper::getStateDTOFromState)
                .collect(Collectors.toList());
    }

    public StateDTO addState(StateDTO stateDTO) {
        State state = stateMapper.getStateFromStateDTO(stateDTO);
        stateRepository.persist(state);
        return stateMapper.getStateDTOFromState(state);
    }

    public StateDTO getById(Long id) {
        return stateMapper.getStateDTOFromState(stateRepository.findByIdOptional(id)
                .orElseThrow(() -> new HttpStatusException(HttpStatus.SC_NOT_FOUND)));
    }

    public boolean deleteState(Long id) {
        return stateRepository.deleteById(id);
    }

}
