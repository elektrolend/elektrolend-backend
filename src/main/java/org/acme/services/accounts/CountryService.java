package org.acme.services.accounts;

import io.vertx.ext.web.handler.impl.HttpStatusException;
import lombok.AllArgsConstructor;
import org.acme.dto.accounts.CountryDTO;
import org.acme.mappers.CountryMapper;
import org.acme.models.accounts.Country;
import org.acme.repository.accounts.CountryRepository;
import org.apache.http.HttpStatus;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@ApplicationScoped
public class CountryService {

    CountryMapper countryMapper;
    CountryRepository countryRepository;

    public List<CountryDTO> getCountries() {
        return countryRepository.findAll().stream()
                .map(countryMapper::getCountryDTOFromCountry)
                .collect(Collectors.toList());
    }

    public CountryDTO addCountry(CountryDTO countryDTO) {
        Country country = countryMapper.getCountryFromCountryDTO(countryDTO);
        countryRepository.persist(country);
        return countryMapper.getCountryDTOFromCountry(country);
    }

    public CountryDTO getById(Long id) {
        return countryMapper.getCountryDTOFromCountry(countryRepository.findByIdOptional(id)
                .orElseThrow(() -> new HttpStatusException(HttpStatus.SC_NOT_FOUND)));
    }

    public boolean deleteCountry(Long id) {
        return countryRepository.deleteById(id);
    }

}
