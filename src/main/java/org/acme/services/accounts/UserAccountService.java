package org.acme.services.accounts;

import io.vertx.ext.web.handler.impl.HttpStatusException;
import lombok.AllArgsConstructor;
import org.acme.dto.PaginationDTO;
import org.acme.dto.accounts.EmployeeDetailsDTO;
import org.acme.dto.accounts.UserAccountDTO;
import org.acme.dto.accounts.UserAccountUpdateDTO;
import org.acme.dto.accounts.UserDetailsDTO;
import org.acme.dto.accounts.UserDetailsUpdateDTO;
import org.acme.dto.accounts.UserLoggedDTO;
import org.acme.dto.accounts.UserLoginDTO;
import org.acme.dto.orders.OrderInfoDTO;
import org.acme.dto.orders.UserOrderDTO;
import org.acme.dto.orders.UserOrderWithPaginationDTO;
import org.acme.mappers.EmployeeMapper;
import org.acme.mappers.OrderProductMapper;
import org.acme.mappers.PaginationMapper;
import org.acme.mappers.UserAccountMapper;
import org.acme.models.accounts.Customer;
import org.acme.models.accounts.Employee;
import org.acme.models.accounts.State;
import org.acme.models.accounts.UserAccount;
import org.acme.models.orders.Order;
import org.acme.models.orders.OrderProduct;
import org.acme.models.orders.OrderProductState;
import org.acme.pagination.PageRequest;
import org.acme.repository.accounts.CustomerRepository;
import org.acme.repository.accounts.EmployeeRepository;
import org.acme.repository.accounts.StateRepository;
import org.acme.repository.accounts.UserAccountRepository;
import org.acme.repository.orders.OrderProductRepository;
import org.acme.repository.orders.OrderRepository;
import org.acme.services.products.ProductService;
import org.acme.utils.PasswordUtil;
import org.apache.http.HttpStatus;

import javax.enterprise.context.ApplicationScoped;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@AllArgsConstructor
@ApplicationScoped
public class UserAccountService {

    UserAccountMapper userAccountMapper;
    UserAccountRepository userAccountRepository;
    StateRepository stateRepository;
    CustomerRepository customerRepository;
    OrderRepository orderRepository;
    OrderProductRepository orderProductRepository;
    OrderProductMapper orderProductMapper;
    CustomerService customerService;
    EmployeeRepository employeeRepository;
    PaginationMapper paginationMapper;
    ProductService productService;
    EmployeeMapper employeeMapper;

    public List<UserAccountDTO> getUserAccounts() {
        return userAccountRepository.findAll().stream()
                .map(userAccountMapper::getUserAccountDTOFromUserAccount)
                .collect(Collectors.toList());
    }

    public EmployeeDetailsDTO getById(Long id) {
        Optional<Customer> customer = customerRepository.findCustomerByUserIdOptional(id);
        if (customer.isPresent()) {
            return userAccountMapper.getUserDetailsDTOFromCustomer(customer.get());
        }

        Optional<Employee> employee = employeeRepository.findEmployeeByUserIdOptional(id);
        if (employee.isPresent()) {
            return employeeMapper.fromEmployee(employee.get());
        }

        throw new HttpStatusException(HttpStatus.SC_NO_CONTENT);
    }

    public UserDetailsDTO getByNickname(String nickname) {
        return userAccountMapper.getUserDetailsDTOFromCustomer(
                customerRepository.findByNicknameOptional(nickname)
                        .orElseThrow(() -> new HttpStatusException(HttpStatus.SC_NOT_FOUND))
        );
    }

    public boolean deleteUserAccount(Long id) {
        return userAccountRepository.deleteById(id);
    }

    public UserDetailsDTO updateUserDetails(Long id, UserDetailsUpdateDTO userDetailsUpdateDTO) {
        Customer customer = customerRepository.findCustomerByUserIdOptional(id)
                .orElseThrow(() -> new HttpStatusException(HttpStatus.SC_NOT_FOUND));


        Customer updatedCustomer = updateCustomer(customer, userDetailsUpdateDTO);
        return userAccountMapper.getUserDetailsDTOFromCustomer(updatedCustomer);
    }

    public UserAccountDTO updateUserAccount(Long id, UserAccountUpdateDTO userAccountUpdateDTO) {
        UserAccount userAccount = userAccountRepository.findById(id);
        if (userAccount == null) {
            throw new HttpStatusException(HttpStatus.SC_NOT_FOUND);
        }
        String newNickname = userAccountUpdateDTO.getNickname();
        String newPassword = userAccountUpdateDTO.getPassword();
        if (newNickname != null) {
            if (userAccountRepository.findByNameOptional(newNickname).isPresent()) {
                throw new HttpStatusException(HttpStatus.SC_CONFLICT);
            }
            userAccount.setNickname(newNickname);
        }
        if (newPassword != null) {
            if (PasswordUtil.isPasswordTooWeak(newPassword)) {
                throw new HttpStatusException(HttpStatus.SC_BAD_REQUEST);
            }
            PasswordUtil.hashPassword(userAccount, newPassword);
        }

        userAccountRepository.persist(userAccount);
        return userAccountMapper.getUserAccountDTOFromUserAccount(userAccount);
    }

    public UserLoggedDTO logUser(UserLoginDTO userLoginDTO) {
        UserAccount userAccount = userAccountRepository
                .findByNameOptional(userLoginDTO.getNickname())
                .orElseThrow(() -> new HttpStatusException(HttpStatus.SC_UNAUTHORIZED));

        if (!PasswordUtil.isPasswordCorrect(userAccount, userLoginDTO.getPassword())) {
            throw new HttpStatusException(HttpStatus.SC_UNAUTHORIZED);
        }

        if (Boolean.FALSE.equals(userAccount.getIsEmailVerified())
                && Objects.isNull(userLoginDTO.getRegistrationCode())) {
            throw new HttpStatusException(HttpStatus.SC_NOT_ACCEPTABLE);
        }

        if (Boolean.FALSE.equals(userAccount.getIsEmailVerified())
                && !Objects.equals(userLoginDTO.getRegistrationCode(), userAccount.getRegisterCode())) {
            throw new HttpStatusException(HttpStatus.SC_CONFLICT);
        }

        if (Boolean.FALSE.equals(userAccount.getIsEmailVerified())
                && Objects.equals(userLoginDTO.getRegistrationCode(), userAccount.getRegisterCode())) {
            userAccount.setIsEmailVerified(true);
        }

        UserLoggedDTO userLogged = userAccountMapper.getUserLoggedDTOFromUserLoginDTO(userLoginDTO);
        Long userId = userAccount.getId();
        userLogged.setId(userId);
        Optional<Employee> optionalEmployee = employeeRepository.findEmployeeByUserIdOptional(userId);
        if (optionalEmployee.isPresent()) {
            userLogged.setAccountType(optionalEmployee.get().employeeType.toString());
        } else {
            userLogged.setAccountType("CUSTOMER");
        }
        return userLogged;
    }

    public UserOrderWithPaginationDTO getUserOrders(Long id, PageRequest pageRequest, OrderProductState orderProductState) {
        if (pageRequest.getPageSize() < 1 || pageRequest.getPageNum() < 1) {
            throw new HttpStatusException((HttpStatus.SC_BAD_REQUEST));
        }
        if (userAccountRepository.findByIdOptional(id).isEmpty()) {
            throw new HttpStatusException((HttpStatus.SC_NOT_FOUND));
        }
        List<UserOrderDTO> userOrdersDTOList = new ArrayList<>();
        List<Order> customerOrders = orderRepository.findOrdersByCustomerId(id);
        if (customerOrders.isEmpty()) {
            throw new HttpStatusException(HttpStatus.SC_NO_CONTENT);
        }
        customerOrders.forEach(order -> {
            List<OrderProduct> orderProducts = orderProductRepository.findByOrderID(order.id);
            if (orderProductState != null) {
                orderProducts = filterOrderProducts(orderProducts, orderProductState);
            }
            List<OrderInfoDTO> orderInfoDTOs = new ArrayList<>();
            orderProducts.forEach(orderProduct ->
                    orderInfoDTOs.add(orderProductMapper.getOrderInfoDTOFromOrderProduct(orderProduct)));

            if (!orderInfoDTOs.isEmpty()) {
                orderInfoDTOs.forEach(orderInfoDTO ->
                        orderInfoDTO.setOrderProductPrice(productService.calculatePrice(orderInfoDTO)));

                userOrdersDTOList.add(orderProductMapper.getUserOrderDTOFromOrderInfoDTO(order, orderInfoDTOs));
            }
        });
        int userOrdersDTOListSize = userOrdersDTOList.size();
        int beginOfRequest = (pageRequest.getPageNum() - 1) * pageRequest.getPageSize();
        int endOfRequest = pageRequest.getPageNum() * pageRequest.getPageSize();
        List<UserOrderDTO> userOrderDTOList;
        if (userOrdersDTOListSize < beginOfRequest) {
            userOrderDTOList = Collections.emptyList();
        } else {
            userOrderDTOList = userOrdersDTOList.subList(beginOfRequest, Math.min(endOfRequest, userOrdersDTOListSize));
        }
        PaginationDTO paginationDTO = paginationMapper.getPaginationDTOFromPageRequest(pageRequest, userOrdersDTOListSize);
        return new UserOrderWithPaginationDTO(userOrderDTOList, paginationDTO);
    }

    private List<OrderProduct> filterOrderProducts(List<OrderProduct> orderProducts, OrderProductState orderProductState) {
        return orderProducts.stream()
                .filter(orderProduct -> orderProduct.orderProductState.equals(orderProductState))
                .collect(Collectors.toList());
    }

    private Customer updateCustomer(Customer customer, UserDetailsUpdateDTO userDetailsUpdateDTO) {
        customer.setStreet(userDetailsUpdateDTO.getStreet());
        customer.setHomeNumber(Integer.parseInt(userDetailsUpdateDTO.getHomeNumber()));
        customer.setLocalNumber(Integer.parseInt(userDetailsUpdateDTO.getLocalNumber()));
        customer.setZipCode(userDetailsUpdateDTO.getZipCode());
        customer.setCity(userDetailsUpdateDTO.getCity());
        customer.setEmail(userDetailsUpdateDTO.getEmail());
        customer.setPhone(userDetailsUpdateDTO.getPhone());
        customer.setName(userDetailsUpdateDTO.getName());
        customer.setSurname(userDetailsUpdateDTO.getSurname());

        State state = stateRepository.findByNameOptional(userDetailsUpdateDTO.getState()).orElse(null);
        customer.setState(state);
        customerRepository.persist(customer);

        return customer;
    }

}
