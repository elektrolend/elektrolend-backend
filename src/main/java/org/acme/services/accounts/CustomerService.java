package org.acme.services.accounts;

import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.panache.common.Page;
import io.vertx.ext.web.handler.impl.HttpStatusException;
import lombok.AllArgsConstructor;
import org.acme.dto.PaginationDTO;
import org.acme.dto.accounts.CustomerDTO;
import org.acme.dto.accounts.CustomerNewPasswordDTO;
import org.acme.dto.accounts.CustomerPasswordRestoringEmailDTO;
import org.acme.dto.accounts.CustomersDTOWithPagination;
import org.acme.mappers.CustomerMapper;
import org.acme.mappers.PaginationMapper;
import org.acme.mappers.UserAccountMapper;
import org.acme.models.accounts.Customer;
import org.acme.models.accounts.UserAccount;
import org.acme.pagination.PageRequest;
import org.acme.repository.accounts.CustomerRepository;
import org.acme.repository.accounts.UserAccountRepository;
import org.acme.utils.MailSenderUtil;
import org.acme.utils.PasswordUtil;
import org.apache.http.HttpStatus;

import javax.enterprise.context.ApplicationScoped;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@ApplicationScoped
@AllArgsConstructor
public class CustomerService {

    CustomerMapper customerMapper;
    CustomerRepository customerRepository;
    UserAccountMapper userAccountMapper;
    UserAccountRepository userAccountRepository;
    PaginationMapper paginationMapper;
    MailSenderUtil mailSender;

    public CustomersDTOWithPagination getCustomers(PageRequest pageRequest) {
        PanacheQuery<Customer> customerPanacheQuery = customerRepository.findAll();
        int totalResults = (int) customerPanacheQuery.stream().count();

        List<CustomerDTO> customerDTOList = customerPanacheQuery
                .page(Page.of(pageRequest.getPageNum() - 1, pageRequest.getPageSize()))
                .stream()
                .map(customer -> customerMapper.getCustomerDTOFromCustomer(customer))
                .collect(Collectors.toList());

        PaginationDTO paginationDTO = paginationMapper.getPaginationDTOFromPageRequest(pageRequest, totalResults);

        return new CustomersDTOWithPagination(customerDTOList, paginationDTO);
    }

    public CustomerDTO addCustomer(CustomerDTO customerDTO) {
        if (customerRepository.findByNicknameOptional(customerDTO.getNickname()).isPresent()) {
            throw new HttpStatusException(HttpStatus.SC_CONFLICT);
        }
        UserAccount userAccount = new UserAccount();
        userAccount.setNickname(customerDTO.getNickname());
        PasswordUtil.hashPassword(userAccount, customerDTO.getPassword());
        String registrationCode = codeGenerator();
        userAccount.setRegisterCode(registrationCode);
        userAccount.setIsEmailVerified(false);
        userAccount.setRole("customer");
        userAccountRepository.persist(userAccount);

        Customer customer = customerMapper.getCustomerFromCustomerDTO(customerDTO);
        customer.userAccount = userAccount;
        customerRepository.persist(customer);

        mailSender.sendRegistrationEmail(customer.getEmail(), registrationCode);
        return customerMapper.getCustomerDTOFromCustomer(customer);
    }

    public CustomerDTO getById(Long id) {
        return customerMapper.getCustomerDTOFromCustomer(customerRepository.findByIdOptional(id)
                .orElseThrow(() -> new HttpStatusException(HttpStatus.SC_NOT_FOUND)));
    }

    public boolean deleteCustomer(Long id) {
        return customerRepository.deleteById(id);
    }

    private String codeGenerator() {
        String chrs = "0123456789abcdefghijklmnopqrstuvwxyz-_ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        try {
            SecureRandom secureRandom = SecureRandom.getInstanceStrong();
            return secureRandom.ints(6, 0, chrs.length())
                    .mapToObj(chrs::charAt)
                    .collect(StringBuilder::new, StringBuilder::append, StringBuilder::append).toString();
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException(e.getMessage(), e);
        }
    }

    public void sendEmailPasswordRestoring(CustomerPasswordRestoringEmailDTO customerPasswordRestoringEmailDTO) {
        Customer customer = customerRepository.findByEmailOptional(customerPasswordRestoringEmailDTO.getEmail())
                .orElseThrow(() -> new HttpStatusException(HttpStatus.SC_NOT_FOUND));
        String code = codeGenerator();
        customer.getUserAccount().setRestoringCode(code);
        mailSender.sendRestoringPasswordEmail(customerPasswordRestoringEmailDTO.getEmail(), code);
    }

    public void setNewPassword(CustomerNewPasswordDTO customerNewPasswordDTO) {
        if ((Objects.isNull(customerNewPasswordDTO.getEmail()) && Objects.isNull(customerNewPasswordDTO.getNickname()))
                || Objects.isNull(customerNewPasswordDTO.getPassword()) || Objects.isNull(customerNewPasswordDTO.getCode())) {
            throw new HttpStatusException(HttpStatus.SC_CONFLICT);
        }
        Optional<Customer> customer = Optional.empty();
        Optional<UserAccount> userAccount = Optional.empty();
        if (Objects.nonNull(customerNewPasswordDTO.getEmail())) {
            customer = customerRepository.findByEmailOptional(customerNewPasswordDTO.getEmail());
        }
        if (Objects.nonNull(customerNewPasswordDTO.getNickname())) {
            userAccount = userAccountRepository.findByNameOptional(customerNewPasswordDTO.getNickname());
        }
        if (customer.isEmpty() && userAccount.isEmpty()) {
            throw new HttpStatusException(HttpStatus.SC_NOT_FOUND);
        }
        UserAccount userAccountExpected = userAccount.isPresent() ? userAccount.get() : customer.get().getUserAccount();
        if (!Objects.equals(userAccountExpected.getRestoringCode(), customerNewPasswordDTO.getCode())) {
            throw new HttpStatusException(HttpStatus.SC_UNAUTHORIZED);
        }
        if (PasswordUtil.isPasswordTooWeak(customerNewPasswordDTO.getPassword())) {
            throw new HttpStatusException(HttpStatus.SC_NOT_ACCEPTABLE);
        }
        PasswordUtil.hashPassword(userAccountExpected, customerNewPasswordDTO.getPassword());
        userAccountExpected.setRestoringCode(null);
    }
}
