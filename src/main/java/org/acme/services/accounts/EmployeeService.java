package org.acme.services.accounts;

import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.panache.common.Page;
import io.vertx.ext.web.handler.impl.HttpStatusException;
import lombok.AllArgsConstructor;
import org.acme.dto.PaginationDTO;
import org.acme.dto.accounts.EmployeeDTO;
import org.acme.dto.accounts.EmployeesDTOWithPagination;
import org.acme.mappers.EmployeeMapper;
import org.acme.mappers.PaginationMapper;
import org.acme.mappers.UserAccountMapper;
import org.acme.models.accounts.Employee;
import org.acme.models.accounts.UserAccount;
import org.acme.pagination.PageRequest;
import org.acme.repository.accounts.EmployeeRepository;
import org.acme.repository.accounts.UserAccountRepository;
import org.acme.utils.PasswordUtil;
import org.apache.http.HttpStatus;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@ApplicationScoped
public class EmployeeService {

    EmployeeMapper employeeMapper;
    EmployeeRepository employeeRepository;
    UserAccountRepository userAccountRepository;
    UserAccountMapper userAccountMapper;
    PaginationMapper paginationMapper;

    public EmployeesDTOWithPagination getEmployees(PageRequest pageRequest) {
        PanacheQuery<Employee> employeePanacheQuery = employeeRepository.findAll();
        int totalResults = (int) employeePanacheQuery.stream().count();

        List<EmployeeDTO> employeeDTOList = employeePanacheQuery
                .page(Page.of(pageRequest.getPageNum() - 1, pageRequest.getPageSize()))
                .stream()
                .map(employee -> employeeMapper.getEmployeeDTOFromEmployee(employee))
                .collect(Collectors.toList());

        PaginationDTO paginationDTO = paginationMapper.getPaginationDTOFromPageRequest(pageRequest, totalResults);

        return new EmployeesDTOWithPagination(employeeDTOList, paginationDTO);
    }

    public EmployeeDTO addEmployee(EmployeeDTO employeeDTO) {
        if (employeeRepository.findByNicknameOptional(employeeDTO.getNickname()).isPresent()) {
            throw new HttpStatusException(HttpStatus.SC_CONFLICT);
        }

        UserAccount userAccount = new UserAccount();
        userAccount.setNickname(employeeDTO.getNickname());
        PasswordUtil.hashPassword(userAccount, employeeDTO.getPassword());
        userAccount.setIsEmailVerified(true);
        userAccount.setRole(employeeDTO.getEmployeeType().toString().toLowerCase());
        userAccountRepository.persist(userAccount);

        Employee employee = employeeMapper.getEmployeeFromEmployeeDTO(employeeDTO);
        employee.userAccount = userAccount;
        employeeRepository.persist(employee);
        return employeeMapper.getEmployeeDTOFromEmployee(employee);
    }

    public EmployeeDTO getById(Long id) {
        return employeeMapper.getEmployeeDTOFromEmployee(employeeRepository.findByIdOptional(id)
                .orElseThrow(() -> new HttpStatusException(HttpStatus.SC_NOT_FOUND)));
    }

    public boolean deleteEmployee(Long id) {
        return employeeRepository.deleteById(id);
    }

}
