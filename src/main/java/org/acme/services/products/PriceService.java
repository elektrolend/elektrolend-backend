package org.acme.services.products;

import lombok.AllArgsConstructor;
import org.acme.dto.products.PriceDTO;
import org.acme.mappers.PriceMapper;
import org.acme.models.products.Price;
import org.acme.repository.products.PriceRepository;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@ApplicationScoped
public class PriceService {

    PriceMapper priceMapper;
    PriceRepository priceRepository;

    public List<PriceDTO> getPrices() {
        return priceRepository.listAll().stream()
                .map(priceMapper::getPriceDTOFromPrice)
                .collect(Collectors.toList());
    }

    public PriceDTO addPrice(PriceDTO priceDTO) {
        Price price = priceMapper.getPriceFromPriceDTO(priceDTO);
        priceRepository.persist(price);
        return priceMapper.getPriceDTOFromPrice(price);
    }

    public boolean deletePrice(Long id) {
        return priceRepository.deleteById(id);
    }

}
