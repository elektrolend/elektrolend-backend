package org.acme.services.products;

import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.panache.common.Page;
import io.vertx.ext.web.handler.impl.HttpStatusException;
import lombok.AllArgsConstructor;
import org.acme.dto.PaginationDTO;
import org.acme.dto.orders.OrderInfoDTO;
import org.acme.dto.products.ProductCreateUpdateDTO;
import org.acme.dto.products.ProductDTO;
import org.acme.dto.products.ProductsDTOWithPagination;
import org.acme.mappers.PaginationMapper;
import org.acme.mappers.ProductMapper;
import org.acme.models.orders.OrderProductState;
import org.acme.models.orders.ServiceSecurityType;
import org.acme.models.products.Category;
import org.acme.models.products.Price;
import org.acme.models.products.Product;
import org.acme.pagination.PageRequest;
import org.acme.repository.products.CategoryRepository;
import org.acme.repository.products.ProductRepository;
import org.acme.utils.DateUtil;
import org.acme.utils.StringUtil;
import org.apache.http.HttpStatus;

import javax.enterprise.context.ApplicationScoped;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.stream.Collectors;

import static java.time.temporal.ChronoUnit.DAYS;

@AllArgsConstructor
@ApplicationScoped
public class ProductService {

    ProductMapper productMapper;
    ProductRepository productRepository;
    PaginationMapper paginationMapper;
    CategoryRepository categoryRepository;

    public ProductsDTOWithPagination getProducts(PageRequest pageRequest, Long categoryID, Boolean availability) {
        Category category = categoryID != null ? categoryRepository.findByIdOptional(categoryID).orElse(null) : null;
        if (category == null && categoryID != null) {
            return new ProductsDTOWithPagination(Collections.emptyList(), paginationMapper.getPaginationDTOFromPageRequest(pageRequest, 0));
        }

        PanacheQuery<Product> productPanacheQuery = categoryID == null ?
                productRepository.findProductsByAvailability(availability)
                : productRepository.findByCategoryIdAndAvailabilityOptional(categoryID, availability);

        int totalResults = (int) productPanacheQuery
                .stream()
                .count();

        List<ProductDTO> productDTOList = productPanacheQuery
                .page(Page.of(pageRequest.getPageNum() - 1, pageRequest.getPageSize()))
                .stream()
                .map(product -> productMapper.getProductDTOFromProduct(product))
                .collect(Collectors.toList());

        PaginationDTO paginationDTO = paginationMapper.getPaginationDTOFromPageRequest(pageRequest, totalResults);

        return new ProductsDTOWithPagination(productDTOList, paginationDTO);
    }

    public ProductsDTOWithPagination searchProducts(
            String productName,
            Long priceFrom,
            Long priceTo,
            Long categoryID,
            Boolean sortDescending,
            Boolean availability,
            PageRequest pageRequest) {

        List<Product> foundProducts = productRepository.findAll()
                .stream()
                .filter(product -> {
                    if (categoryID != null) {
                        return product.getCategory().getId().equals(categoryID);
                    }
                    return true;
                })
                .filter(product -> {
                    if (productName != null) {
                        return StringUtil.containsIgnoreCase(product.getName(), productName);
                    }
                    return true;
                })
                .filter(product -> {
                    if (availability != null) {
                        return Objects.equals(product.getIsAvailable(), availability);
                    }
                    return true;
                })
                .filter(product -> {
                    if (priceFrom != null) {
                        Integer productPrice = findLastPrice(product);
                        if (productPrice != null) {
                            return priceFrom <= productPrice;
                        }
                        return true;
                    }
                    return true;
                })
                .filter(product -> {
                    if (priceTo != null) {
                        Integer productPrice = findLastPrice(product);
                        if (productPrice != null) {
                            return productPrice <= priceTo;
                        }
                        return true;
                    }
                    return true;
                })
                .collect(Collectors.toList());

        foundProducts = foundProducts
                .stream()
                .sorted(Comparator.comparing(this::findLastPrice))
                .collect(Collectors.toList());

        if (Boolean.FALSE.equals(sortDescending)) {
            Collections.reverse(foundProducts);
        }

        int foundProductsSize = foundProducts.size();
        int beginOfRequest = (pageRequest.getPageNum() - 1) * pageRequest.getPageSize();
        int endOfRequest = pageRequest.getPageNum() * pageRequest.getPageSize();
        List<ProductDTO> productDTOList = foundProducts.stream()
                .map(product -> productMapper.getProductDTOFromProduct(product))
                .collect(Collectors.toList());

        if (foundProductsSize < beginOfRequest) {
            productDTOList = Collections.emptyList();
        } else {
            productDTOList = productDTOList.subList(beginOfRequest, Math.min(endOfRequest, foundProductsSize));
        }
        PaginationDTO paginationDTO = paginationMapper.getPaginationDTOFromPageRequest(pageRequest, foundProductsSize);
        return new ProductsDTOWithPagination(productDTOList, paginationDTO);
    }

    private Integer findLastPrice(Product product) {
        return product.priceList
                .stream()
                .filter(price -> DateUtil.isDateFromPastOrToday(price.beginDate))
                .sorted(Comparator.comparing(Price::getBeginDate).reversed())
                .collect(Collectors.toList())
                .stream()
                .findFirst()
                .map(price -> price.pricePerDayBrutto)
                .orElse(null);
    }


    public ProductDTO addProduct(ProductCreateUpdateDTO productCreateUpdateDTO) {
        Product product = productMapper.getProductFromProductCreateUpdateDTO(productCreateUpdateDTO);
        productRepository.persist(product);
        return productMapper.getProductDTOFromProduct(product);
    }

    public ProductDTO getById(Long id) {
        return productMapper.getProductDTOFromProduct(productRepository.findByIdOptional(id)
                .orElseThrow(() -> new HttpStatusException(HttpStatus.SC_NOT_FOUND)));
    }

    public boolean deleteProduct(Long id) {
        return productRepository.deleteById(id);
    }

    public int calculatePrice(OrderInfoDTO orderInfoDTO) {
        Product product = productRepository.findById(orderInfoDTO.getProductID());

        long numberOfDays = DAYS.between(
                orderInfoDTO.getOrderBeginDate().atStartOfDay(),
                orderInfoDTO.getOrderEndDate().atStartOfDay());

        Integer pricePerDay = product
                .priceList
                .stream()
                .sorted(Comparator.comparing(lhs -> lhs.beginDate))
                .collect(Collectors.toList())
                .stream()
                .reduce((firstPrice, secondPrice) -> secondPrice)
                .map(price -> price.pricePerDayBrutto)
                .orElse(null);

        if (pricePerDay == null) {
            throw new NoSuchElementException("Product does not have price");
        }

        int financialSecurity = orderInfoDTO.getServiceSecurityType() == ServiceSecurityType.DEPOSIT ?
                product.deposit : product.insurance;

        return (int) (pricePerDay * numberOfDays) + financialSecurity;
    }

    public OrderProductState startOrder() {
        return OrderProductState.TO_REALIZATION;
    }

    public ProductDTO updateProduct(Long id, ProductCreateUpdateDTO productCreateUpdateDTO) {
        Product product = productRepository.findById(id);
        if (product == null) {
            throw new HttpStatusException(HttpStatus.SC_NOT_FOUND);
        }
        Product updatedProduct = updateProduct(product, productCreateUpdateDTO);
        return productMapper.getProductDTOFromProduct(updatedProduct);
    }

    Product updateProduct(Product product, ProductCreateUpdateDTO productCreateUpdateDTO) {
        Product productToUpdate = productMapper.getProductFromProductCreateUpdateDTO(productCreateUpdateDTO);

        product.setCategory(productToUpdate.getCategory());
        product.setName(productToUpdate.getName());
        product.setDescription(productToUpdate.getDescription());
        product.setSpecificationList(productToUpdate.getSpecificationList());
        product.setDeposit(productToUpdate.getDeposit());
        product.setInsurance(productToUpdate.getInsurance());
        product.setImageUrl(productToUpdate.getImageUrl());

        productRepository.persist(product);
        return product;
    }

    public boolean changeProductAvailability(Long productID, boolean isAvailable) {
        Product product = productRepository.findById(productID);
        if (product == null) {
            return false;
        }
        product.setIsAvailable(isAvailable);
        productRepository.persist(product);
        return true;
    }


}
