package org.acme.services.products;

import io.vertx.ext.web.handler.impl.HttpStatusException;
import lombok.AllArgsConstructor;
import org.acme.dto.products.CategoryDTO;
import org.acme.mappers.CategoryMapper;
import org.acme.models.products.Category;
import org.acme.repository.products.CategoryRepository;
import org.apache.http.HttpStatus;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@ApplicationScoped
public class CategoryService {

    CategoryMapper categoryMapper;
    CategoryRepository categoryRepository;

    public List<CategoryDTO> getCategories() {
        return categoryRepository.findAll().stream()
                .map(categoryMapper::getCategoryDTOFromCategory)
                .collect(Collectors.toList());
    }

    public CategoryDTO addCategory(CategoryDTO categoryDTO) {
        Category category = categoryMapper.getCategoryFromCategoryDTO(categoryDTO);
        categoryRepository.persist(category);
        return categoryMapper.getCategoryDTOFromCategory(category);
    }

    public CategoryDTO getById(Long id) {
        return categoryMapper.getCategoryDTOFromCategory(categoryRepository.findByIdOptional(id)
                .orElseThrow(() -> new HttpStatusException(HttpStatus.SC_NOT_FOUND)));
    }

    public boolean deleteCategory(Long id) {
        return categoryRepository.deleteById(id);
    }

}
