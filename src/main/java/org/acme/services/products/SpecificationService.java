package org.acme.services.products;

import lombok.AllArgsConstructor;
import org.acme.dto.products.SpecificationDTO;
import org.acme.mappers.SpecificationMapper;
import org.acme.models.products.Specification;
import org.acme.repository.products.SpecificationRepository;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@ApplicationScoped
public class SpecificationService {

    SpecificationMapper specificationMapper;
    SpecificationRepository specificationRepository;

    public List<SpecificationDTO> getSpecifications() {
        return specificationRepository.listAll().stream()
                .map(specificationMapper::getSpecificationDTOFromSpecification)
                .collect(Collectors.toList());
    }

    public SpecificationDTO addSpecification(SpecificationDTO specificationDTO) {
        Specification specification = specificationMapper.getSpecificationFromSpecificationDTO(specificationDTO);
        specificationRepository.persist(specification);
        return specificationMapper.getSpecificationDTOFromSpecification(specification);
    }

    public boolean deleteSpecification(Long id) {
        return specificationRepository.deleteById(id);
    }

}

