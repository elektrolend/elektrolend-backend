package org.acme.pagination;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.QueryParam;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class PageRequest {

    @QueryParam("pageNum")
    @DefaultValue("1")
    private int pageNum;

    @QueryParam("pageSize")
    @DefaultValue("10")
    private int pageSize;

}
