package org.acme.repository.orders;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import org.acme.models.orders.ComplaintPhoto;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@ApplicationScoped
public class ComplaintPhotoRepository implements PanacheRepository<ComplaintPhoto> {

    public Optional<List<ComplaintPhoto>> findByComplaintIdOptional(Long complaintID) {
        return Optional.of(
                find("complaintID", complaintID)
                        .stream()
                        .collect(Collectors.toList())
        );
    }

}
