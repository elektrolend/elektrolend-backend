package org.acme.repository.orders;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import org.acme.models.orders.OfficeBox;

import javax.enterprise.context.ApplicationScoped;
import java.util.Optional;

@ApplicationScoped
public class OfficeBoxRepository implements PanacheRepository<OfficeBox> {

    public long countAllOfficeBoxes() {
        return findAll().stream().count();
    }

    public Optional<OfficeBox> findByCodeOptional(String code) {
        return find("code", code).singleResultOptional();
    }

}
