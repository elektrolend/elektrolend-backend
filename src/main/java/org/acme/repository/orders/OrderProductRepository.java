package org.acme.repository.orders;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import io.vertx.ext.web.handler.impl.HttpStatusException;
import org.acme.models.orders.OfficeBox;
import org.acme.models.orders.OrderProduct;
import org.apache.http.HttpStatus;

import javax.enterprise.context.ApplicationScoped;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@ApplicationScoped
public class OrderProductRepository implements PanacheRepository<OrderProduct> {

    OfficeBoxRepository officeBoxRepository = new OfficeBoxRepository();

    public Optional<ArrayList<OrderProduct>> findFutureOrderProductByProductId(Long id) {
        return Optional.of(find("productID", id)
                .list()
                .stream()
                .filter(orderProduct -> orderProduct.orderEndDate.isAfter(LocalDate.now()))
                .collect(Collectors.toCollection(ArrayList::new)));
    }

    public Long findFirstAvailableOfficeBox(LocalDate orderDate) {
        long numberOfOfficeBoxes = officeBoxRepository.countAllOfficeBoxes();

        List<OfficeBox> officeBoxNotAvailableList =
                findAll().stream()
                        .filter(orderProduct -> orderProduct.orderBeginDate.equals(orderDate))
                        .map(OrderProduct::getOfficeBoxStart)
                        .collect(Collectors.toList());

        officeBoxNotAvailableList.addAll(
                findAll().stream()
                        .filter(orderProduct -> orderProduct.orderEndDate.equals(orderDate))
                        .map(OrderProduct::getOfficeBoxEnd)
                        .collect(Collectors.toList())
        );

        if (numberOfOfficeBoxes == 0 || officeBoxNotAvailableList.size() == numberOfOfficeBoxes) {
            throw new HttpStatusException(HttpStatus.SC_CONFLICT);
        }

        if (officeBoxNotAvailableList.isEmpty()) {
            return (long) 1;
        }

        officeBoxNotAvailableList.sort(new SortByBoxId());
        for (int idToCheck = 1; idToCheck <= numberOfOfficeBoxes; idToCheck++) {
            if (officeBoxNotAvailableList.size() >= idToCheck
                    && officeBoxNotAvailableList.get(idToCheck - 1).id != idToCheck) {
                return (long) idToCheck;
            }
            if (idToCheck == officeBoxNotAvailableList.size()
                    && officeBoxNotAvailableList.get(idToCheck - 1).id == idToCheck
                    && idToCheck < numberOfOfficeBoxes) {
                return (long) idToCheck + 1;
            }
        }
        throw new HttpStatusException(HttpStatus.SC_CONFLICT);
    }

    public List<OrderProduct> findAllOrdersWithGivenDate(LocalDate date) {
        return Stream.concat(
                find("orderbegindate", date).stream().collect(Collectors.toList()).stream(),
                find("orderenddate", date).stream().collect(Collectors.toList()).stream()
        ).collect(Collectors.toList());
    }

    private static class SortByBoxId implements Comparator<OfficeBox> {
        @Override
        public int compare(OfficeBox a, OfficeBox b) {
            return (int) (a.id - b.id);
        }
    }

    public List<OrderProduct> findByOrderID(Long id) {
        return findAll().stream()
                .filter(orderProduct -> orderProduct.order.id.equals(id))
                .collect(Collectors.toList());
    }

    public List<OrderProduct> findOrderProductsByOrderId(Long orderID) {
        return find("SELECT op FROM order_product op WHERE orderID = ?1", orderID)
                .stream()
                .collect(Collectors.toList());
    }

}
