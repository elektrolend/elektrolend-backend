package org.acme.repository.orders;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import org.acme.models.orders.Payment;

import javax.enterprise.context.ApplicationScoped;
import java.util.Optional;

@ApplicationScoped
public class PaymentRepository implements PanacheRepository<Payment> {

    public Optional<Payment> findByOrderProductIdOptional(Long orderProductID) {
        return find("""
                    SELECT p FROM payment p
                    JOIN order_table ot ON ot.id = p.order.id
                    JOIN order_product op ON op.order.id = ot.id
                    WHERE op.id = ?1
                """, orderProductID)
                .singleResultOptional();
    }

}