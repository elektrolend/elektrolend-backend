package org.acme.repository.orders;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import org.acme.models.orders.Order;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;
import java.util.stream.Collectors;

@ApplicationScoped
public class OrderRepository implements PanacheRepository<Order> {

    public List<Order> findOrdersByCustomerId(Long id) {
        return findAll().stream()
                .filter(order -> order.customer.userAccount.id.equals(id))
                .collect(Collectors.toList());
    }

}