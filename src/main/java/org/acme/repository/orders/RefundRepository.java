package org.acme.repository.orders;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import org.acme.models.orders.Refund;

import javax.enterprise.context.ApplicationScoped;
import java.util.Optional;

@ApplicationScoped
public class RefundRepository implements PanacheRepository<Refund> {

    public Optional<Refund> findByOrderProductID(Long orderProductID) {
        return find("orderProductID", orderProductID).firstResultOptional();
    }
}