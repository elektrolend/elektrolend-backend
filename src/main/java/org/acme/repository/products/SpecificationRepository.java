package org.acme.repository.products;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import org.acme.dto.products.SpecificationDTO;
import org.acme.models.products.Specification;

import javax.enterprise.context.ApplicationScoped;
import java.util.Optional;

@ApplicationScoped
public class SpecificationRepository implements PanacheRepository<Specification> {

    public boolean isSpecificationDuplicated(SpecificationDTO specificationDTO) {
        return findAll().stream()
                .filter(specification -> specification.product.id.equals(specificationDTO.getProductID()))
                .anyMatch(specification -> specification.key.equals(specificationDTO.getKey()));
    }

    public Optional<Specification> findByProductIdAndKeyOptional(long productID, String specificationOneKey) {
        return find("SELECT s FROM specification s WHERE s.product.id = ?1 AND key = ?2", productID, specificationOneKey)
                .singleResultOptional();
    }

}

