package org.acme.repository.products;

import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import org.acme.models.products.Product;

import javax.enterprise.context.ApplicationScoped;
import java.util.Optional;

@ApplicationScoped
public class ProductRepository implements PanacheRepository<Product> {

    public Optional<Product> findByNameOptional(String name) {
        return find("name", name).singleResultOptional();
    }

    public PanacheQuery<Product> findByCategoryIdAndAvailabilityOptional(Long categoryId, Boolean availability) {
        return find("SELECT p FROM product p WHERE p.category.id = ?1 AND p.isAvailable = ?2", categoryId, availability);
    }

    public PanacheQuery<Product> findProductsByAvailability(Boolean availability) {
        return find("SELECT p FROM product p WHERE p.isAvailable = ?1", availability);
    }
}
