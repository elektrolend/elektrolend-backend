package org.acme.repository.products;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import org.acme.models.products.Category;

import javax.enterprise.context.ApplicationScoped;
import java.util.Optional;

@ApplicationScoped
public class CategoryRepository implements PanacheRepository<Category> {

    public Optional<Category> findByNameOptional(String name) {
        return find("name", name).singleResultOptional();
    }

}
