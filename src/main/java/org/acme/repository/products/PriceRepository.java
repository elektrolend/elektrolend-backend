package org.acme.repository.products;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import org.acme.models.products.Price;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class PriceRepository implements PanacheRepository<Price> {

}
