package org.acme.repository.accounts;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import org.acme.models.accounts.UserAccount;

import javax.enterprise.context.ApplicationScoped;
import java.util.Optional;

@ApplicationScoped
public class UserAccountRepository implements PanacheRepository<UserAccount> {

    public Optional<UserAccount> findByNameOptional(String nickname) {
        return find("nickname", nickname).singleResultOptional();
    }

}
