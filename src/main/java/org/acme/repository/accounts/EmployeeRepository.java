package org.acme.repository.accounts;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import org.acme.models.accounts.Employee;

import javax.enterprise.context.ApplicationScoped;
import java.util.Optional;

@ApplicationScoped
public class EmployeeRepository implements PanacheRepository<Employee> {

    public Optional<Employee> findByNicknameOptional(String nickname) {
        return find("SELECT e FROM employee e WHERE e.userAccount.nickname = ?1", nickname)
                .singleResultOptional();
    }

    public Optional<Employee> findEmployeeByUserIdOptional(Long userId) {
        return find("userid", userId).singleResultOptional();
    }

    public Optional<Employee> findByEmailOptional(String email) {
        return find("email", email).singleResultOptional();
    }

    public Optional<Employee> findByPhoneOptional(String phone) {
        return find("phone", phone).singleResultOptional();
    }

}
