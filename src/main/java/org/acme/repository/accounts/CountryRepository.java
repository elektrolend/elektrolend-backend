package org.acme.repository.accounts;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import org.acme.models.accounts.Country;

import javax.enterprise.context.ApplicationScoped;
import java.util.Optional;

@ApplicationScoped
public class CountryRepository implements PanacheRepository<Country> {

    public Optional<Country> findByNameOptional(String name) {
        return find("name", name).singleResultOptional();
    }

}
