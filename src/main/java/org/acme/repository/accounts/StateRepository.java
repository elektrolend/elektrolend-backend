package org.acme.repository.accounts;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import org.acme.models.accounts.State;

import javax.enterprise.context.ApplicationScoped;
import java.util.Optional;

@ApplicationScoped
public class StateRepository implements PanacheRepository<State> {

    public Optional<State> findByNameOptional(String name) {
        return find("name", name).singleResultOptional();
    }

}
