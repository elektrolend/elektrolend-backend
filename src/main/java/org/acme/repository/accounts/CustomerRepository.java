package org.acme.repository.accounts;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import org.acme.models.accounts.Customer;

import javax.enterprise.context.ApplicationScoped;
import java.util.Optional;

@ApplicationScoped
public class CustomerRepository implements PanacheRepository<Customer> {

    public Optional<Customer> findByNicknameOptional(String nickname) {
        return find("SELECT c FROM customer c WHERE c.userAccount.nickname = ?1", nickname)
                .singleResultOptional();
    }

    public Optional<Customer> findCustomerByUserIdOptional(Long customerID) {
        return find("userID", customerID).singleResultOptional();
    }

    public Optional<Customer> findByEmailOptional(String email) {
        return find("email", email).singleResultOptional();
    }

    public Optional<Customer> findByPhoneOptional(String phone) {
        return find("phone", phone).singleResultOptional();
    }

    public String findCustomerNameById(Long customerID) {
        Customer customer = findById(customerID);
        return customer.getName() + " " + customer.getSurname();
    }

}
