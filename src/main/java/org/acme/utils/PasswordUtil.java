package org.acme.utils;

import io.vertx.ext.web.handler.impl.HttpStatusException;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.acme.models.accounts.UserAccount;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpStatus;
import org.wildfly.security.WildFlyElytronProvider;
import org.wildfly.security.password.PasswordFactory;
import org.wildfly.security.password.interfaces.BCryptPassword;
import org.wildfly.security.password.spec.EncryptablePasswordSpec;
import org.wildfly.security.password.spec.IteratedSaltedPasswordAlgorithmSpec;

import java.security.NoSuchAlgorithmException;
import java.security.Provider;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.Random;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class PasswordUtil {

    private static final Provider ELYTRON_PROVIDER = new WildFlyElytronProvider();
    private static final int ITERATION_COUNT = 10;

    public static boolean isPasswordTooWeak(String password) {
        String regex = "^(?=.*[a-z])(?=.*[A-Z])(?=.*[-+_!@#$%^&*., ?]).+$";
        return !password.matches(regex) || password.length() < 6 || password.length() > 20;
    }

    public static String createBoxPassword() {
        Random rand;
        try {
            rand = SecureRandom.getInstanceStrong();
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException(e.getMessage(), e);
        }
        int upperbound = 5;
        int passwordLength = rand.nextInt(upperbound) + 7;

        String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        StringBuilder password = new StringBuilder();
        Random random;
        try {
            random = SecureRandom.getInstanceStrong();
            for (int i = 0; i < passwordLength; i++) {
                int index = random.nextInt(alphabet.length());
                char randomChar = alphabet.charAt(index);
                password.append(randomChar);
            }
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException(e.getMessage(), e);
        }
        return password.toString();
    }

    public static boolean isPasswordCorrect(UserAccount userAccount, String guess) {
        byte[] output = getHash(guess, userAccount.getSalt());
        return Arrays.equals(Base64.decodeBase64(userAccount.getPasswordHash()), output);
    }

    private static byte[] getHash(String password, String salt) {
        PasswordFactory passwordFactory;
        try {
            passwordFactory = PasswordFactory.getInstance(BCryptPassword.ALGORITHM_BCRYPT, ELYTRON_PROVIDER);
        } catch (NoSuchAlgorithmException e) {
            throw new HttpStatusException(HttpStatus.SC_BAD_REQUEST);
        }

        byte[] byteSalt = Base64.decodeBase64(salt);

        return hashPassword(password, byteSalt, passwordFactory);
    }

    public static void hashPassword(UserAccount userAccount, String password) {
        PasswordFactory passwordFactory;
        try {
            passwordFactory = PasswordFactory.getInstance(BCryptPassword.ALGORITHM_BCRYPT, ELYTRON_PROVIDER);
        } catch (NoSuchAlgorithmException e) {
            throw new HttpStatusException(HttpStatus.SC_BAD_REQUEST);
        }

        SecureRandom random = new SecureRandom();
        byte[] salt = new byte[BCryptPassword.BCRYPT_SALT_SIZE];
        random.nextBytes(salt);

        byte[] hashedBytes = hashPassword(password, salt, passwordFactory);
        java.util.Base64.Encoder encoder = java.util.Base64.getEncoder();

        userAccount.setPasswordHash(encoder.encodeToString(hashedBytes));
        userAccount.setSalt(encoder.encodeToString(salt));
    }

    private static byte[] hashPassword(String password, byte[] salt, PasswordFactory passwordFactory) {
        try {
            IteratedSaltedPasswordAlgorithmSpec iteratedAlgorithmSpec = new IteratedSaltedPasswordAlgorithmSpec(ITERATION_COUNT, salt);
            EncryptablePasswordSpec encryptableSpec = new EncryptablePasswordSpec(password.toCharArray(), iteratedAlgorithmSpec);

            BCryptPassword original = (BCryptPassword) passwordFactory.generatePassword(encryptableSpec);

            return original.getHash();
        } catch (InvalidKeySpecException e) {
            throw new HttpStatusException(HttpStatus.SC_BAD_REQUEST);
        }
    }

}
