package org.acme.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class DateUtil {

    public static boolean checkIfDatesAreChronological(LocalDate firstDate, LocalDate secondDate) {
        return firstDate.isBefore(secondDate);
    }

    public static boolean checkIfDatesAreChronologicalOrEqual(LocalDate firstDate, LocalDate secondDate) {
        return firstDate.isBefore(secondDate) || firstDate.equals(secondDate);
    }

    public static boolean isDateFromPastOrToday(LocalDate date) {
        LocalDate today = LocalDate.now();
        return date.isEqual(today) || date.isBefore(today);
    }

    public static boolean timeSpansCorrect(LocalDate beginDateToCheck, LocalDate endDateToCheck,
                                           LocalDate beginFinalDate, LocalDate endFinalDate) {
        if (endDateToCheck.isBefore(beginDateToCheck) || beginDateToCheck.equals(endDateToCheck)) {
            return false;
        }

        return (beginDateToCheck.isAfter(endFinalDate) && endDateToCheck.isAfter(endFinalDate))
                || (beginDateToCheck.isBefore(beginFinalDate) && endDateToCheck.isBefore(beginFinalDate));
    }

}
