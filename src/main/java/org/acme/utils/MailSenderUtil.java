package org.acme.utils;

import io.quarkus.mailer.Mail;
import io.quarkus.mailer.Mailer;
import io.smallrye.common.annotation.Blocking;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class MailSenderUtil {

    @Inject
    Mailer mailer;

    @Blocking
    public void sendRegistrationEmail(String email, String code) {
        mailer.send(
                Mail.withText(email,
                        "Witamy w Elektrolend",
                        "Aby potwierdzić swoje konto prosimy o podanie następującego kodu: " + code + " przy pierwszym logowaniu."
                )
        );
    }

    @Blocking
    public void sendRestoringPasswordEmail(String email, String code) {
        mailer.send(
                Mail.withText(email,
                        "Odzyskiwanie hasła w systemie Elektrolend",
                        "Aby zmienić swoje hasło prosimy o podanie następującego kodu: " + code + " przy próbie odzyskiwania hasła."
                )
        );
    }


}
