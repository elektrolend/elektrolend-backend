package org.acme.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ResponseUtil {

    public static Response createResponseWithMessage(Response.Status status, String message) {
        return Response.status(status)
                .entity(message)
                .type(MediaType.TEXT_PLAIN_TYPE)
                .build();
    }

}
