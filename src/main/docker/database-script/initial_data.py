import hashlib
import psycopg2
import random
import base64
import bcrypt
from datetime import datetime
from faker import Faker

fake = Faker()


conn = psycopg2.connect(database="postgres", user="postgres", password="password", host="database", port="5432")
print("Opened database successfully")
cur = conn.cursor()

state_ids = []
country_map = {
    'Polska': 'PL',
    'Hiszpania': 'ES',
    'Niemcy': 'DE',
    'Francja': 'FR',
    'Włochy': 'IT',
    'Słowacja': 'SK',
    'Republika Czeska': 'CZ',
    'Portugalia': 'PT'
}

for country_to_insert in country_map:
    cur.execute("INSERT INTO country (name, acronym) VALUES (%s, %s)",
                (country_to_insert, country_map[country_to_insert]))

states_poland = ['małopolskie', 'śląskie', 'mazowieckie', 'dolnośląskie', 'kujawsko-pomorskie', 'lubelskie',
                 'lubuskie', 'łódzkie', 'opolskie', 'podkarpackie', 'podlaskie', 'pomorskie', 'świętokrzyskie',
                 'warmińsko-mazurskie', 'wielkopolskie', 'zachodniopomorskie']

cur.execute("SELECT id FROM country WHERE name = 'Polska'")
country_id = cur.fetchone()
for state_to_insert in states_poland:
    cur.execute("INSERT INTO state (name, countryid) VALUES (%s, %s)", (state_to_insert, country_id))
    cur.execute("SELECT id FROM state WHERE name = '{0}'".format(state_to_insert))
    state_ids.append(cur.fetchone())

states_spain = ['Katalonia', 'Andaluzja', 'Cantabria']

cur.execute("SELECT id FROM country WHERE name = 'Hiszpania'")
country_id = cur.fetchone()
for state_to_insert in states_spain:
    cur.execute("INSERT INTO state (name, countryid) VALUES (%s, %s)", (state_to_insert, country_id))
    cur.execute("SELECT id FROM state WHERE name = '{0}'".format(state_to_insert))
    state_ids.append(cur.fetchone())

states_germany = ['Bawaria', 'Saksonia', 'Hesja']

cur.execute("SELECT id FROM country WHERE name = 'Niemcy'")
country_id = cur.fetchone()
for state_to_insert in states_germany:
    cur.execute("INSERT INTO state (name, countryid) VALUES (%s, %s)", (state_to_insert, country_id))
    cur.execute("SELECT id FROM state WHERE name = '{0}'".format(state_to_insert))
    state_ids.append(cur.fetchone())

states_france = ['Korsyka', 'Oksytania', 'Kraj Loary']

cur.execute("SELECT id FROM country WHERE name = 'Francja'")
country_id = cur.fetchone()
for state_to_insert in states_france:
    cur.execute("INSERT INTO state (name, countryid) VALUES (%s, %s)", (state_to_insert, country_id))
    cur.execute("SELECT id FROM state WHERE name = '{0}'".format(state_to_insert))
    state_ids.append(cur.fetchone())

states_italy = ['Apulia', 'Kalabria', 'Lacujm']

cur.execute("SELECT id FROM country WHERE name = 'Włochy'")
country_id = cur.fetchone()
for state_to_insert in states_italy:
    cur.execute("INSERT INTO state (name, countryid) VALUES (%s, %s)", (state_to_insert, country_id))
    cur.execute("SELECT id FROM state WHERE name = '{0}'".format(state_to_insert))
    state_ids.append(cur.fetchone())

states_slovakia = ['Kraj bratysławski', 'Kraj koszycki', 'Kraj żyliński']

cur.execute("SELECT id FROM country WHERE name = 'Słowacja'")
country_id = cur.fetchone()
for state_to_insert in states_slovakia:
    cur.execute("INSERT INTO state (name, countryid) VALUES (%s, %s)", (state_to_insert, country_id))
    cur.execute("SELECT id FROM state WHERE name = '{0}'".format(state_to_insert))
    state_ids.append(cur.fetchone())

states_czech = ['środkowoczeskie', 'południowomorawskie', 'usteckie']

cur.execute("SELECT id FROM country WHERE name = 'Republika Czeska'")
country_id = cur.fetchone()
for state_to_insert in states_czech:
    cur.execute("INSERT INTO state (name, countryid) VALUES (%s, %s)", (state_to_insert, country_id))
    cur.execute("SELECT id FROM state WHERE name = '{0}'".format(state_to_insert))
    state_ids.append(cur.fetchone())

states_portugal = ['Beja', 'Castelo Branco', 'Faro']

cur.execute("SELECT id FROM country WHERE name = 'Portugalia'")
country_id = cur.fetchone()
for state_to_insert in states_portugal:
    cur.execute("INSERT INTO state (name, countryid) VALUES (%s, %s)", (state_to_insert, country_id))
    cur.execute("SELECT id FROM state WHERE name = '{0}'".format(state_to_insert))
    state_ids.append(cur.fetchone())

category_map = {
    'Telefony': 'aaa',
    'Telewizory': 'aaa',
    'Komputery stacjonarne': 'aaa',
    'Słuchawki': 'aaa',
    'Soundbary': 'aaa'
}

category_photo_map = {
    'Telefony': 'https://firebasestorage.googleapis.com/v0/b/elektrolend-images.appspot.com/o/images%2Fcategories%2Fphones.png?alt=media&token=f06582f7-1dcf-41f8-9985-69c309856b2e',
    'Telewizory': 'https://firebasestorage.googleapis.com/v0/b/elektrolend-images.appspot.com/o/images%2Fcategories%2Ftvs.png?alt=media&token=64e0b3ae-4d72-4483-8eb3-d6b7b3544684',
    'Komputery stacjonarne': 'https://firebasestorage.googleapis.com/v0/b/elektrolend-images.appspot.com/o/images%2Fcategories%2Fcomputers.png?alt=media&token=b914d8c3-f88f-4d9e-b70d-638ea46662a4',
    'Słuchawki': 'https://firebasestorage.googleapis.com/v0/b/elektrolend-images.appspot.com/o/images%2Fcategories%2Fheadphones.png?alt=media&token=f20d45b8-c545-487e-8dfe-eb7391d48ddb',
    'Soundbary': 'https://firebasestorage.googleapis.com/v0/b/elektrolend-images.appspot.com/o/images%2Fcategories%2Fsoundbars.png?alt=media&token=e04f43f4-56c8-4953-b5b3-b57bda55101e'
}

image_url = 'https://wellnessinmind.ca/wp-content/themes/wellness/img/default_new_image.jpg'
for category_to_insert in category_map:
    cur.execute("INSERT INTO category (name, description, imageurl) VALUES (%s, %s, %s)",
                (category_to_insert, category_map[category_to_insert], category_photo_map[category_to_insert]))

product_ids = []

products_phones_map = {
    'Samsung Galaxy A21s (czarny)': 'Aparaty tylny/przedni   48 Mpix + 8 Mpix + 2 Mpix + 2 Mpix / 13 Mpix Pojemność baterii 5000 mAh Wyświetlacz   6,5 \", 1600 x 720 pikseli, System operacyjny Android 10 + One UI 2,0 Procesor  8-rdzeniowy Samsung Exynos 850',
    'Samsung Galaxy M11 (czarny)': 'Aparaty tylny/przedni   13 Mpix + 2 Mpix + 5 Mpix / 8 Mpix Pojemność baterii   5000 mAh Pamięć   3 GB / 32 GB Wyświetlacz   6,39 \", 1560 x 720 pikseli, Infinity-O Display System operacyjny   Android 10 Procesor   8-rdzeniowy Qualcomm Snapdragon 450',
    'Xiaomi Redmi Note 9 4+128 Midnight Grey': 'Aparaty tylny/przedni   48 Mpix + 8 Mpix + 2 Mpix + 2 Mpix / 13 Mpix Pojemność baterii   5020 mAh Pamięć   4 GB / 128 GB Wyświetlacz   6,53 \", 2340 x 1080 pikseli, Full HD+ System operacyjny   Android 10 Procesor   8-rdzeniowy MediaTek Helio G85'
}

products_phones_photos_map = {
    'Samsung Galaxy A21s (czarny)': 'https://firebasestorage.googleapis.com/v0/b/elektrolend-images.appspot.com/o/images%2Fproducts%2Fsamsung-galaxy-a-21-czarny.jpg?alt=media&token=4888151e-17a1-437c-9097-ecbe1b4c2493',
    'Samsung Galaxy M11 (czarny)': 'https://firebasestorage.googleapis.com/v0/b/elektrolend-images.appspot.com/o/images%2Fproducts%2Fsamsung-galaxy-m-11-czarny.jpg?alt=media&token=95a3e2ab-c2fc-49e0-81fa-c80820d45774',
    'Xiaomi Redmi Note 9 4+128 Midnight Grey': 'https://firebasestorage.googleapis.com/v0/b/elektrolend-images.appspot.com/o/images%2Fproducts%2Fxiaomi-redmi-note-9.jpg?alt=media&token=da4b0b72-650a-40a9-8fc6-1d2af23d0dd8'
}

cur.execute("SELECT id FROM category WHERE name = 'Telefony'")
category_id = cur.fetchone()
for phone_to_insert in products_phones_map:
    deposit_to_insert = random.randint(100, 200)
    insurance_to_insert = random.randint(30, 80)
    cur.execute("INSERT INTO product (name, description, categoryid, deposit, insurance, imageurl) VALUES (%s, %s, %s, %s, %s, %s)",
                (phone_to_insert, products_phones_map[phone_to_insert], category_id, deposit_to_insert,
                 insurance_to_insert, products_phones_photos_map[phone_to_insert]))
    cur.execute("SELECT id FROM product WHERE name = '{0}'".format(phone_to_insert))
    product_id_inserted = cur.fetchone()
    cur.execute("INSERT INTO specification (productid, key, value) VALUES (%s,%s,%s)", (product_id_inserted, 'Ekran', str(random.randint(4, 7)) + " ''"))
    cur.execute("INSERT INTO specification (productid, key, value) VALUES (%s,%s,%s)", (product_id_inserted, 'Bateria', str(random.randint(2000, 3000)) + " mAh"))
    product_ids.append(product_id_inserted)

products_tv_map = {
    'LG OLED65CX3LA': 'Ekran   65 cali, 4K UHD / 3840 x 2160 Smart TV   Smart TV Częstotliwość odświeżania ekranu   100 Hz / 120 Hz Technologia obrazu   OLED Funkcje   HDR, Wi-Fi, Bluetooth, USB - multimedia, USB - nagrywanie, obsługa głosowa',
    'Samsung QLED QE65Q67TAU': 'Ekran   65 cali, 4K UHD / 3840 x 2160 Smart TV   Smart TV Częstotliwość odświeżania ekranu   50 Hz / 60 Hz Technologia obrazu   QLED, LED Funkcje   HDR, Wi-Fi, Bluetooth, USB - multimedia, USB - nagrywanie, TimeShift, obsługa głosowa',
    'LG OLED55BX3LB': 'Ekran   55 cali, 4K UHD / 3840 x 2160 Smart TV   Smart TV Częstotliwość odświeżania ekranu   100 Hz / 120 Hz Technologia obrazu   OLED Funkcje   HDR, Wi-Fi, Bluetooth, USB - multimedia, USB - nagrywanie, obsługa głosowa'
}

products_tv_photo_map = {
    'LG OLED65CX3LA': 'https://firebasestorage.googleapis.com/v0/b/elektrolend-images.appspot.com/o/images%2Fproducts%2FLG-OLED65CX3LA.jpg?alt=media&token=e15af593-28cf-4710-ac6f-c793509a362a',
    'Samsung QLED QE65Q67TAU': 'https://firebasestorage.googleapis.com/v0/b/elektrolend-images.appspot.com/o/images%2Fproducts%2FSamsung-QLED-QE65Q67TAU.jpg?alt=media&token=7f8ccc0e-be81-44ac-87de-8704851245cd',
    'LG OLED55BX3LB': 'https://firebasestorage.googleapis.com/v0/b/elektrolend-images.appspot.com/o/images%2Fproducts%2FLG-OLED55BX3LB.jpg?alt=media&token=a50cab7f-6fda-4d91-b17a-55e07cb3e029'
}

cur.execute("SELECT id FROM category WHERE name = 'Telewizory'")
category_id = cur.fetchone()
for tv_to_insert in products_tv_map:
    deposit_to_insert = random.randint(100, 200)
    insurance_to_insert = random.randint(30, 80)
    cur.execute("INSERT INTO product (name, description, categoryid, deposit, insurance, imageurl) VALUES (%s, %s, %s, %s, %s, %s)",
                (tv_to_insert, products_tv_map[tv_to_insert], category_id, deposit_to_insert, insurance_to_insert, products_tv_photo_map[tv_to_insert]))
    cur.execute("SELECT id FROM product WHERE name = '{0}'".format(tv_to_insert))
    product_id_inserted = cur.fetchone()
    cur.execute("INSERT INTO specification (productid, key, value) VALUES (%s,%s,%s)", (product_id_inserted, 'Ekran', str(random.randint(40, 70)) + "''"))
    cur.execute("INSERT INTO specification (productid, key, value) VALUES (%s,%s,%s)", (product_id_inserted, 'Częstotliwość odświeżania ekranu', str(random.randint(100, 200)) + "Hz"))
    product_ids.append(product_id_inserted)

products_pc_map = {
    'Acer Nitro 50 Intel® Core™ i5-10400F 8GB 1TB + 512GB GTX1650': 'Procesor   Intel® Core i5 10gen 10400F 2,9 - 4,3 GHz Karta graficzna   nVIDIA® GeForce GTX1650 Pamięć RAM   8 GB Pojemność dysku   1000 GB Dodatkowy dysk   512 GB SSD System operacyjny   bez systemu',
    'Actina AMD Ryzen 5 3600 16GB 512GB RX570 W10': 'Procesor   AMD Ryzen™ 5 3600 3,6 - 4,2 GHz Karta graficzna   AMD Radeon RX 570 Pamięć RAM   16 GB Pojemność dysku   512 GB SSD System operacyjny   Windows 10 Home Edition'
}

products_pc_photo_map = {
    'Acer Nitro 50 Intel® Core™ i5-10400F 8GB 1TB + 512GB GTX1650': 'https://firebasestorage.googleapis.com/v0/b/elektrolend-images.appspot.com/o/images%2Fproducts%2FAcer-Nitro-50-Intel.jpg?alt=media&token=f16a9660-a471-4556-931b-3ada3168b797',
    'Actina AMD Ryzen 5 3600 16GB 512GB RX570 W10': 'https://firebasestorage.googleapis.com/v0/b/elektrolend-images.appspot.com/o/images%2Fproducts%2FActina-AMD-Ryzen-5.jpg?alt=media&token=f4b10fb1-7c31-43c0-8706-f12a5344bbb8'
}

cur.execute("SELECT id FROM category WHERE name = 'Komputery stacjonarne'")
category_id = cur.fetchone()
for pc_to_insert in products_pc_map:
    deposit_to_insert = random.randint(100, 200)
    insurance_to_insert = random.randint(30, 80)
    cur.execute("INSERT INTO product (name, description, categoryid, deposit, insurance, imageurl) VALUES (%s, %s, %s, %s, %s, %s)",
                (pc_to_insert, products_pc_map[pc_to_insert], category_id, deposit_to_insert, insurance_to_insert, products_pc_photo_map[pc_to_insert]))
    cur.execute("SELECT id FROM product WHERE name = '{0}'".format(pc_to_insert))
    product_id_inserted = cur.fetchone()
    product_ids.append(product_id_inserted)

products_headphones_map = {
    'Sony WH-CH510 (czarny)': 'Słuchawki nausze, nie składane, 132 gram, z regulacją głośności, z transmisją bluetooth',
    'BEATS BY DR. DRE Solo Pro Wireless ANC': 'Słuchawki nausze, składane, 267 gram, z regulacją głośności, z transmisją bluetooth',
    'BEATS BY DR. DRE Powerbeats Pro (czarny)': 'Słuchawki dokanałowe, nie składane, z regulacją głośności, z transmisją bluetooth',
    'Apple AirPods Pro ANC (białe)': 'Słuchawki douszne, nie składane, 5.4 gram, z regulacją głośności, z transmisją bluetooth'
}

products_headphones_photo_map = {
    'Sony WH-CH510 (czarny)': 'https://firebasestorage.googleapis.com/v0/b/elektrolend-images.appspot.com/o/images%2Fproducts%2FSony-WH-CH510-czarny.jpg?alt=media&token=28db0139-2fd6-40e6-b2aa-56b5fb599a1f',
    'BEATS BY DR. DRE Solo Pro Wireless ANC': 'https://firebasestorage.googleapis.com/v0/b/elektrolend-images.appspot.com/o/images%2Fproducts%2FBEATS-BY-DR-DRE-Solo-Pro-Wireless-ANC.jpg?alt=media&token=13e05e03-b59c-4d90-afbf-e0f7c6756153',
    'BEATS BY DR. DRE Powerbeats Pro (czarny)': 'https://firebasestorage.googleapis.com/v0/b/elektrolend-images.appspot.com/o/images%2Fproducts%2FBEATS-BY-DR-DRE-Powerbeats-Pro-czarny.jpg?alt=media&token=be4522ba-c698-421b-8bf4-aec885fe7c4d',
    'Apple AirPods Pro ANC (białe)': 'https://firebasestorage.googleapis.com/v0/b/elektrolend-images.appspot.com/o/images%2Fproducts%2FApple-AirPods-Pro-ANC-bia%C5%82e.jpg?alt=media&token=b89f7653-272a-49e9-817f-5e7fdcc2c228',
}

cur.execute("SELECT id FROM category WHERE name = 'Słuchawki'")
category_id = cur.fetchone()
for headphones_to_insert in products_headphones_map:
    deposit_to_insert = random.randint(100, 200)
    insurance_to_insert = random.randint(30, 80)
    cur.execute("INSERT INTO product (name, description, categoryid, deposit, insurance, imageurl) VALUES (%s, %s, %s, %s, %s, %s)",
                (headphones_to_insert, products_headphones_map[headphones_to_insert], category_id, deposit_to_insert,
                 insurance_to_insert, products_headphones_photo_map[headphones_to_insert]))
    cur.execute("SELECT id FROM product WHERE name = '{0}'".format(headphones_to_insert))
    product_ids.append(cur.fetchone())

products_soundbars_map = {
    'JBL Bar 2.1 DEEP BASS': 'Soundbar o liczbie kanałów 2.1, mocy 300W, łączność bluetooth, przewodowy',
    'JBL Bar 5.0 Multibeam': 'Soundbar o liczbie kanałów 5, mocy 250W, łączność bluetooth, AirPlay2 i Chromecast',
    'DENNON DHT-S416C': 'Soundbar o liczbie kanałów 2.1, łączność bluetooth i Wi-Fi, aktywny, bezprzewodowy'
}

products_soundbars_photo_map = {
    'JBL Bar 2.1 DEEP BASS': 'https://firebasestorage.googleapis.com/v0/b/elektrolend-images.appspot.com/o/images%2Fproducts%2FJBL-Bar-2-1-DEEP-BASS.jpg?alt=media&token=59d4e6ba-4026-4f26-b020-ce369a1a08bb',
    'JBL Bar 5.0 Multibeam': 'https://firebasestorage.googleapis.com/v0/b/elektrolend-images.appspot.com/o/images%2Fproducts%2FJBL-Bar-5-0-Multibeam.jpg?alt=media&token=6a8e10f0-52b7-4638-889b-a8d1421ff416',
    'DENNON DHT-S416C': 'https://firebasestorage.googleapis.com/v0/b/elektrolend-images.appspot.com/o/images%2Fproducts%2FDENNON-DHT-S416C.jpg?alt=media&token=424aa16d-c07c-43ca-9bf9-29c393381296'
}

cur.execute("SELECT id FROM category WHERE name = 'Soundbary'")
category_id = cur.fetchone()
for soundbars_to_insert in products_soundbars_map:
    deposit_to_insert = random.randint(100, 200)
    insurance_to_insert = random.randint(30, 80)
    cur.execute("INSERT INTO product (name, description, categoryid, deposit, insurance, imageurl) VALUES (%s, %s, %s, %s, %s, %s)",
                (soundbars_to_insert, products_soundbars_map[soundbars_to_insert], category_id, deposit_to_insert,
                 insurance_to_insert, products_soundbars_photo_map[soundbars_to_insert]))
    cur.execute("SELECT id FROM product WHERE name = '{0}'".format(soundbars_to_insert))
    product_ids.append(cur.fetchone())

for product_id in product_ids:
    netto = random.randint(15, 35)
    cur.execute("INSERT INTO price (productid, priceperdaynetto, priceperdaybrutto, begindate, enddate) VALUES(%s, %s, %s, %s, %s)",
                (product_id, netto, netto * 1.23, '2020-10-10', None))

office_box_ids = []
for i in range(10):
    code_to_insert = 'skrytka' + str(i)
    cur.execute("INSERT INTO office_box (code) VALUES ('{0}')".format(code_to_insert))
    cur.execute("SELECT id FROM office_box WHERE code = '{0}'".format(code_to_insert))
    office_box_ids.append(cur.fetchone())

customer_ids = []
for i in range(10):
    nickname = "user" + str(i)
    salt = "foiEC8azxv/ZX/U8AViLiQ=="
    password_hash = "FjBEepJa4njN7pVo9G0sb7myfusBd08="
    cur.execute("INSERT INTO user_account (nickname, passwordhash, salt, isemailverified, role) VALUES(%s,%s,%s,%s,%s)",
                (nickname, password_hash, salt, True, "customer"))
    cur.execute("SELECT id FROM user_account WHERE nickname = '{0}'".format(nickname))
    user_id_to_insert = cur.fetchone()[0]
    street_to_insert = fake.street_name()
    homenumber_to_insert = random.randint(1, 100)
    localnumber_to_insert = random.randint(1, 100)
    state_to_insert = state_ids[random.randint(0, len(state_ids) - 1)]
    city_to_insert = fake.city()
    zipcode_to_insert = str(random.randint(10, 99)) + '-' + str(random.randint(100, 999))
    email_to_insert = fake.email()
    name_to_insert = fake.first_name()
    phone_to_insert = random.randint(100_000_000, 999_999_999)
    surname_to_insert = fake.last_name()
    cur.execute("INSERT INTO customer (street, homenumber, localnumber, stateid, city, zipcode, email, name, phone, surname, userid) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",
                (street_to_insert, homenumber_to_insert, localnumber_to_insert, state_to_insert,
                 city_to_insert, zipcode_to_insert, email_to_insert, name_to_insert, phone_to_insert,
                 surname_to_insert, user_id_to_insert))
    cur.execute("SELECT id FROM customer WHERE userid = '{0}'".format(user_id_to_insert))
    customer_ids.append(cur.fetchone())


for i in range(10):
    nickname = "employee" + str(i)
    salt = "ck2o1qy8JFCQKH255zuVRA=="
    password_hash = "ddShGlSk/fAefWq9SI3WKZUTUDQDyAg="
    cur.execute("INSERT INTO user_account (nickname, passwordhash, salt, isemailverified, role) VALUES(%s,%s,%s,%s,%s)",
                (nickname, password_hash, salt, True, "regular"))
    cur.execute("SELECT id FROM user_account WHERE nickname = '{0}'".format(nickname))
    user_id_to_insert = cur.fetchone()
    email_to_insert = fake.email()
    name_to_insert = fake.first_name()
    phone_to_insert = str(random.randint(100_000_000, 999_999_999))
    surname_to_insert = fake.last_name()
    cur.execute("INSERT INTO employee (employeetype, name, surname, phone, email, userid) VALUES (%s,%s,%s,%s,%s,%s)",
                ('0', name_to_insert, surname_to_insert, phone_to_insert, email_to_insert, user_id_to_insert))

nickname = "admin"
salt = "XEUWd277BA1vO0zATaXx7g=="
password_hash = "l0LRU6gomWi9wQG9R3m7SEuWmhYnm+Q="
cur.execute("INSERT INTO user_account (nickname, passwordhash, salt, isemailverified, role) VALUES(%s,%s,%s,%s,%s)",
            (nickname, password_hash, salt, True, "admin"))
cur.execute("SELECT id FROM user_account WHERE nickname = '{0}'".format(nickname))
user_id_to_insert = cur.fetchone()
email_to_insert = fake.email()
name_to_insert = fake.first_name()
phone_to_insert = str(random.randint(100_000_000, 999_999_999))
surname_to_insert = fake.last_name()
cur.execute("INSERT INTO employee (employeetype, name, surname, phone, email, userid) VALUES (%s,%s,%s,%s,%s,%s)",
            ('1', name_to_insert, surname_to_insert, phone_to_insert, email_to_insert, user_id_to_insert))


def calculate_days_between(begin_date, end_date):
    date_format = "%Y-%m-%d"
    begin = datetime.strptime(begin_date, date_format)
    end = datetime.strptime(end_date, date_format)
    return (end - begin).days


def calculate_price(product_id, order_begin_date, order_end_date, service_type_security):
    number_of_days = calculate_days_between(order_begin_date, order_end_date)
    if service_type_security == '0':
        cur.execute("SELECT insurance FROM product WHERE id = '{0}'".format(product_id[0]))
    else:
        cur.execute("SELECT deposit FROM product WHERE id = '{0}'".format(product_id[0]))
    security_value = cur.fetchone()[0]
    cur.execute("SELECT priceperdaybrutto FROM price WHERE productid = '{0}'".format(product_id[0]))
    priceperday = cur.fetchone()[0]
    return priceperday * number_of_days + security_value


for i in range(20):
    order_date_to_insert = "2020-" + str(random.randint(3, 6)) + "-" + (str(random.randint(1, 30)))
    customer_id_to_insert = random.choice(customer_ids)
    cur.execute("INSERT INTO order_table (orderdate, customerid) VALUES (%s,%s)", (order_date_to_insert,customer_id_to_insert))
    cur.execute("SELECT id FROM order_table WHERE orderdate = %s AND customerid = %s", (order_date_to_insert,customer_id_to_insert))
    id_of_order = cur.fetchone()
    for j in range(1, random.randint(1, 4)):
        chosen_product_id = random.choice(product_ids)
        chosen_order_begin_date = "2020-" + str(random.randint(6, 8)) + "-" + (str(random.randint(1, 30))),
        chosen_order_end_date = "2020-" + str(random.randint(9, 11)) + "-" + (str(random.randint(1, 30))),
        chosen_service_type_security = str(random.randint(0, 1))
        calculated_price = calculate_price(chosen_product_id, chosen_order_begin_date[0], chosen_order_end_date[0], chosen_service_type_security)
        cur.execute("INSERT INTO order_product (officeboxidstart, officeboxidend, orderid, productid, orderproductstate, orderbegindate, orderenddate, price, servicesecuritytype, boxpassword) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",
                    (random.choice(office_box_ids), random.choice(office_box_ids), id_of_order, chosen_product_id, '0',
                     chosen_order_begin_date, chosen_order_end_date,
                     calculated_price, chosen_service_type_security, "ABCDEFG"))

for i in range(20):
    order_product_id = random.randint(1, 10)
    refund = random.randint(0, 100)
    date_to_insert = "2020-" + str(random.randint(1, 12)) + "-" + (str(random.randint(1, 28)))
    description = "LoremIpsum"
    if random.randint(1, 12) > 6:
        is_Considered = True
    else:
        is_Considered = False
    cur.execute("INSERT INTO complaint (orderproductid, refund, date, description, isconsidered) VALUES (%s,%s,%s,%s,%s)",
                (order_product_id, refund, date_to_insert, description, is_Considered))
    cur.execute("SELECT id FROM complaint WHERE orderproductid = %s AND refund = %s", (order_product_id, refund))
    complaint_id = cur.fetchone()
    for j in range(5):
        image_url = ('https://firebasestorage.googleapis.com/v0/b/elektrolend-images.appspot.com/o/images\%2Fcategories\%2F783px-Test-Logo.svg.png?alt=media&token=14262ea4-a053-4498-89d6-c01941d35b25')
        cur.execute("INSERT INTO complaint_photo (complaintid, url) VALUES (%s, %s)", (complaint_id, image_url))


conn.commit()
print("Records created successfully")
conn.close()
