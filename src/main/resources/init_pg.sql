create table category
(
    id          serial       not null
        constraint category_pk
            primary key,
    name        varchar(50)  not null unique,
    description varchar(255) not null,
    imageUrl    varchar(400) unique
);

create table product
(
    id          serial       not null
        constraint product_pk
            primary key,
    name        varchar(255) not null unique,
    description varchar(255) not null,
    categoryID  int          not null
        constraint product_category_id_fk
            references category,
    deposit     int          not null,
    insurance   int          not null,
    imageUrl    varchar(400) unique,
    isAvailable boolean      not null default true
);

create table price
(
    id                serial not null
        constraint price_pk
            primary key,
    productID         int    not null
        constraint price_product_id_fk
            references product,
    pricePerDayNetto  int    not null,
    pricePerDayBrutto int    not null,
    beginDate         date   not null,
    endDate           date
);

create table specification
(
    id        serial       not null
        constraint specification_pk
            primary key,
    productID int          not null
        constraint specification_product_id_fk
            references product,
    key       varchar(50)  not null,
    value     varchar(255) not null
);

create table user_account
(
    id              serial       not null
        constraint user_account_pk
            primary key,
    nickname        varchar(50)  not null unique,
    passwordHash    varchar(255) not null,
    salt            varchar(255)  not null,
    registerCode    varchar(20),
    isEmailVerified boolean,
    restoringCode   varchar(20),
    role            varchar(20)
);

create table employee
(
    id           serial      not null
        constraint employee_pk
            primary key,
    employeeType varchar(50) not null,
    email        varchar(50) not null unique,
    name         varchar(50) not null,
    phone        varchar(50) not null unique,
    surname      varchar(50) not null,
    userID       int         not null
        constraint customer_user_account_id_fk
            references user_account
);

create table country
(
    id      serial      not null
        constraint country_pk
            primary key,
    name    varchar(50) not null unique,
    acronym varchar(50) not null unique
);

create table state
(
    id        serial      not null
        constraint state_pk
            primary key,
    name      varchar(50) not null unique,
    countryID int         not null
        constraint state_country_id_fk
            references country
);

create table customer
(
    id          serial      not null
        constraint customer_pk
            primary key,
    street      varchar(50) not null,
    homeNumber  varchar(50) not null,
    localNumber varchar(50),
    stateID     int         not null
        constraint customer_state_id_fk
            references state,
    city        varchar(50) not null,
    zipCode     varchar(11) not null,
    email       varchar(50) not null unique,
    name        varchar(50) not null,
    phone       varchar(50) not null unique,
    surname     varchar(50) not null,
    userID      int         not null
        constraint customer_user_account_id_fk
            references user_account
);

create table office_box
(
    id   serial      not null
        constraint office_box_pk
            primary key,
    code varchar(50) not null
);

create table order_table
(
    id         serial not null
        constraint order_table_pk
            primary key,
    orderDate  date   not null,
    customerID int    not null
        constraint order_table_customer_id_fk
            references customer
);

create table payment
(
    id             serial       not null
        constraint payment_pk
            primary key,
    value          float        not null,
    date           date         not null,
    orderID        int          not null
        constraint payment_order_table_id_fk
            references order_table,
    paymentAccount varchar(200) not null
);

create table order_product
(
    id                  serial      not null
        constraint order_product_pk
            primary key,
    officeBoxIDStart    int         not null
        constraint order_product_office_box_id_start_fk
            references office_box,
    officeBoxIDEnd      int         not null
        constraint order_product_office_box_id_end_fk
            references office_box,
    orderID             int         not null
        constraint order_product_order_table_id_fk
            references order_table,
    productID           int         not null
        constraint order_product_product_id_fk
            references product,
    orderProductState   varchar(50) not null,
    orderBeginDate      date        not null,
    orderEndDate        date        not null,
    price               int         not null,
    serviceSecurityType varchar(50) not null,
    boxPassword         varchar(50) not null
);

create table complaint
(
    id             serial       not null
        constraint complaint_pk
            primary key,
    refund         int,
    date           date         not null,
    description    varchar(500) not null,
    isConsidered   boolean      not null,
    orderProductID int          not null
        constraint complaint_order_product_id_fk
            references order_product
);

create table complaint_photo
(
    id          serial       not null
        constraint complaint_photo_pk
            primary key,
    url         varchar(400) not null,
    complaintID int          not null
        constraint complaint_photo_complaint_id_fk
            references complaint
);

create table refund
(
    id                   serial  not null
        constraint refund_pk
            primary key,
    orderProductID       int     not null
        constraint refund_order_product_id_fk
            references order_product,
    refundValue          int,
    date                 date    not null,
    isReturnedToCustomer boolean not null
);
