package integration.tests.accounts;

import integration.resources.PostgresResource;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import io.vertx.ext.web.handler.impl.HttpStatusException;
import org.acme.models.accounts.Customer;
import org.acme.models.accounts.UserAccount;
import org.acme.repository.accounts.CountryRepository;
import org.acme.repository.accounts.CustomerRepository;
import org.acme.repository.accounts.StateRepository;
import org.acme.repository.accounts.UserAccountRepository;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.testcontainers.shaded.com.google.common.net.HttpHeaders;

import javax.inject.Inject;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.NoSuchElementException;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

@QuarkusTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@QuarkusTestResource(value=PostgresResource.class, restrictToAnnotatedClass = true)
@TestSecurity(authorizationEnabled = false)
class RegisterLoginIT {

    @Inject
    CountryRepository countryRepository;

    @Inject
    StateRepository stateRepository;

    @Inject
    CustomerRepository customerRepository;

    @Inject
    UserAccountRepository userAccountRepository;

    private static final String countryName = "Test country";
    private static final String countryAcronym = "TC";
    private static final String stateName = "Test state";
    private static final String customerNickname = "Test nickname";
    private static final String customerPassword = "Password!";
    private static final String customerPhone = " 123456789";
    private static final String customerEmail = "test@email.pl";
    private static final String customerStreet = "Test street";
    private static final int customerHomeNumber = 1;
    private static final int customerLocalNumber = 2;
    private static final String customerCity = " Test City";
    private static final String customerZipCode = "12-345";
    private static final String customerName = "Test name";
    private static final String customerSurname = "Test surname";

    private static int countryID;
    private static int stateID;
    private static int customerID;

    @Test
    @Order(1)
    @DisplayName("Prepare data for test")
    void prepareDataForTest() {
        given()
                .body(String.format(
                        """
                            {
                              "name": "%s",
                              "acronym": "%s"
                            }
                        """,
                        countryName,
                        countryAcronym
                ))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/countries")
                .then()
                .statusCode(Response.Status.CREATED.getStatusCode());

        countryID = Math.toIntExact(countryRepository.findByNameOptional(countryName)
                .orElseThrow(() -> new NoSuchElementException("Country not found"))
                .getId());

        given()
                .body(String.format(
                        """
                            {
                              "name": "%s",
                              "countryID": %d
                            }
                        """,
                        stateName,
                        countryID
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/states")
                .then()
                .statusCode(Response.Status.CREATED.getStatusCode());

        stateID = Math.toIntExact(stateRepository.findByNameOptional(stateName)
                .orElseThrow(() -> new NoSuchElementException("State not found"))
                .getId());
    }


    @Test
    @Order(2)
    @DisplayName("Register correctly")
    void correctRegisterTest() {
        given()
                .body(String.format(
                        """
                            {
                              "nickname": "%s",
                              "password": "%s",
                              "phone": "%s",
                              "email":"%s",
                              "street": "%s",
                              "homeNumber": %d,
                              "localNumber": %d,
                              "city": "%s",
                              "zipCode": "%s",
                              "state": "%s",
                              "country": "%s",
                              "name": "%s",
                              "surname": "%s"
                            }
                        """,
                        customerNickname,
                        customerPassword,
                        customerPhone,
                        customerEmail,
                        customerStreet,
                        customerHomeNumber,
                        customerLocalNumber,
                        customerCity,
                        customerZipCode,
                        stateName,
                        countryName,
                        customerName,
                        customerSurname
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/users/register")
                .then()
                .statusCode(Response.Status.CREATED.getStatusCode());

        customerID = Math.toIntExact(customerRepository.findByNicknameOptional(customerNickname)
                .orElseThrow(() -> new NoSuchElementException("Customer not found"))
                .getId());
    }


    @Test
    @Order(3)
    @DisplayName("Register for existing nickname")
    void registerForExistingNicknameTest() {
        given()
                .body(String.format(
                        """
                            {
                              "nickname": "%s",
                              "password": "%s",
                              "phone": "%s",
                              "email":"%s",
                              "street": "%s",
                              "homeNumber": %d,
                              "localNumber": %d,
                              "city": "%s",
                              "zipCode": "%s",
                              "state": "%s",
                              "country": "%s",
                              "name": "%s",
                              "surname": "%s"
                            }
                        """,
                        customerNickname,
                        customerPassword,
                        customerPhone,
                        customerEmail,
                        customerStreet,
                        customerHomeNumber,
                        customerLocalNumber,
                        customerCity,
                        customerZipCode,
                        stateName,
                        countryName,
                        customerName,
                        customerSurname
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/users/register")
                .then()
                .statusCode(Response.Status.CONFLICT.getStatusCode());
    }

    @Test
    @Order(4)
    @DisplayName("First login with null registration code")
    void firstLoginWithNullRegistrationCodeTest() {
        given()
                .body(String.format(
                                """
                                    {
                                      "nickname": "%s",
                                      "password": "%s",
                                      "registrationCode": null
                                    }
                                """,
                                customerNickname,
                                customerPassword
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/users/login")
                .then()
                .statusCode(Response.Status.NOT_ACCEPTABLE.getStatusCode());
    }

    @Test
    @Order(5)
    @DisplayName("First login with incorrect registration code")
    void firstLoginWithIncorrectRegistrationCodeTest() {
        given()
                .body(String.format(
                                """
                                    {
                                      "nickname": "%s",
                                      "password": "%s",
                                      "registrationCode": "incorrect"
                                    }
                                """,
                                customerNickname,
                                customerPassword
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/users/login")
                .then()
                .statusCode(Response.Status.CONFLICT.getStatusCode());
    }

    @Test
    @Order(6)
    @DisplayName("Login with correct credentials")
    void loginWithCorrectCredentialsTest() {
        Customer customer = customerRepository.findCustomerByUserIdOptional((long)customerID)
                .orElseThrow(() -> new HttpStatusException(HttpStatus.SC_NOT_FOUND));
        given()
                .body(String.format(
                        """
                            {
                              "nickname": "%s",
                              "password": "%s",
                              "registrationCode": "%s"
                            }
                        """,
                        customerNickname,
                        customerPassword,
                        customer.getUserAccount().getRegisterCode()
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/users/login")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .body("nickname", equalTo(customerNickname))
                .body("accountType", equalTo("CUSTOMER"));
    }

    @Test
    @Order(7)
    @DisplayName("Login with incorrect password")
    void loginWithIncorrectPasswordTest() {
        given()
                .body(String.format(
                        """
                            {
                              "nickname": "%s",
                              "password": "%s"
                            }
                        """,
                        customerNickname,
                        "wrongPassword"
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/users/login")
                .then()
                .statusCode(Response.Status.UNAUTHORIZED.getStatusCode());
    }

    @Test
    @Order(8)
    @DisplayName("Second login with correct credentials without registration code")
    void secondLoginWithCorrectCredentialsWithoutRegistrationCodeTest() {
        Customer customer = customerRepository.findCustomerByUserIdOptional((long)customerID)
                .orElseThrow(() -> new HttpStatusException(HttpStatus.SC_NOT_FOUND));

        given()
                .body(String.format(
                                """
                                    {
                                      "nickname": "%s",
                                      "password": "%s",
                                      "registrationCode": null
                                    }
                                """,
                                customerNickname,
                                customerPassword
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/users/login")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .body("nickname", equalTo(customerNickname))
                .body("accountType", equalTo("CUSTOMER"));
    }

    @Test
    @Order(9)
    @DisplayName("Clear all data after test")
    void clearDataTest() {
        given()
                .when()
                .delete("/customers/" + customerID)
                .then()
                .statusCode(Response.Status.NO_CONTENT.getStatusCode());

        given()
                .when()
                .delete("/states/" + stateID)
                .then()
                .statusCode(Response.Status.NO_CONTENT.getStatusCode());

        given()
                .when()
                .delete("/countries/" + countryID)
                .then()
                .statusCode(Response.Status.NO_CONTENT.getStatusCode());
    }

}
