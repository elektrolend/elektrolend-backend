package integration.tests.accounts;

import integration.resources.PostgresResource;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import org.acme.repository.accounts.CountryRepository;
import org.acme.repository.accounts.CustomerRepository;
import org.acme.repository.accounts.StateRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.testcontainers.shaded.com.google.common.net.HttpHeaders;

import javax.inject.Inject;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.NoSuchElementException;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

@QuarkusTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@QuarkusTestResource(value = PostgresResource.class, restrictToAnnotatedClass = true)
@TestSecurity(authorizationEnabled = false)
class UpdateUserDataIT {

    @Inject
    CountryRepository countryRepository;

    @Inject
    StateRepository stateRepository;

    @Inject
    CustomerRepository customerRepository;
    private static final String customerOneNickname = "Test one nickname";
    private static final String customerTwoNickname = "Test two nickname";
    private static final String countryName = "Test country";
    private static final String countryAcronym = "TC";
    private static final String stateName = "Test state";
    private static final String customerPassword = "Password!";
    private static final String customerOnePhone = " 123456789";
    private static final String customerTwoPhone = " 987654321";
    private static final String customerOneEmail = "testOne@email.pl";
    private static final String customerTwoEmail = "testTwo@email.pl";
    private static final String customerStreet = "Test street";
    private static final int customerHomeNumber = 1;
    private static final int customerLocalNumber = 2;
    private static final String customerCity = " Test City";
    private static final String customerZipCode = "12-345";
    private static final String customerName = "Test name";
    private static final String customerSurname = "Test surname";

    private static int countryID;
    private static int stateID;
    private static int customerOneID;
    private static int customerTwoID;

    @Test
    @Order(1)
    @DisplayName("Prepare data for test")
    void prepareDataForTest() {
        given()
                .body(String.format(
                        """
                            {
                              "name": "%s",
                              "acronym": "%s"
                            }
                        """,
                        countryName,
                        countryAcronym
                ))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/countries")
                .then()
                .statusCode(Response.Status.CREATED.getStatusCode());

        countryID = Math.toIntExact(countryRepository.findByNameOptional(countryName)
                .orElseThrow(() -> new NoSuchElementException("Country not found"))
                .getId());

        given()
                .body(String.format(
                        """
                            {
                              "name": "%s",
                              "countryID": %d
                            }
                        """,
                        stateName,
                        countryID
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/states")
                .then()
                .statusCode(Response.Status.CREATED.getStatusCode());

        stateID = Math.toIntExact(stateRepository.findByNameOptional(stateName)
                .orElseThrow(() -> new NoSuchElementException("State not found"))
                .getId());

        given()
                .body(String.format(
                        """
                            {
                              "nickname": "%s",
                              "password": "%s",
                              "phone": "%s",
                              "email":"%s",
                              "street": "%s",
                              "homeNumber": %d,
                              "localNumber": %d,
                              "city": "%s",
                              "zipCode": "%s",
                              "state": "%s",
                              "country": "%s",
                              "name": "%s",
                              "surname": "%s"
                            }
                        """,
                        customerOneNickname,
                        customerPassword,
                        customerOnePhone,
                        customerOneEmail,
                        customerStreet,
                        customerHomeNumber,
                        customerLocalNumber,
                        customerCity,
                        customerZipCode,
                        stateName,
                        countryName,
                        customerName,
                        customerSurname
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/users/register")
                .then()
                .statusCode(Response.Status.CREATED.getStatusCode());

        customerOneID = Math.toIntExact(customerRepository.findByNicknameOptional(customerOneNickname)
                .orElseThrow(() -> new NoSuchElementException("Customer not found"))
                .getId());

        given()
                .body(String.format(
                        """
                            {
                              "nickname": "%s",
                              "password": "%s",
                              "phone": "%s",
                              "email":"%s",
                              "street": "%s",
                              "homeNumber": %d,
                              "localNumber": %d,
                              "city": "%s",
                              "zipCode": "%s",
                              "state": "%s",
                              "country": "%s",
                              "name": "%s",
                              "surname": "%s"
                            }
                        """,
                        customerTwoNickname,
                        customerPassword,
                        customerTwoPhone,
                        customerTwoEmail,
                        customerStreet,
                        customerHomeNumber,
                        customerLocalNumber,
                        customerCity,
                        customerZipCode,
                        stateName,
                        countryName,
                        customerName,
                        customerSurname
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/users/register")
                .then()
                .statusCode(Response.Status.CREATED.getStatusCode());

        customerTwoID = Math.toIntExact(customerRepository.findByNicknameOptional(customerTwoNickname)
                .orElseThrow(() -> new NoSuchElementException("Customer not found"))
                .getId());
    }

    @Test
    @Order(2)
    @DisplayName("Change data for non existing customer")
    void changeDataForNonExistingCustomerTest() {
        given()
                .body(
                        """
                            {
                                "nickname": "Non existing",
                                "password": "CorrectPassword!"
                            }
                        """
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .patch("/users/login/" + customerTwoID + 1)
                .then()
                .statusCode(Response.Status.NOT_FOUND.getStatusCode())
                .body(is("Użytkownik z podanym id nie istnieje"));

        given()
                .body(String.format(
                        """
                            {
                                "phone": "987654321",
                                "email": "test@email.com",
                                "street": "Street test",
                                "homeNumber": 2,
                                "localNumber": 3,
                                "city": "City test",
                                "zipCode": "98-765",
                                "state": "%s",
                                "country": "%s",
                                "name": "Name test",
                                "surname": "Surname test"
                            }
                        """,
                        stateName,
                        countryName
                ))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .put("/users/" + customerTwoID + 1)
                .then()
                .statusCode(Response.Status.NOT_FOUND.getStatusCode())
                .body(is("Użytkownik z podanym id nie istnieje"));
    }

    @Order(3)
    @DisplayName("Change nickname to already taken")
    void changeNicknameToAlreadyTakenTest() {
        given()
                .body(String.format(
                        """
                            {
                                "nickname": "%s"
                            }
                        """,
                        customerTwoNickname
                ))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .patch("/users/login/" + customerOneID)
                .then()
                .statusCode(Response.Status.CONFLICT.getStatusCode())
                .body(is("Podany nickname jest zajęty"));
    }

    @Test
    @Order(4)
    @DisplayName("Change to the same nickname")
    void changeToTheSameNicknameTest() {
        given()
                .body(String.format(
                        """
                            {
                                "nickname": "%s"
                            }
                        """,
                        customerOneNickname
                ))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .patch("/users/login/" + customerOneID)
                .then()
                .statusCode(Response.Status.CONFLICT.getStatusCode())
                .body(is("Podany nickname jest zajęty"));
    }

    @Test
    @Order(5)
    @DisplayName("Change to too weak password")
    void changeToTooWeakPasswordTest() {
        given()
                .body(
                        """
                            {
                                "password": "weakPassword" 
                            }
                        """
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .patch("/users/login/" + customerOneID)
                .then()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .body(is("Podane hasło jest za słabe"));
    }

    @Test
    @Order(6)
    @DisplayName("Correct change password and nickname")
    void correctChangePasswordAndNicknameTest() {
        given()
                .body(
                        """
                            {
                                "nickname": "New nickname",
                                "password": "NewPassword!"
                            }
                        """
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .patch("/users/login/" + customerOneID)
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .body("nickname", equalTo("New nickname"))
                .body("id", equalTo(customerOneID));
    }

    @Test
    @Order(7)
    @DisplayName("Correct change user profile data")
    void correctChangeUserProfileDataTest() {
        given()
                .body(String.format(
                        """
                            {
                                "phone": "987654321",
                                "email": "test@email.com",
                                "street": "Street test",
                                "homeNumber": 2,
                                "localNumber": 3,
                                "city": "City test",
                                "zipCode": "98-765",
                                "state": "%s",
                                "country": "%s",
                                "name": "Name test",
                                "surname": "Surname test"
                            }
                        """,
                        stateName,
                        countryName
                ))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .put("/users/" + customerOneID)
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .body("phone", equalTo("987654321"))
                .body("email", equalTo("test@email.com"))
                .body("street", equalTo("Street test"))
                .body("homeNumber", equalTo("2"))
                .body("localNumber", equalTo("3"))
                .body("city", equalTo("City test"))
                .body("zipCode", equalTo("98-765"))
                .body("name", equalTo("Name test"))
                .body("surname", equalTo("Surname test"));
    }

    @Test
    @Order(8)
    @DisplayName("Clear all data after test")
    void clearDataTest() {
        given()
                .when()
                .delete("/customers/" + customerOneID)
                .then()
                .statusCode(Response.Status.NO_CONTENT.getStatusCode());

        given()
                .when()
                .delete("/customers/" + customerTwoID)
                .then()
                .statusCode(Response.Status.NO_CONTENT.getStatusCode());

        given()
                .when()
                .delete("/states/" + stateID)
                .then()
                .statusCode(Response.Status.NO_CONTENT.getStatusCode());

        given()
                .when()
                .delete("/countries/" + countryID)
                .then()
                .statusCode(Response.Status.NO_CONTENT.getStatusCode());
    }

}
