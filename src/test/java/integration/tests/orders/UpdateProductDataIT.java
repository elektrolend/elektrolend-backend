package integration.tests.orders;

import integration.resources.PostgresResource;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import org.acme.repository.products.CategoryRepository;
import org.acme.repository.products.ProductRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.testcontainers.shaded.com.google.common.net.HttpHeaders;

import javax.inject.Inject;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.NoSuchElementException;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

@QuarkusTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@QuarkusTestResource(value = PostgresResource.class, restrictToAnnotatedClass = true)
@TestSecurity(authorizationEnabled = false)
class UpdateProductDataIT {

    @Inject
    CategoryRepository categoryRepository;

    @Inject
    ProductRepository productRepository;

    private static final String categoryName = "Test category name";
    private static final String categoryDescription = "Test category description";
    private static final String productName = "Test product one name";
    private static final String productDescription = "Test product one description";
    private static final int productDeposit = 100;
    private static final int productInsurance = 10;

    private static int categoryID;
    private static int productID;

    @Test
    @Order(1)
    @DisplayName("Prepare data for test")
    void prepareDataForTest() {
        given()
                .body(String.format(
                        """
                            {
                              "name": "%s",
                              "description" : "%s"
                            }
                        """,
                        categoryName,
                        categoryDescription
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/categories")
                .then()
                .statusCode(Response.Status.CREATED.getStatusCode());

        categoryID = Math.toIntExact(categoryRepository.findByNameOptional(categoryName)
                .orElseThrow(() -> new NoSuchElementException("Category not found"))
                .getId());

        given()
                .body(String.format(
                        """
                            {
                                "categoryID": %d,
                                "name": "%s",
                                "description": "%s",
                                "deposit": %d,
                                "insurance": %d,
                                "isAvailable": true
                            }
                        """,
                        categoryID,
                        productName,
                        productDescription,
                        productDeposit,
                        productInsurance
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/products")
                .then()
                .statusCode(Response.Status.CREATED.getStatusCode());

        productID = Math.toIntExact(productRepository.findByNameOptional(productName)
                .orElseThrow(() -> new NoSuchElementException("Product one not found"))
                .getId());
    }

    @Test
    @Order(2)
    @DisplayName("Update non existing product")
    void updateNonExistingProductTest() {
        given()
                .body(String.format(
                        """
                                {
                                    "categoryID": %d,
                                    "name": "Name test",
                                    "description": "Description test",
                                    "deposit": 200,
                                    "insurance": 300
                                }
                        """,
                        categoryID
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .put("/products/" + productID + 1)
                .then()
                .statusCode(Response.Status.NOT_FOUND.getStatusCode())
                .body(is("Produkt z podanym id nie istnieje"));
    }

    @Test
    @Order(3)
    @DisplayName("Correct update data of product")
    void correctUpdateDataOfProductTest() {
        given()
                .body(String.format(
                        """
                                {
                                    "categoryID": %d,
                                    "name": "Name test",
                                    "description": "Description test",
                                    "deposit": 200,
                                    "insurance": 300
                                }
                        """,
                        categoryID
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .put("/products/" + productID)
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .body("categoryID", equalTo(categoryID))
                .body("name", equalTo("Name test"))
                .body("description", equalTo("Description test"))
                .body("deposit", equalTo(200))
                .body("insurance", equalTo(300));
    }

    @Test
    @Order(4)
    @DisplayName("Clear all data after test")
    void clearDataTest() {
        given()
                .when()
                .delete("/products/" + productID)
                .then()
                .statusCode(Response.Status.NO_CONTENT.getStatusCode());

        given()
                .when()
                .delete("/categories/" + categoryID)
                .then()
                .statusCode(Response.Status.NO_CONTENT.getStatusCode());
    }

}
