package integration.tests.orders;

import integration.resources.PostgresResource;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import org.acme.repository.accounts.CountryRepository;
import org.acme.repository.accounts.CustomerRepository;
import org.acme.repository.accounts.StateRepository;
import org.acme.repository.orders.ComplaintPhotoRepository;
import org.acme.repository.orders.ComplaintRepository;
import org.acme.repository.orders.OfficeBoxRepository;
import org.acme.repository.orders.OrderProductRepository;
import org.acme.repository.orders.OrderRepository;
import org.acme.repository.orders.RefundRepository;
import org.acme.repository.products.CategoryRepository;
import org.acme.repository.products.PriceRepository;
import org.acme.repository.products.ProductRepository;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.testcontainers.shaded.com.google.common.net.HttpHeaders;
import org.testcontainers.shaded.org.apache.commons.lang.ObjectUtils;

import javax.inject.Inject;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.time.LocalDate;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.emptyOrNullString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.hamcrest.Matchers.nullValue;

@QuarkusTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@QuarkusTestResource(value = PostgresResource.class, restrictToAnnotatedClass = true)
@TestSecurity(authorizationEnabled = false)
class CreateComplaintIT {

    @Inject
    CountryRepository countryRepository;

    @Inject
    StateRepository stateRepository;

    @Inject
    CustomerRepository customerRepository;

    @Inject
    CategoryRepository categoryRepository;

    @Inject
    ProductRepository productRepository;

    @Inject
    PriceRepository priceRepository;

    @Inject
    OfficeBoxRepository officeBoxRepository;

    @Inject
    OrderProductRepository orderProductRepository;

    @Inject
    OrderRepository orderRepository;

    @Inject
    ComplaintRepository complaintRepository;

    @Inject
    ComplaintPhotoRepository complaintPhotoRepository;

    @Inject
    RefundRepository refundRepository;

    private static final String countryName = "Test country";
    private static final String countryAcronym = "TC";
    private static final String stateName = "Test state";
    private static final String customerNickname = "Test nickname";
    private static final String customerPassword = "Password!";
    private static final String customerPhone = " 123456789";
    private static final String customerEmail = "test@email.pl";
    private static final String customerStreet = "Test street";
    private static final int customerHomeNumber = 1;
    private static final int customerLocalNumber = 2;
    private static final String customerCity = " Test City";
    private static final String customerZipCode = "12-345";
    private static final String customerName = "Test name";
    private static final String customerSurname = "Test surname";
    private static final String categoryName = "Test category name";
    private static final String categoryDescription = "Test category description";
    private static final String productOneName = "Test product one name";
    private static final String productOneDescription = "Test product one description";
    private static final int productOneDeposit = 100;
    private static final int productOneInsurance = 10;
    private static final String productTwoName = "Test product two name";
    private static final String productTwoDescription = "Test product two description";
    private static final int productTwoDeposit = 200;
    private static final int productTwoInsurance = 20;
    private static final String officeBoxOneCode = "Test office box one";
    private static final String officeBoxTwoCode = "Test office box two";
    private static final int acceptableRefund = 100;
    private static final int unacceptableRefundInsurance = 500;
    private static final int unacceptableRefundDeposit = 700;
    private static final String imageUrl = "https://firebasestorage.googleapis.com/v0/b/elektrolend-images.appspot.com/o/images%2Fcategories%2F783px-Test-Logo.svg.png?alt=media&token=14262ea4-a053-4498-89d6-c01941d35b25";

    private static int countryID;
    private static int stateID;
    private static int customerID;
    private static int categoryID;
    private static int productOneID;
    private static int productTwoID;
    private static int officeBoxOneID;
    private static int officeBoxTwoID;
    private static int priceForProductOneID;
    private static int priceForProductTwoID;
    private static int orderOneID;
    private static int orderProductOneID;
    private static int orderProductTwoID;
    private static int complaintDepositID;
    private static int complaintInsuranceID;
    private static int complaintPhotoDepositID;
    private static int complaintPhotoInsuranceID;
    private static int refundID;


    @Test
    @Order(1)
    @DisplayName("Prepare data for test")
    void prepareDataForTest() {
        given()
                .body(String.format(
                                """
                                    {
                                        "name": "%s",
                                         "acronym": "%s"
                                    }
                                """,
                                countryName,
                                countryAcronym
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/countries")
                .then()
                .statusCode(Response.Status.CREATED.getStatusCode());

        countryID = Math.toIntExact(countryRepository.findByNameOptional(countryName)
                .orElseThrow(() -> new NoSuchElementException("Country not found"))
                .getId());

        given()
                .body(String.format(
                                """
                                    {
                                      "name": "%s",
                                      "countryID": %d
                                    }
                                """,
                                stateName,
                                countryID
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/states")
                .then()
                .statusCode(Response.Status.CREATED.getStatusCode());

        stateID = Math.toIntExact(stateRepository.findByNameOptional(stateName)
                .orElseThrow(() -> new NoSuchElementException("State not found"))
                .getId());

        given()
                .body(String.format(
                                """
                                    {
                                      "nickname": "%s",
                                      "password": "%s",
                                      "phone": "%s",
                                      "email":"%s",
                                      "street": "%s",
                                      "homeNumber": %d,
                                      "localNumber": %d,
                                      "city": "%s",
                                      "zipCode": "%s",
                                      "state": "%s",
                                      "country": "%s",
                                      "name": "%s",
                                      "surname": "%s"
                                    }
                                """,
                                customerNickname,
                                customerPassword,
                                customerPhone,
                                customerEmail,
                                customerStreet,
                                customerHomeNumber,
                                customerLocalNumber,
                                customerCity,
                                customerZipCode,
                                stateName,
                                countryName,
                                customerName,
                                customerSurname
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/users/register")
                .then()
                .statusCode(Response.Status.CREATED.getStatusCode());

        customerID = Math.toIntExact(customerRepository.findByNicknameOptional(customerNickname)
                .orElseThrow(() -> new NoSuchElementException("Customer not found"))
                .getId());

        given()
                .body(String.format(
                                """
                                    {
                                      "name": "%s",
                                      "description" : "%s"
                                    }
                                """,
                                categoryName,
                                categoryDescription
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/categories")
                .then()
                .statusCode(Response.Status.CREATED.getStatusCode());

        categoryID = Math.toIntExact(categoryRepository.findByNameOptional(categoryName)
                .orElseThrow(() -> new NoSuchElementException("Category not found"))
                .getId());

        given()
                .body(String.format(
                                """
                                    {
                                        "categoryID": %d,
                                        "name": "%s",
                                        "description": "%s",
                                        "deposit": %d,
                                        "insurance": %d,
                                        "isAvailable": true
                                    }
                                """,
                                categoryID,
                                productOneName,
                                productOneDescription,
                                productOneDeposit,
                                productOneInsurance
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/products")
                .then()
                .statusCode(Response.Status.CREATED.getStatusCode());

        productOneID = Math.toIntExact(productRepository.findByNameOptional(productOneName)
                .orElseThrow(() -> new NoSuchElementException("Product one not found"))
                .getId());

        given()
                .body(String.format(
                                """
                                    {
                                        "productID": %d,
                                        "pricePerDayNetto": 80,
                                        "pricePerDayBrutto": 98.4,
                                        "beginDate": "%s"
                                    }
                                """,
                                productOneID,
                                LocalDate.now().toString()
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/prices")
                .then()
                .statusCode(Response.Status.CREATED.getStatusCode());

        priceForProductOneID = Math.toIntExact(priceRepository.find(
                        "SELECT p FROM price p WHERE productID = ?1 AND beginDate = ?2",
                        productOneID,
                        LocalDate.now())
                .firstResultOptional()
                .orElseThrow(() -> new NoSuchElementException("Price for product one not found one not found"))
                .getId());

        given()
                .body(String.format(
                                """
                                    {
                                        "categoryID": %d,
                                        "name": "%s",
                                        "description": "%s",
                                        "deposit": %d,
                                        "insurance": %d,
                                        "isAvailable": true
                                    }
                                """,
                                categoryID,
                                productTwoName,
                                productTwoDescription,
                                productTwoDeposit,
                                productTwoInsurance
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/products")
                .then()
                .statusCode(Response.Status.CREATED.getStatusCode());

        productTwoID = Math.toIntExact(productRepository.findByNameOptional(productTwoName)
                .orElseThrow(() -> new NoSuchElementException("Product two not found"))
                .getId());

        given()
                .body(String.format(
                                """
                                    {
                                        "productID": %d,
                                        "pricePerDayNetto": 100,
                                        "pricePerDayBrutto": 123,
                                        "beginDate": "%s"
                                    }
                                """,
                                productTwoID,
                                LocalDate.now().toString()
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/prices")
                .then()
                .statusCode(Response.Status.CREATED.getStatusCode());

        priceForProductTwoID = Math.toIntExact(priceRepository.find(
                        "SELECT p FROM price p WHERE productID = ?1 AND beginDate = ?2",
                        productTwoID,
                        LocalDate.now())
                .firstResultOptional()
                .orElseThrow(() -> new NoSuchElementException("Price for product two not found"))
                .getId());

        given()
                .body(String.format(
                                """
                                    {
                                      "code": "%s"
                                    }
                                """,
                                officeBoxOneCode
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/office-boxes")
                .then()
                .statusCode(Response.Status.CREATED.getStatusCode());

        officeBoxOneID = Math.toIntExact(officeBoxRepository.findByCodeOptional(officeBoxOneCode)
                .orElseThrow(() -> new NoSuchElementException("Office box one not found"))
                .getId());

        given()
                .body(String.format(
                                """
                                    {
                                      "code": "%s"
                                    }
                                """,
                                officeBoxTwoCode
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/office-boxes")
                .then()
                .statusCode(Response.Status.CREATED.getStatusCode());

        officeBoxTwoID = Math.toIntExact(officeBoxRepository.findByCodeOptional(officeBoxTwoCode)
                .orElseThrow(() -> new NoSuchElementException("Office box two not found"))
                .getId());

        given()
                .body(String.format(
                                """
                                    {
                                      "customerID": %d,
                                      "orderInfoList": [
                                        {
                                          "productID": %d,
                                          "orderBeginDate": "2022-04-10",
                                          "orderEndDate": "2022-04-15",
                                          "serviceSecurityType": "INSURANCE"
                                        },
                                        {
                                          "productID": %d,
                                          "orderBeginDate": "%s",
                                          "orderEndDate": "%s",
                                          "serviceSecurityType": "DEPOSIT"
                                        }
                                      ]
                                    }
                                """,
                                customerID,
                                productOneID,
                                productTwoID,
                                LocalDate.now(),
                                LocalDate.now().plusDays(4)
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/orders/create-order")
                .then()
                .statusCode(Response.Status.OK.getStatusCode());

        orderOneID = Math.toIntExact(orderRepository.find("SELECT o FROM order_table o WHERE customerID = ?1 AND orderdate = ?2", customerID, LocalDate.now())
                .stream()
                .skip(0)
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Order not found"))
                .getId());

        orderProductOneID = Math.toIntExact(orderProductRepository.find("SELECT op FROM order_product op WHERE productID = ?1 AND orderID = ?2 ", productOneID, orderOneID)
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Order product not found"))
                .getId());

        orderProductTwoID = Math.toIntExact(orderProductRepository.find("SELECT op FROM order_product op WHERE productID = ?1 AND orderID = ?2", productTwoID, orderOneID)
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Order product not found"))
                .getId());

        given()
                .body(String.format(
                            """
                                {
                                    "productList": [
                                        {
                                            "orderProductID": %d,
                                            "orderProductState": "PAID"
                                        },
                                        {
                                            "orderProductID": %d,
                                            "orderProductState": "PAID"
                                        }
                                    ]
                                }
                            """,
                        orderProductOneID,
                        orderProductTwoID
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/order-products/status")
                .then()
                .statusCode(Response.Status.OK.getStatusCode());

        given()
                .body(String.format(
                        """
                            {
                                "productList": [
                                    {
                                        "orderProductID": %d,
                                        "orderProductState": "IN_OFFICE_BOX"
                                    },
                                    {
                                        "orderProductID": %d,
                                        "orderProductState": "IN_OFFICE_BOX"
                                    }
                                ]
                            }
                        """,
                        orderProductOneID,
                        orderProductTwoID
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/order-products/status")
                .then()
                .statusCode(Response.Status.OK.getStatusCode());

        given()
                .body(String.format(
                        """
                            {
                                "productList": [
                                    {
                                        "orderProductID": %d,
                                        "orderProductState": "RECEIVED"
                                    },
                                    {
                                        "orderProductID": %d,
                                        "orderProductState": "RECEIVED"
                                    }
                                ]
                            }
                        """,
                        orderProductOneID,
                        orderProductTwoID
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/order-products/status")
                .then()
                .statusCode(Response.Status.OK.getStatusCode());

    }

    @Test
    @Order(2)
    @DisplayName("Create complaint")
    void createComplaint() {
        given()
                .body(String.format(
                                """
                                    {
                                        "orderProductID": %d,
                                        "description": "desc",
                                        "imageUrls": [ "%s" ]
                                    }
                                """,
                                orderProductTwoID,
                                imageUrl
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/complaints")
                .then()
                .body("orderProductID", equalTo(orderProductTwoID))
                .body("description", equalTo("desc"))
                .body("newBoxID", equalTo(officeBoxOneID))
                .body("imageUrls", equalTo(List.of(imageUrl)))
                .body("isConsidered", equalTo(false))
                .body("customerID", equalTo(customerID))
                .body("refund", nullValue())
                .statusCode(Response.Status.OK.getStatusCode());

        complaintDepositID = Math.toIntExact(complaintRepository.find("SELECT c FROM complaint c WHERE orderProductID = ?1 AND date = ?2", orderProductTwoID, LocalDate.now())
                .firstResultOptional()
                .orElseThrow(() -> new NoSuchElementException("Complaint not found"))
                .getId());

        complaintPhotoDepositID = complaintPhotoRepository
                .findByComplaintIdOptional((long) complaintDepositID)
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Complaint photo not found"))
                .get(0)
                .getId()
                .intValue();

        given()
                .body(String.format(
                                """
                                    {
                                        "orderProductID": %d,
                                        "description": "desc",
                                        "imageUrls": [ "%s" ]
                                    }
                                """,
                                orderProductOneID,
                                imageUrl
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/complaints")
                .then()
                .body("orderProductID", equalTo(orderProductOneID))
                .body("description", equalTo("desc"))
                .body("newBoxID", equalTo(officeBoxTwoID))
                .body("imageUrls", equalTo(List.of(imageUrl)))
                .body("isConsidered", equalTo(false))
                .body("customerID", equalTo(customerID))
                .body("refund", nullValue())
                .statusCode(Response.Status.OK.getStatusCode());

        complaintInsuranceID = Math.toIntExact(complaintRepository.find("SELECT c FROM complaint c WHERE orderProductID = ?1 AND date = ?2", orderProductOneID, LocalDate.now())
                .firstResultOptional()
                .orElseThrow(() -> new NoSuchElementException("Complaint not found"))
                .getId());

        complaintPhotoInsuranceID = complaintPhotoRepository.findByComplaintIdOptional((long) complaintInsuranceID).stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Complaint photo not found"))
                .get(0)
                .getId().intValue();
    }

    @Test
    @Order(3)
    @DisplayName("Not create complaint, orderProductID not existed")
    void notCreateComplaintNotExistedOrderProduct() {
        given()
                .body(String.format(
                                """
                                    {
                                        "orderProductID": %d,
                                        "description": "desc",
                                        "imageUrls": [ "%s" ]
                                    }
                                """,
                                orderProductTwoID + 100,
                                imageUrl
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/complaints")
                .then()
                .statusCode(Response.Status.NOT_FOUND.getStatusCode())
                .body(is("Order product o podanym id nie istnieje"));
    }

    @Test
    @Order(4)
    @DisplayName("Not create complaint, not more officeBox")
    void notCreateComplaintNoMoreOfficeBox() {
        given()
                .body(String.format(
                                """
                                    {
                                        "orderProductID": %d,
                                        "description": "desc",
                                        "imageUrls": [ "%s" ]
                                    }
                                """,
                                orderProductTwoID,
                                imageUrl
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/complaints")
                .then()
                .statusCode(Response.Status.CONFLICT.getStatusCode())
                .body(is("Nie ma wystarczającej ilości skrytek. Prosimy o dostarczenie sprzętu do sklepu."));
    }

    @Test
    @Order(5)
    @DisplayName("Positive accepted complaint, add refund")
    void addRefund() {
        given()
                .body(String.format(
                        """
                            {
                                "productList": [
                                    {
                                        "orderProductID": %d,
                                        "orderProductState": "IN_OFFICE_BOX_COMPLAINT"
                                    }
                                ]
                            }
                        """,
                        orderProductTwoID
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/order-products/status")
                .then()
                .statusCode(Response.Status.OK.getStatusCode());

        given()
                .body(String.format(
                        """
                            {
                                "productList": [
                                    {
                                        "orderProductID": %d,
                                        "orderProductState": "COMPLAINT_FOR_CONSIDERATION"
                                    }
                                ]
                            }
                        """,
                        orderProductTwoID
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/order-products/status")
                .then()
                .statusCode(Response.Status.OK.getStatusCode());

        given()
                .body(String.format(
                                """
                                    {
                                        "refund": %d
                                    }
                                """,
                                acceptableRefund
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .patch(String.format("/complaints/%d", complaintDepositID))
                .then()
                .body("orderProductID", equalTo(orderProductTwoID))
                .body("refund", equalTo(acceptableRefund))
                .body("description", equalTo("desc"))
                .body("imageUrls", equalTo(List.of(imageUrl)))
                .body("isConsidered", equalTo(true))
                .statusCode(Response.Status.OK.getStatusCode());

        refundID = refundRepository
                .findByOrderProductID((long) orderProductTwoID)
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Refund not found"))
                .getId()
                .intValue();
    }

    @Test
    @Order(6)
    @DisplayName("Cannot add refund, not existed complaint")
    void noAddRefundToNotExistedComplaint() {
        given()
                .body(String.format(
                                """
                                    {
                                        "refund": %d
                                    }
                                """,
                                acceptableRefund
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .patch(String.format("/complaints/%d", complaintDepositID + 100))
                .then()
                .statusCode(Response.Status.NOT_FOUND.getStatusCode())
                .body(is("Reklamacja o podanym id nie istnieje"));
    }

    @Test
    @Order(7)
    @DisplayName("Cannot add refund, product with insurance, refund higher than price")
    void noAddRefundToProductWithLowerPriceThanRefundInsurance() {
        given()
                .body(String.format(
                                """
                                    {
                                        "refund": %d
                                    }
                                """,
                                unacceptableRefundInsurance
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .patch(String.format("/complaints/%d", complaintInsuranceID))
                .then()
                .statusCode(Response.Status.CONFLICT.getStatusCode())
                .body(is("Wysokość zwrotu jest wyższa niż wpłacono za zamówiony produkt"));
    }

    @Test
    @Order(8)
    @DisplayName("Cannot add refund, refund is too high")
    void noAddRefundToProductWithLowerPriceAndDepositThanRefund() {
        given()
                .body(String.format(
                                """
                                    {
                                        "refund":%d
                                    }
                                """,
                                unacceptableRefundDeposit
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .patch(String.format("/complaints/%d", complaintDepositID))
                .then()
                .statusCode(Response.Status.CONFLICT.getStatusCode())
                .body(is("Wysokość zwrotu jest wyższa niż wpłacono za zamówiony produkt"));
    }

    @Test
    @Order(9)
    @DisplayName("Negative refuse complaint")
    void refuseComplaint() {
        given()
                .body(String.format(
                        """
                            {
                                "productList": [
                                    {
                                        "orderProductID": %d,
                                        "orderProductState": "IN_OFFICE_BOX_COMPLAINT"
                                    }
                                ]
                            }
                        """,
                        orderProductOneID
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/order-products/status")
                .then()
                .statusCode(Response.Status.OK.getStatusCode());

        given()
                .body(String.format(
                        """
                            {
                                "productList": [
                                    {
                                        "orderProductID": %d,
                                        "orderProductState": "COMPLAINT_FOR_CONSIDERATION"
                                    }
                                ]
                            }
                        """,
                        orderProductOneID
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/order-products/status")
                .then()
                .statusCode(Response.Status.OK.getStatusCode());

        given()
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .get(String.format("/complaints/%d", complaintInsuranceID))
                .then()
                .body("orderProductID", equalTo(orderProductOneID))
                .body("description", equalTo("desc"))
                .body("imageUrls", equalTo(List.of(imageUrl)))
                .body("isConsidered", equalTo(false))
                .body("refund", nullValue())
                .statusCode(Response.Status.OK.getStatusCode());

        given()
                .body(
                    """
                        {
                        }
                    """
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .patch(String.format("/complaints/%d", complaintInsuranceID))
                .then()
                .body("orderProductID", equalTo(orderProductOneID))
                .body("description", equalTo("desc"))
                .body("imageUrls", equalTo(List.of(imageUrl)))
                .body("isConsidered", equalTo(true))
                .body("refund", nullValue())
                .statusCode(Response.Status.OK.getStatusCode());
    }

    @Test
    @Order(10)
    @DisplayName("Clear all data after test")
    void clearDataTest() {
        given()
                .when()
                .delete("/refunds/" + refundID)
                .then()
                .statusCode(Response.Status.NO_CONTENT.getStatusCode());

        given()
                .when()
                .delete("/complaints/photos/" + complaintPhotoInsuranceID)
                .then()
                .statusCode(Response.Status.NO_CONTENT.getStatusCode());

        given()
                .when()
                .delete("/complaints/photos/" + complaintPhotoDepositID)
                .then()
                .statusCode(Response.Status.NO_CONTENT.getStatusCode());

        given()
                .when()
                .delete("/complaints/" + complaintInsuranceID)
                .then()
                .statusCode(Response.Status.NO_CONTENT.getStatusCode());

        given()
                .when()
                .delete("/complaints/" + complaintDepositID)
                .then()
                .statusCode(Response.Status.NO_CONTENT.getStatusCode());

        given()
                .when()
                .delete("/order-products/" + orderProductOneID)
                .then()
                .statusCode(Response.Status.NO_CONTENT.getStatusCode());

        given()
                .when()
                .delete("/order-products/" + orderProductTwoID)
                .then()
                .statusCode(Response.Status.NO_CONTENT.getStatusCode());

        given()
                .when()
                .delete("/orders/" + orderOneID)
                .then()
                .statusCode(Response.Status.NO_CONTENT.getStatusCode());

        given()
                .when()
                .delete("/office-boxes/" + officeBoxOneID)
                .then()
                .statusCode(Response.Status.NO_CONTENT.getStatusCode());

        given()
                .when()
                .delete("/office-boxes/" + officeBoxTwoID)
                .then()
                .statusCode(Response.Status.NO_CONTENT.getStatusCode());

        given()
                .when()
                .delete("/prices/" + priceForProductOneID)
                .then()
                .statusCode(Response.Status.NO_CONTENT.getStatusCode());

        given()
                .when()
                .delete("/prices/" + priceForProductTwoID)
                .then()
                .statusCode(Response.Status.NO_CONTENT.getStatusCode());

        given()
                .when()
                .delete("/products/" + productOneID)
                .then()
                .statusCode(Response.Status.NO_CONTENT.getStatusCode());

        given()
                .when()
                .delete("/products/" + productTwoID)
                .then()
                .statusCode(Response.Status.NO_CONTENT.getStatusCode());

        given()
                .when()
                .delete("/categories/" + categoryID)
                .then()
                .statusCode(Response.Status.NO_CONTENT.getStatusCode());

        given()
                .when()
                .delete("/customers/" + customerID)
                .then()
                .statusCode(Response.Status.NO_CONTENT.getStatusCode());

        given()
                .when()
                .delete("/states/" + stateID)
                .then()
                .statusCode(Response.Status.NO_CONTENT.getStatusCode());

        given()
                .when()
                .delete("/countries/" + countryID)
                .then()
                .statusCode(Response.Status.NO_CONTENT.getStatusCode());
    }

}
