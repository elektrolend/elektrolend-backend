package integration.tests.orders;

import integration.resources.PostgresResource;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import org.acme.repository.accounts.CountryRepository;
import org.acme.repository.accounts.CustomerRepository;
import org.acme.repository.accounts.StateRepository;
import org.acme.repository.orders.OfficeBoxRepository;
import org.acme.repository.orders.OrderProductRepository;
import org.acme.repository.orders.OrderRepository;
import org.acme.repository.products.CategoryRepository;
import org.acme.repository.products.PriceRepository;
import org.acme.repository.products.ProductRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.testcontainers.shaded.com.google.common.net.HttpHeaders;

import javax.inject.Inject;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.time.LocalDate;
import java.util.NoSuchElementException;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;

@QuarkusTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@QuarkusTestResource(value=PostgresResource.class, restrictToAnnotatedClass = true)
@TestSecurity(authorizationEnabled = false)
class OrderProductStatusChangeIT {

    @Inject
    CountryRepository countryRepository;

    @Inject
    StateRepository stateRepository;

    @Inject
    CustomerRepository customerRepository;

    @Inject
    CategoryRepository categoryRepository;

    @Inject
    ProductRepository productRepository;

    @Inject
    PriceRepository priceRepository;

    @Inject
    OfficeBoxRepository officeBoxRepository;

    @Inject
    OrderProductRepository orderProductRepository;

    @Inject
    OrderRepository orderRepository;

    private static final String countryName = "Test country";
    private static final String countryAcronym = "TC";
    private static final String stateName = "Test state";
    private static final String customerNickname = "Test nickname";
    private static final String customerPassword = "Password!";
    private static final String customerPhone = " 123456789";
    private static final String customerEmail = "test@email.pl";
    private static final String customerStreet = "Test street";
    private static final int customerHomeNumber = 1;
    private static final int customerLocalNumber = 2;
    private static final String customerCity = " Test City";
    private static final String customerZipCode = "12-345";
    private static final String customerName = "Test name";
    private static final String customerSurname = "Test surname";
    private static final String categoryName = "Test category name";
    private static final String categoryDescription = "Test category description";
    private static final String productOneName = "Test product one name";
    private static final String productOneDescription = "Test product one description";
    private static final int productOneDeposit = 100;
    private static final int productOneInsurance = 10;
    private static final String productTwoName = "Test product two name";
    private static final String productTwoDescription = "Test product two description";
    private static final int productTwoDeposit = 200;
    private static final int productTwoInsurance = 20;
    private static final String officeBoxOneCode = "Test office box one";

    private static int countryID;
    private static int stateID;
    private static int customerID;
    private static int categoryID;
    private static int productOneID;
    private static int productTwoID;
    private static int officeBoxOneID;
    private static int priceForProductOneID;
    private static int priceForProductTwoID;
    private static int orderOneID;
    private static int orderProductOneID;
    private static int orderProductTwoID;

    @Test
    @Order(1)
    @DisplayName("Prepare data for test")
    void prepareDataForTest() {
        given()
                .body(String.format(
                        """
                            {
                              "name": "%s",
                              "acronym": "%s"
                            }
                        """,
                        countryName,
                        countryAcronym
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/countries")
                .then()
                .statusCode(Response.Status.CREATED.getStatusCode());

        countryID = Math.toIntExact(countryRepository.findByNameOptional(countryName)
                .orElseThrow(() -> new NoSuchElementException("Country not found"))
                .getId());

        given()
                .body(String.format(
                        """
                            {
                              "name": "%s",
                              "countryID": %d
                            }
                        """,
                        stateName,
                        countryID
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/states")
                .then()
                .statusCode(Response.Status.CREATED.getStatusCode());

        stateID = Math.toIntExact(stateRepository.findByNameOptional(stateName)
                .orElseThrow(() -> new NoSuchElementException("State not found"))
                .getId());

        given()
                .body(String.format(
                        """
                            {
                              "nickname": "%s",
                              "password": "%s",
                              "phone": "%s",
                              "email":"%s",
                              "street": "%s",
                              "homeNumber": %d,
                              "localNumber": %d,
                              "city": "%s",
                              "zipCode": "%s",
                              "state": "%s",
                              "country": "%s",
                              "name": "%s",
                              "surname": "%s"
                            }
                        """,
                        customerNickname,
                        customerPassword,
                        customerPhone,
                        customerEmail,
                        customerStreet,
                        customerHomeNumber,
                        customerLocalNumber,
                        customerCity,
                        customerZipCode,
                        stateName,
                        countryName,
                        customerName,
                        customerSurname
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/users/register")
                .then()
                .statusCode(Response.Status.CREATED.getStatusCode());

        customerID = Math.toIntExact(customerRepository.findByNicknameOptional(customerNickname)
                .orElseThrow(() -> new NoSuchElementException("Customer not found"))
                .getId());

        given()
                .body(String.format(
                        """
                            {
                              "name": "%s",
                              "description" : "%s"
                            }
                        """,
                        categoryName,
                        categoryDescription
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/categories")
                .then()
                .statusCode(Response.Status.CREATED.getStatusCode());

        categoryID = Math.toIntExact(categoryRepository.findByNameOptional(categoryName)
                .orElseThrow(() -> new NoSuchElementException("Category not found"))
                .getId());

        given()
                .body(String.format(
                        """
                            {
                                "categoryID": %d,
                                "name": "%s",
                                "description": "%s",
                                "deposit": %d,
                                "insurance": %d,
                                "isAvailable": true
                            }
                        """,
                        categoryID,
                        productOneName,
                        productOneDescription,
                        productOneDeposit,
                        productOneInsurance
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/products")
                .then()
                .statusCode(Response.Status.CREATED.getStatusCode());

        productOneID = Math.toIntExact(productRepository.findByNameOptional(productOneName)
                .orElseThrow(() -> new NoSuchElementException("Product one not found"))
                .getId());

        given()
                .body(String.format(
                        """
                            {
                                "productID":%d,
                                "pricePerDayNetto":80,
                                "pricePerDayBrutto":98.4,
                                "beginDate":"%s"
                            }
                        """,
                        productOneID,
                        LocalDate.now().toString()
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/prices")
                .then()
                .statusCode(Response.Status.CREATED.getStatusCode());

        priceForProductOneID = Math.toIntExact(priceRepository.find(
                "SELECT p FROM price p WHERE productID = ?1 AND beginDate = ?2",
                productOneID,
                LocalDate.now())
                .firstResultOptional()
                .orElseThrow(() -> new NoSuchElementException("Price for product one not found one not found"))
                .getId());

        given()
                .body(String.format(
                        """
                            {
                                "categoryID": %d,
                                "name": "%s",
                                "description": "%s",
                                "deposit": %d,
                                "insurance": %d,
                                "isAvailable": true
                            }
                        """,
                        categoryID,
                        productTwoName,
                        productTwoDescription,
                        productTwoDeposit,
                        productTwoInsurance
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/products")
                .then()
                .statusCode(Response.Status.CREATED.getStatusCode());

        productTwoID = Math.toIntExact(productRepository.findByNameOptional(productTwoName)
                .orElseThrow(() -> new NoSuchElementException("Product two not found"))
                .getId());

        given()
                .body(String.format(
                        """
                            {
                                "productID":%d,
                                "pricePerDayNetto":100,
                                "pricePerDayBrutto":123,
                                "beginDate":"%s"
                            }
                        """,
                        productTwoID,
                        LocalDate.now().toString()
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/prices")
                .then()
                .statusCode(Response.Status.CREATED.getStatusCode());

        priceForProductTwoID = Math.toIntExact(priceRepository.find(
                "SELECT p FROM price p WHERE productID = ?1 AND beginDate = ?2",
                productTwoID,
                LocalDate.now())
                .firstResultOptional()
                .orElseThrow(() -> new NoSuchElementException("Price for product two not found"))
                .getId());

        given()
                .body(String.format(
                        """
                            {
                              "code": "%s"
                            }
                        """,
                        officeBoxOneCode
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/office-boxes")
                .then()
                .statusCode(Response.Status.CREATED.getStatusCode());

        officeBoxOneID = Math.toIntExact(officeBoxRepository.findByCodeOptional(officeBoxOneCode)
                .orElseThrow(() -> new NoSuchElementException("Office box one not found"))
                .getId());

        given()
                .body(String.format(
                        """
                            {
                              "customerID": %d,
                              "orderInfoList": [
                                {
                                  "productID": %d,
                                  "orderBeginDate": "2022-04-10",
                                  "orderEndDate": "2022-04-15",
                                  "serviceSecurityType": "INSURANCE"
                                },
                                {
                                  "productID": %d,
                                  "orderBeginDate": "2022-05-12",
                                  "orderEndDate": "2022-05-16",
                                  "serviceSecurityType": "DEPOSIT"
                                }
                              ]
                            }
                        """,
                        customerID,
                        productOneID,
                        productTwoID
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/orders/create-order")
                .then()
                .statusCode(Response.Status.OK.getStatusCode());

        orderOneID = Math.toIntExact(orderRepository.find("SELECT o FROM order_table o WHERE customerID = ?1 AND orderdate = ?2", customerID, LocalDate.now())
                .stream()
                .skip(0)
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Order not found"))
                .getId());

        orderProductOneID = Math.toIntExact(orderProductRepository.find("SELECT op FROM order_product op WHERE productID = ?1 AND orderID = ?2 ", productOneID, orderOneID)
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Order product not found"))
                .getId());

        orderProductTwoID = Math.toIntExact(orderProductRepository.find("SELECT op FROM order_product op WHERE productID = ?1 AND orderID = ?2", productTwoID, orderOneID)
                .stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Order product not found"))
                .getId());
    }

    @Test
    @Order(2)
    @DisplayName("Change status for not existing order")
    void changeProductStatusForNotExistingOrderTest() {
        given()
                .body(
                        """
                            {
                              "productList": []
                            }
                        """
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/order-products/status")
                .then()
                .statusCode(Response.Status.NOT_FOUND.getStatusCode())
                .body(is("Jeden z obiektów nie istnieje"));
    }

    @Test
    @Order(3)
    @DisplayName("Change status for not existing order product")
    void changeProductStatusForNotExistingProductTest() {
        given()
                .body(String.format(
                        """
                            {
                              "productList": [
                                  {
                                    "orderProductID": %d,
                                    "orderProductState": "DAMAGED"
                                  }
                              ]
                            }
                        """,
                        orderProductTwoID + 1
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/order-products/status")
                .then()
                .statusCode(Response.Status.NOT_FOUND.getStatusCode())
                .body(is("Jeden z obiektów nie istnieje"));
    }

    @Test
    @Order(4)
    @DisplayName("Change status conflict with status changes")
    void changeStatusUnavailableChange() {
        given()
                .body(String.format(
                        """
                            {
                              "productList": [
                                  {
                                    "orderProductID": %d,
                                    "orderProductState": "FINISHED"
                                  }
                              ]
                            }
                        """,
                        orderProductTwoID
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/order-products/status")
                .then()
                .statusCode(Response.Status.CONFLICT.getStatusCode())
                .body(is("Nie można zmienić na podany status jednego z produktów"));
    }

    @Test
    @Order(5)
    @DisplayName("Change status for one product")
    void changeStatusOneProductList() {
        given()
                .body(String.format(
                        """
                            {
                              "productList": [
                                  {
                                    "orderProductID": %d,
                                    "orderProductState": "PAID"
                                  }
                              ]
                            }
                        """,
                        orderProductOneID
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/order-products/status")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .body(is("Zmieniono statusy dla podanej listy produktów"));
    }

    @Test
    @Order(6)
    @DisplayName("Change status for two products")
    void changeStatusTwoProductsList() {
        given()
                .body(String.format(
                        """
                            {
                              "productList": [
                                  {
                                    "orderProductID": %d,
                                    "orderProductState": "IN_OFFICE_BOX"
                                  },
                                  {
                                    "orderProductID": %d,
                                    "orderProductState": "PAID"
                                  }
                              ]
                            }
                        """,
                        orderProductOneID,
                        orderProductTwoID
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/order-products/status")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .body(is("Zmieniono statusy dla podanej listy produktów"));
    }

    @Test
    @Order(7)
    @DisplayName("Change status in second product is not permit")
    void changeStatusTwoProductsListNotPermitChanges() {
        given()
                .body(String.format(
                        """
                            {
                              "productList": [
                                  {
                                    "orderProductID": %d,
                                    "orderProductState": "DAMAGED"
                                  },
                                  {
                                    "orderProductID": %d,
                                    "orderProductState": "FINISHED"
                                  }
                              ]
                            }
                        """,
                        orderProductOneID,
                        orderProductTwoID
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/order-products/status")
                .then()
                .statusCode(Response.Status.CONFLICT.getStatusCode())
                .body(is("Nie można zmienić na podany status jednego z produktów"));
    }

    @Test
    @Order(8)
    @DisplayName("Clear all data after test")
    void clearDataTest() {
        given()
                .when()
                .delete("/order-products/" + orderProductOneID)
                .then()
                .statusCode(Response.Status.NO_CONTENT.getStatusCode());

        given()
                .when()
                .delete("/order-products/" + orderProductTwoID)
                .then()
                .statusCode(Response.Status.NO_CONTENT.getStatusCode());

        given()
                .when()
                .delete("/orders/" + orderOneID)
                .then()
                .statusCode(Response.Status.NO_CONTENT.getStatusCode());

        given()
                .when()
                .delete("/office-boxes/" + officeBoxOneID)
                .then()
                .statusCode(Response.Status.NO_CONTENT.getStatusCode());

        given()
                .when()
                .delete("/prices/" + priceForProductOneID)
                .then()
                .statusCode(Response.Status.NO_CONTENT.getStatusCode());

        given()
                .when()
                .delete("/prices/" + priceForProductTwoID)
                .then()
                .statusCode(Response.Status.NO_CONTENT.getStatusCode());

        given()
                .when()
                .delete("/products/" + productOneID)
                .then()
                .statusCode(Response.Status.NO_CONTENT.getStatusCode());

        given()
                .when()
                .delete("/products/" + productTwoID)
                .then()
                .statusCode(Response.Status.NO_CONTENT.getStatusCode());

        given()
                .when()
                .delete("/categories/" + categoryID)
                .then()
                .statusCode(Response.Status.NO_CONTENT.getStatusCode());

        given()
                .when()
                .delete("/customers/" + customerID)
                .then()
                .statusCode(Response.Status.NO_CONTENT.getStatusCode());

        given()
                .when()
                .delete("/states/" + stateID)
                .then()
                .statusCode(Response.Status.NO_CONTENT.getStatusCode());

        given()
                .when()
                .delete("/countries/" + countryID)
                .then()
                .statusCode(Response.Status.NO_CONTENT.getStatusCode());
    }

}
