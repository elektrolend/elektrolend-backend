package integration.tests.products;

import integration.resources.PostgresResource;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import org.acme.repository.products.CategoryRepository;
import org.acme.repository.products.ProductRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.testcontainers.shaded.com.google.common.net.HttpHeaders;

import javax.inject.Inject;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.NoSuchElementException;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

@QuarkusTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@QuarkusTestResource(value=PostgresResource.class, restrictToAnnotatedClass = true)
@TestSecurity(authorizationEnabled = false)
class BasicProductIT {

    @Inject
    CategoryRepository categoryRepository;

    @Inject
    ProductRepository productRepository;

    private static final String categoryName = "Test category name";
    private static final String categoryDescription = "Test category description";
    private static final String productName = "Test product name";
    private static final String productDescription = "Test product description";
    private static final int productDeposit = 500;
    private static final int productInsurance = 50;

    private static int categoryID;
    private static int productID;

    @Test
    @Order(1)
    @DisplayName("Prepare data")
    void prepareDataTest() {
        given()
                .body(String.format(
                        """
                            {
                              "name": "%s",
                              "description" : "%s"
                            }
                        """,
                        categoryName,
                        categoryDescription
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/categories")
                .then()
                .statusCode(Response.Status.CREATED.getStatusCode());

        categoryID = Math.toIntExact(categoryRepository.findByNameOptional(categoryName)
                .orElseThrow(() -> new NoSuchElementException("Category not found"))
                .getId());

        given()
                .when()
                .get("/categories/" + categoryID)
                .then()
                .body("id", equalTo(categoryID))
                .body("name", equalTo(categoryName))
                .body("description", equalTo(categoryDescription))
                .statusCode(Response.Status.OK.getStatusCode());
    }

    @Test
    @Order(2)
    @DisplayName("Post product")
    void postProductTest() {
        given()
                .body(String.format(
                        """
                            {
                                "categoryID": %d,
                                "name": "%s",
                                "description": "%s",
                                "deposit": %d,
                                "insurance": %d,
                                "isAvailable": true
                            }
                        """,
                        categoryID,
                        productName,
                        productDescription,
                        productDeposit,
                        productInsurance
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/products")
                .then()
                .statusCode(Response.Status.CREATED.getStatusCode());

        productID = Math.toIntExact(productRepository.findByNameOptional(productName)
                .orElseThrow(() -> new NoSuchElementException("Product not found"))
                .getId());
    }

    @Test
    @Order(3)
    @DisplayName("Get created product")
    void getCreatedProductTest() {
        given()
                .when()
                .get("/products/" + productID)
                .then()
                .body("id", equalTo(productID))
                .body("categoryID", equalTo(categoryID))
                .body("name", equalTo(productName))
                .body("description", equalTo(productDescription))
                .body("deposit", equalTo(productDeposit))
                .body("insurance", equalTo(productInsurance))
                .statusCode(Response.Status.OK.getStatusCode());
    }

    @Test
    @Order(4)
    @DisplayName("Delete product")
    void deleteProductTest() {
        given()
                .when()
                .delete("/products/" + productID)
                .then()
                .statusCode(Response.Status.NO_CONTENT.getStatusCode());
    }

    @Test
    @Order(5)
    @DisplayName("Try to get deleted product")
    void getDeletedProductTest() {
        given()
                .when()
                .get("/products/" + productID)
                .then()
                .statusCode(Response.Status.NOT_FOUND.getStatusCode());
    }

    @Test
    @Order(6)
    @DisplayName("Delete category")
    void deleteCategoryTest() {
        given()
                .when()
                .delete("/categories/" + categoryID)
                .then()
                .statusCode(Response.Status.NO_CONTENT.getStatusCode());
    }

    @Test
    @Order(7)
    @DisplayName("Try to get deleted category")
    void getDeletedCategoryTest() {
        given()
                .when()
                .get("/categories/" + categoryID)
                .then()
                .statusCode(Response.Status.NOT_FOUND.getStatusCode());
    }

}
