package integration.tests.products;

import integration.resources.PostgresResource;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import org.acme.repository.products.CategoryRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.testcontainers.shaded.com.google.common.net.HttpHeaders;

import javax.inject.Inject;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.NoSuchElementException;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

@QuarkusTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@QuarkusTestResource(value=PostgresResource.class, restrictToAnnotatedClass = true)
@TestSecurity(authorizationEnabled = false)
class BasicCategoryIT {

    @Inject
    CategoryRepository categoryRepository;

    private static final String categoryName = "Test category name";
    private static final String categoryDescription = "Test category description";

    private static int categoryID;

    @Test
    @Order(1)
    @DisplayName("Post category")
    void postCategoryTest() {
        given()
                .body(String.format(
                        """
                            {
                              "name": "%s",
                              "description" : "%s"
                            }
                        """,
                        categoryName,
                        categoryDescription
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/categories")
                .then()
                .statusCode(Response.Status.CREATED.getStatusCode());

        categoryID = Math.toIntExact(categoryRepository.findByNameOptional(categoryName)
                .orElseThrow(() -> new NoSuchElementException("Category not found"))
                .id);
    }

    @Test
    @Order(2)
    @DisplayName("Get created category")
    void getCreatedCategoryTest() {
        given()
                .when()
                .get("/categories/" + categoryID)
                .then()
                .body("id", equalTo(categoryID))
                .body("name", equalTo(categoryName))
                .body("description", equalTo(categoryDescription))
                .statusCode(Response.Status.OK.getStatusCode());
    }

    @Test
    @Order(3)
    @DisplayName("Delete category")
    void deleteCategoryTest() {
        given()
                .when()
                .delete("/categories/" + categoryID)
                .then()
                .statusCode(Response.Status.NO_CONTENT.getStatusCode());
    }

    @Test
    @Order(4)
    @DisplayName("Try to get deleted category")
    void getDeletedCategoryTest() {
        given()
                .when()
                .get("/categories/" + categoryID)
                .then()
                .statusCode(Response.Status.NOT_FOUND.getStatusCode());
    }

}
