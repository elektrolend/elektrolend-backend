package integration.tests.products;

import integration.resources.PostgresResource;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import org.acme.repository.products.CategoryRepository;
import org.acme.repository.products.ProductRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.testcontainers.shaded.com.google.common.net.HttpHeaders;

import javax.inject.Inject;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;

@QuarkusTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@QuarkusTestResource(value = PostgresResource.class, restrictToAnnotatedClass = true)
@TestSecurity(authorizationEnabled = false)
class ProductPaginationIT {

    @Inject
    CategoryRepository categoryRepository;

    @Inject
    ProductRepository productRepository;

    private static final String categoryName = "Test category name";
    private static final String categoryDescription = "Test category description";
    private static final String productDescription = "Test product description";
    private static final int productDeposit = 100;
    private static final int productInsurance = 10;

    private static final String productOneName = "Test product one name";
    private static final String productTwoName = "Test product two name";
    private static final String productThreeName = "Test product three name";
    private static final String productFourName = "Test product four name";
    private static final String productFiveName = "Test product five name";

    List<String> productNameList = List.of(
            productOneName,
            productTwoName,
            productThreeName,
            productFourName,
            productFiveName
    );

    private static int categoryID;

    private static final List<Integer> productIdList = new ArrayList<>();

    @Test
    @Order(1)
    @DisplayName("Prepare data")
    void prepareDataTest() {
        given()
                .body(String.format(
                        """
                            {
                              "name": "%s",
                              "description" : "%s"
                            }
                        """,
                        categoryName,
                        categoryDescription
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/categories")
                .then()
                .statusCode(Response.Status.CREATED.getStatusCode());

        categoryID = Math.toIntExact(categoryRepository.findByNameOptional(categoryName)
                .orElseThrow(() -> new NoSuchElementException("Category not found"))
                .getId());

        given()
                .when()
                .get("/categories/" + categoryID)
                .then()
                .body("id", equalTo(categoryID))
                .body("name", equalTo(categoryName))
                .body("description", equalTo(categoryDescription))
                .statusCode(Response.Status.OK.getStatusCode());

        for (int i = 0; i < 5; i++) {
            given()
                    .body(String.format(
                            """
                                {
                                    "categoryID": %d,
                                    "name": "%s",
                                    "description": "%s",
                                    "deposit": %d,
                                    "insurance": %d,
                                    "isAvailable": true
                                }
                            """,
                            categoryID,
                            productNameList.get(i),
                            productDescription,
                            productDeposit,
                            productInsurance
                            )
                    )
                    .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                    .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                    .when()
                    .post("/products")
                    .then()
                    .statusCode(Response.Status.CREATED.getStatusCode());

            productIdList.add(Math.toIntExact(productRepository.findByNameOptional(productNameList.get(i))
                    .orElseThrow(() -> new NoSuchElementException("Product not found"))
                    .getId()));
        }

    }

    @Test
    @Order(2)
    @DisplayName("First page five products - all")
    void firstPageFiveProductsTest() {
        given()
                .when()
                .get("/products?pageNum=1&pageSize=5")
                .then()
                .body("productDTOList[0].id", equalTo(productIdList.get(0)))
                .body("productDTOList[1].id", equalTo(productIdList.get(1)))
                .body("productDTOList[2].id", equalTo(productIdList.get(2)))
                .body("productDTOList[3].id", equalTo(productIdList.get(3)))
                .body("productDTOList[4].id", equalTo(productIdList.get(4)))
                .body("productDTOList", hasSize(5))
                .body("paginationDTO.pageNum", equalTo(1))
                .body("paginationDTO.pageSize", equalTo(5))
                .body("paginationDTO.totalResults", equalTo(5))
                .statusCode(Response.Status.OK.getStatusCode());
    }

    @Test
    @Order(3)
    @DisplayName("First page one product - first id")
    void firstPageOneProductTest() {
        given()
                .when()
                .get("/products?pageNum=1&pageSize=1")
                .then()
                .body("productDTOList[0].id", equalTo(productIdList.get(0)))
                .body("productDTOList", hasSize(1))
                .body("paginationDTO.pageNum", equalTo(1))
                .body("paginationDTO.pageSize", equalTo(1))
                .body("paginationDTO.totalResults", equalTo(5))
                .statusCode(Response.Status.OK.getStatusCode());
    }

    @Test
    @Order(4)
    @DisplayName("Second page two products - second and third ids")
    void secondPageTwoProductsTest() {
        given()
                .when()
                .get("/products?pageNum=2&pageSize=2")
                .then()
                .body("productDTOList[0].id", equalTo(productIdList.get(2)))
                .body("productDTOList[1].id", equalTo(productIdList.get(3)))
                .body("productDTOList", hasSize(2))
                .body("paginationDTO.pageNum", equalTo(2))
                .body("paginationDTO.pageSize", equalTo(2))
                .body("paginationDTO.totalResults", equalTo(5))
                .statusCode(Response.Status.OK.getStatusCode());
    }

    @Test
    @Order(5)
    @DisplayName("Second page five products - empty result")
    void secondPageFiveProductsTest() {
        given()
                .when()
                .get("/products?pageNum=2&pageSize=5")
                .then()
                .body("productDTOList", hasSize(0))
                .body("paginationDTO.pageNum", equalTo(2))
                .body("paginationDTO.pageSize", equalTo(5))
                .body("paginationDTO.totalResults", equalTo(5))
                .statusCode(Response.Status.OK.getStatusCode());
    }


    @Test
    @Order(6)
    @DisplayName("Clear all data after test")
    void clearDataTest() {
        for (int i = 0; i < 5; i++) {
            given()
                    .when()
                    .delete("/products/" + productIdList.get(i))
                    .then()
                    .statusCode(Response.Status.NO_CONTENT.getStatusCode());
        }

        given()
                .when()
                .delete("/categories/" + categoryID)
                .then()
                .statusCode(Response.Status.NO_CONTENT.getStatusCode());
    }

}
