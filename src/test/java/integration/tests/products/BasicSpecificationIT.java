package integration.tests.products;

import integration.resources.PostgresResource;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import org.acme.repository.products.CategoryRepository;
import org.acme.repository.products.ProductRepository;
import org.acme.repository.products.SpecificationRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.testcontainers.shaded.com.google.common.net.HttpHeaders;

import javax.inject.Inject;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.NoSuchElementException;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

@QuarkusTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@QuarkusTestResource(value = PostgresResource.class, restrictToAnnotatedClass = true)
@TestSecurity(authorizationEnabled = false)
class BasicSpecificationIT {

    @Inject
    CategoryRepository categoryRepository;

    @Inject
    ProductRepository productRepository;

    @Inject
    SpecificationRepository specificationRepository;

    private static final String categoryName = "Test category name";
    private static final String categoryDescription = "Test category description";
    private static final String productName = "Test product name";
    private static final String productDescription = "Test product description";
    private static final int productDeposit = 500;
    private static final int productInsurance = 50;
    private static final String specificationOneKey = "TestSpecificationOneKey";
    private static final String specificationOneValue = "TestSpecificationOneValue";
    private static final String specificationTwoKey = "TestSpecificationTwoKey";
    private static final String specificationTwoValue = "TestSpecificationTwoValue";

    private static int categoryID;
    private static int productID;
    private static int specificationOneID;
    private static int specificationTwoID;

    @Test
    @Order(1)
    @DisplayName("Prepare data")
    void prepareDataTest() {
        given()
                .body(String.format(
                        """
                            {
                              "name": "%s",
                              "description" : "%s"
                            }
                        """,
                        categoryName,
                        categoryDescription
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/categories")
                .then()
                .statusCode(Response.Status.CREATED.getStatusCode());

        categoryID = Math.toIntExact(categoryRepository.findByNameOptional(categoryName)
                .orElseThrow(() -> new NoSuchElementException("Category not found"))
                .getId());

        given()
                .when()
                .get("/categories/" + categoryID)
                .then()
                .body("id", equalTo(categoryID))
                .body("name", equalTo(categoryName))
                .body("description", equalTo(categoryDescription))
                .statusCode(Response.Status.OK.getStatusCode());

        given()
                .body(String.format(
                        """
                            {
                                "categoryID": %d,
                                "name": "%s",
                                "description": "%s",
                                "deposit": %d,
                                "insurance": %d,
                                "isAvailable": true
                            }
                        """,
                        categoryID,
                        productName,
                        productDescription,
                        productDeposit,
                        productInsurance
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/products")
                .then()
                .statusCode(Response.Status.CREATED.getStatusCode());

        productID = Math.toIntExact(productRepository.findByNameOptional(productName)
                .orElseThrow(() -> new NoSuchElementException("Product not found"))
                .getId());
    }

    @Test
    @Order(2)
    @DisplayName("Post specification")
    void postSpecificationsTest() {
        given()
                .body(String.format(
                        """
                            {
                                "productID": "%d",
                                "key": "%s",
                                "value": "%s"
                            }
                        """,
                        productID,
                        specificationOneKey,
                        specificationOneValue
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/specifications")
                .then()
                .statusCode(Response.Status.CREATED.getStatusCode());

        specificationOneID = Math.toIntExact(specificationRepository.findByProductIdAndKeyOptional(productID, specificationOneKey)
                .orElseThrow(() -> new NoSuchElementException("Specification not found"))
                .getId());

        given()
                .body(String.format(
                        """
                            {
                                "productID": "%d",
                                "key": "%s",
                                "value": "%s"
                            }
                        """,
                        productID,
                        specificationTwoKey,
                        specificationTwoValue
                        )
                )
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/specifications")
                .then()
                .statusCode(Response.Status.CREATED.getStatusCode());

        specificationTwoID = Math.toIntExact(specificationRepository.findByProductIdAndKeyOptional(productID, specificationTwoKey)
                .orElseThrow(() -> new NoSuchElementException("Specification not found"))
                .getId());
    }

    @Test
    @Order(3)
    @DisplayName("Get product with specifications")
    void getProductWithSpecificationsTest() {
        given()
                .when()
                .get("/products/" + productID)
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .body("id", equalTo(productID))
                .body("categoryID", equalTo(categoryID))
                .body("name", equalTo(productName))
                .body("description", equalTo(productDescription))
                .body("deposit", equalTo(productDeposit))
                .body("specificationsMap." + specificationOneKey, equalTo(specificationOneValue))
                .body("specificationsMap." + specificationTwoKey, equalTo(specificationTwoValue))
                .statusCode(Response.Status.OK.getStatusCode());
    }

    @Test
    @Order(4)
    @DisplayName("Clear all data after test")
    void clearDataTest() {
        given()
                .when()
                .delete("/specifications/" + specificationOneID)
                .then()
                .statusCode(Response.Status.NO_CONTENT.getStatusCode());

        given()
                .when()
                .delete("/specifications/" + specificationTwoID)
                .then()
                .statusCode(Response.Status.NO_CONTENT.getStatusCode());

        given()
                .when()
                .delete("/products/" + productID)
                .then()
                .statusCode(Response.Status.NO_CONTENT.getStatusCode());

        given()
                .when()
                .delete("/categories/" + categoryID)
                .then()
                .statusCode(Response.Status.NO_CONTENT.getStatusCode());
    }

}
