package org.acme.services.products;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import io.quarkus.test.security.TestSecurity;
import org.acme.dto.products.CategoryDTO;
import org.acme.repository.products.CategoryRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;

@QuarkusTest
@TestSecurity(authorizationEnabled = false)
class CategoryServiceTest {

    @InjectMock
    CategoryRepository categoryRepository;

    @Inject
    CategoryService categoryService;

    @Test
    void addCategoryTest() {
        doNothing().when(categoryRepository).persist(Collections.singleton(any()));

        CategoryDTO categoryDTO = new CategoryDTO();
        categoryDTO.setName("Category name");
        categoryDTO.setDescription("Category description");
        categoryDTO.setImageUrl("Category imageUrl");

        CategoryDTO categoryResult = categoryService.addCategory(categoryDTO);

        Assertions.assertEquals(categoryResult, categoryDTO);
    }

}