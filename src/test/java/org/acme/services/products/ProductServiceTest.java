package org.acme.services.products;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import io.quarkus.test.security.TestSecurity;
import org.acme.dto.orders.OrderInfoDTO;
import org.acme.dto.products.ProductCreateUpdateDTO;
import org.acme.models.orders.ServiceSecurityType;
import org.acme.models.products.Price;
import org.acme.models.products.Product;
import org.acme.repository.products.ProductRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.inject.Inject;
import java.time.LocalDate;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import static java.time.temporal.ChronoUnit.DAYS;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@QuarkusTest
@TestSecurity(authorizationEnabled = false)
class ProductServiceTest {

    @InjectMock
    ProductRepository productRepository;

    @Inject
    ProductService productService;

    @Mock
    Product product;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void calculatePriceWithOnePriceDepositTest() {
        when(productRepository.findById(1L)).thenReturn(product);

        Price price = new Price();
        price.pricePerDayBrutto = 12;
        price.pricePerDayNetto = 10;
        price.beginDate = LocalDate.now();

        when(product.priceList).thenReturn(List.of(price));
        when(product.deposit).thenReturn(100);

        OrderInfoDTO orderInfoDTO = new OrderInfoDTO();
        orderInfoDTO.setProductID(1L);
        orderInfoDTO.setOrderProductID(1L);
        orderInfoDTO.setOrderBeginDate(LocalDate.now());
        orderInfoDTO.setOrderEndDate(LocalDate.now().plus(3, DAYS));
        orderInfoDTO.setServiceSecurityType(ServiceSecurityType.DEPOSIT);

        int expectedPrice = 136;
        int priceResult = productService.calculatePrice(orderInfoDTO);

        Assertions.assertEquals(expectedPrice, priceResult);
    }

    @Test
    void calculatePriceWithOnePriceInsuranceTest() {
        when(productRepository.findById(1L)).thenReturn(product);

        Price price = new Price();
        price.pricePerDayBrutto = 12;
        price.pricePerDayNetto = 10;
        price.beginDate = LocalDate.now();

        when(product.priceList).thenReturn(List.of(price));
        when(product.insurance).thenReturn(50);

        OrderInfoDTO orderInfoDTO = new OrderInfoDTO();
        orderInfoDTO.setProductID(1L);
        orderInfoDTO.setOrderProductID(1L);
        orderInfoDTO.setOrderBeginDate(LocalDate.now());
        orderInfoDTO.setOrderEndDate(LocalDate.now().plus(3, DAYS));
        orderInfoDTO.setServiceSecurityType(ServiceSecurityType.INSURANCE);

        int expectedPrice = 86;
        int priceResult = productService.calculatePrice(orderInfoDTO);

        Assertions.assertEquals(expectedPrice, priceResult);
    }

    @Test
    void calculatePriceWithNoPriceListTest() {
        when(productRepository.findById(1L)).thenReturn(product);

        when(product.priceList).thenReturn(List.of());
        when(product.deposit).thenReturn(100);

        OrderInfoDTO orderInfoDTO = new OrderInfoDTO();
        orderInfoDTO.setProductID(1L);
        orderInfoDTO.setOrderProductID(1L);
        orderInfoDTO.setOrderBeginDate(LocalDate.now());
        orderInfoDTO.setOrderEndDate(LocalDate.now().plus(3, DAYS));
        orderInfoDTO.setServiceSecurityType(ServiceSecurityType.DEPOSIT);

        Assertions.assertThrows(NoSuchElementException.class, () -> productService.calculatePrice(orderInfoDTO));
    }

    @Test
    void calculatePriceWithManyPriceListTest() {
        when(productRepository.findById(1L)).thenReturn(product);

        Price priceOne = new Price();
        priceOne.pricePerDayBrutto = 12;
        priceOne.pricePerDayNetto = 10;
        priceOne.beginDate = LocalDate.now();

        Price priceTwo = new Price();
        priceTwo.pricePerDayBrutto = 24;
        priceTwo.pricePerDayNetto = 20;
        priceTwo.beginDate = LocalDate.now().minus(10, DAYS);
        priceTwo.endDate = LocalDate.now().minus(6, DAYS);

        Price priceThree = new Price();
        priceThree.pricePerDayBrutto = 36;
        priceThree.pricePerDayNetto = 30;
        priceThree.beginDate = LocalDate.now().minus(5, DAYS);
        priceThree.endDate = LocalDate.now().minus(1, DAYS);

        when(product.priceList).thenReturn(List.of(priceOne, priceTwo, priceThree));
        when(product.deposit).thenReturn(100);

        OrderInfoDTO orderInfoDTO = new OrderInfoDTO();
        orderInfoDTO.setProductID(1L);
        orderInfoDTO.setOrderProductID(1L);
        orderInfoDTO.setOrderBeginDate(LocalDate.now());
        orderInfoDTO.setOrderEndDate(LocalDate.now().plus(3, DAYS));
        orderInfoDTO.setServiceSecurityType(ServiceSecurityType.DEPOSIT);

        int expectedPrice = 136;
        int priceResult = productService.calculatePrice(orderInfoDTO);

        Assertions.assertEquals(expectedPrice, priceResult);
    }

    @Test
    void updateProduct() {
        doNothing().when(productRepository).persist(Collections.singleton(any()));

        Product product = new Product();
        Map<String, String> specificationMap = new HashMap<>();
        specificationMap.put("RAM", "8GB");

        ProductCreateUpdateDTO productCreateUpdateDTO = new ProductCreateUpdateDTO();
        productCreateUpdateDTO.setCategoryID(2L);
        productCreateUpdateDTO.setName("Updated product");
        productCreateUpdateDTO.setDescription("Updated description");
        productCreateUpdateDTO.setSpecificationsMap(specificationMap);
        productCreateUpdateDTO.setDeposit(100);
        productCreateUpdateDTO.setInsurance(50);
        productCreateUpdateDTO.setImageUrl("Image url");

        Product expectedProduct = productService.updateProduct(product, productCreateUpdateDTO);

        Assertions.assertEquals(product, expectedProduct);
    }

}
