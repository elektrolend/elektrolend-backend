package org.acme.services.products;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import io.quarkus.test.security.TestSecurity;
import org.acme.dto.products.SpecificationDTO;
import org.acme.repository.products.SpecificationRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;

@QuarkusTest
@TestSecurity(authorizationEnabled = false)
class SpecificationServiceTest {

    @InjectMock
    SpecificationRepository specificationRepository;

    @Inject
    SpecificationService specificationService;

    @Test
    void addSpecificationTest() {
        doNothing().when(specificationRepository).persist(Collections.singleton(any()));

        SpecificationDTO specificationDTO = new SpecificationDTO();
        specificationDTO.setId(1L);
        specificationDTO.setProductID(2L);
        specificationDTO.setKey("RAM");
        specificationDTO.setValue("8GB");

        SpecificationDTO specificationResult = specificationService.addSpecification(specificationDTO);
        specificationDTO.setProductID(null);

        Assertions.assertEquals(specificationResult, specificationDTO);
    }

}