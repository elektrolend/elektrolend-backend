package org.acme.services.products;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import io.quarkus.test.security.TestSecurity;
import org.acme.dto.products.PriceDTO;
import org.acme.repository.products.PriceRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.time.LocalDate;
import java.util.Collections;

import static java.time.temporal.ChronoUnit.DAYS;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;

@QuarkusTest
@TestSecurity(authorizationEnabled = false)
class PriceServiceTest {

    @InjectMock
    PriceRepository priceRepository;

    @Inject
    PriceService priceService;

    @Test
    void addPriceTest() {
        doNothing().when(priceRepository).persist(Collections.singleton(any()));

        PriceDTO priceDTO = new PriceDTO();
        priceDTO.setId(1L);
        priceDTO.setProductID(2L);
        priceDTO.setPricePerDayNetto(100);
        priceDTO.setPricePerDayBrutto(123);
        priceDTO.setBeginDate(LocalDate.now());
        priceDTO.setEndDate(LocalDate.now().plus(10, DAYS));

        PriceDTO priceResult = priceService.addPrice(priceDTO);
        priceDTO.setProductID(null);

        Assertions.assertEquals(priceResult, priceDTO);
    }

}
