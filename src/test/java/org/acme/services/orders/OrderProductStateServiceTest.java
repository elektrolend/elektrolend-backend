package org.acme.services.orders;

import io.quarkus.test.security.TestSecurity;
import org.acme.models.orders.OrderProductState;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

@TestSecurity(authorizationEnabled = false)
class OrderProductStateServiceTest {

    OrderProductStateService productStateService = OrderProductStateService.getInstance();

    @ParameterizedTest
    @MethodSource("changeOrderProductStateProvider")
    @DisplayName("Order product state should be changed")
    void shouldChangeOrderProductState(OrderProductState fromState, OrderProductState toState) {
        boolean success = productStateService.isAvailableStatusChange(fromState, toState);
        Assertions.assertTrue(success);
    }

    @ParameterizedTest
    @MethodSource("notChangeOrderProductStateProvider")
    @DisplayName("Order product state should not be changed")
    void notShouldChangeOrderProductState(OrderProductState fromState, OrderProductState toState) {
        boolean success = productStateService.isAvailableStatusChange(fromState, toState);
        Assertions.assertFalse(success);
    }

    private static Stream<Arguments> changeOrderProductStateProvider() {
        return Stream.of(
                Arguments.of(OrderProductState.TO_REALIZATION, OrderProductState.PAID),
                Arguments.of(OrderProductState.TO_REALIZATION, OrderProductState.CANCELED),
                Arguments.of(OrderProductState.CANCELED, OrderProductState.FINISHED),
                Arguments.of(OrderProductState.PAID, OrderProductState.IN_OFFICE_BOX),
                Arguments.of(OrderProductState.IN_OFFICE_BOX, OrderProductState.RECEIVED),
                Arguments.of(OrderProductState.IN_OFFICE_BOX, OrderProductState.FINISHED),
                Arguments.of(OrderProductState.RECEIVED, OrderProductState.NOT_DAMAGED),
                Arguments.of(OrderProductState.RECEIVED, OrderProductState.DAMAGED),
                Arguments.of(OrderProductState.NOT_DAMAGED, OrderProductState.BACK_TO_OFFICE_BOX),
                Arguments.of(OrderProductState.BACK_TO_OFFICE_BOX, OrderProductState.BACK_TO_OFFICE),
                Arguments.of(OrderProductState.BACK_TO_OFFICE, OrderProductState.REFUND),
                Arguments.of(OrderProductState.REFUND, OrderProductState.FINISHED),
                Arguments.of(OrderProductState.DAMAGED, OrderProductState.IN_OFFICE_BOX_COMPLAINT),
                Arguments.of(OrderProductState.IN_OFFICE_BOX_COMPLAINT, OrderProductState.COMPLAINT_FOR_CONSIDERATION),
                Arguments.of(OrderProductState.COMPLAINT_FOR_CONSIDERATION, OrderProductState.COMPLAINT_ACCEPTED),
                Arguments.of(OrderProductState.COMPLAINT_FOR_CONSIDERATION, OrderProductState.COMPLAINT_NOT_ACCEPTED),
                Arguments.of(OrderProductState.COMPLAINT_ACCEPTED, OrderProductState.REFUND),
                Arguments.of(OrderProductState.COMPLAINT_NOT_ACCEPTED, OrderProductState.FINISHED)
        );
    }

    public static Stream<Arguments> notChangeOrderProductStateProvider() {
        return Stream.of(
                Arguments.of(OrderProductState.TO_REALIZATION, OrderProductState.TO_REALIZATION),
                Arguments.of(OrderProductState.CANCELED, OrderProductState.IN_OFFICE_BOX),
                Arguments.of(OrderProductState.IN_OFFICE_BOX, OrderProductState.PAID),
                Arguments.of(OrderProductState.NOT_DAMAGED, OrderProductState.IN_OFFICE_BOX),
                Arguments.of(OrderProductState.NOT_DAMAGED, OrderProductState.NOT_DAMAGED),
                Arguments.of(OrderProductState.BACK_TO_OFFICE_BOX, OrderProductState.IN_OFFICE_BOX),
                Arguments.of(OrderProductState.BACK_TO_OFFICE, OrderProductState.FINISHED),
                Arguments.of(OrderProductState.DAMAGED, OrderProductState.COMPLAINT_FOR_CONSIDERATION),
                Arguments.of(OrderProductState.DAMAGED, OrderProductState.TO_REALIZATION),
                Arguments.of(OrderProductState.COMPLAINT_FOR_CONSIDERATION, OrderProductState.FINISHED),
                Arguments.of(OrderProductState.COMPLAINT_FOR_CONSIDERATION, OrderProductState.FINISHED),
                Arguments.of(OrderProductState.COMPLAINT_ACCEPTED, OrderProductState.CANCELED),
                Arguments.of(OrderProductState.COMPLAINT_NOT_ACCEPTED, OrderProductState.CANCELED),
                Arguments.of(OrderProductState.REFUND, OrderProductState.TO_REALIZATION)
        );
    }

}
