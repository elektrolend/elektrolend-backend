package org.acme.services.orders;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import org.acme.models.orders.Complaint;
import org.acme.models.orders.OrderProduct;
import org.acme.models.orders.ServiceSecurityType;
import org.acme.models.products.Product;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.inject.Inject;

import static org.mockito.Mockito.when;

@QuarkusTest
@TestSecurity(authorizationEnabled = false)
class ComplaintServiceTest {

    @Inject
    ComplaintService complaintService;

    @Mock
    Complaint complaint;

    @Mock
    OrderProduct orderProduct;

    @Mock
    Product product;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        when(complaint.getOrderProduct()).thenReturn(orderProduct);
        when(orderProduct.getPrice()).thenReturn(100);
    }

    @Test
    void checkAppropriateRefundWithDepositTest() {
        when(orderProduct.getServiceSecurityType()).thenReturn(ServiceSecurityType.DEPOSIT);
        Assertions.assertTrue(complaintService.isRefundAppropriate(complaint, 100));
        Assertions.assertFalse(complaintService.isRefundAppropriate(complaint, 101));
    }

    @Test
    void checkAppropriateRefundWithInsuranceTest() {
        when(orderProduct.getServiceSecurityType()).thenReturn(ServiceSecurityType.INSURANCE);
        when(orderProduct.getProduct()).thenReturn(product);
        when(product.getInsurance()).thenReturn(20);

        Assertions.assertTrue(complaintService.isRefundAppropriate(complaint, 80));
        Assertions.assertFalse(complaintService.isRefundAppropriate(complaint, 81));
    }

}
