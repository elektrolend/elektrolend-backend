package org.acme.endpoints.orders;

import io.quarkus.test.TestTransaction;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import io.quarkus.test.security.TestSecurity;
import org.acme.dto.orders.OrderProductChangeStatusDTO;
import org.acme.dto.orders.OrderProductStatusDTO;
import org.acme.models.orders.OrderProductState;
import org.acme.services.orders.OrderProductService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.testcontainers.shaded.com.google.common.net.HttpHeaders;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.mockito.Mockito.when;

@QuarkusTest
@RunWith(MockitoJUnitRunner.class)
@TestTransaction
@TestSecurity(authorizationEnabled = false)
class OrdersResourceTest {

    @InjectMock
    OrderProductService orderProductService;

    @Test
    @DisplayName("Change order status")
    void changeOrderStatus() {
        OrderProductChangeStatusDTO orderProductChangeStatusDTO = setupOrderChangeStatusDTO();
        when(orderProductService.changeOrderProductsStatus(orderProductChangeStatusDTO)).thenReturn(true);

        given()
                .body(orderProductChangeStatusDTO)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/order-products/status")
                .then()
                .statusCode(Response.Status.OK.getStatusCode());
    }

    private OrderProductChangeStatusDTO setupOrderChangeStatusDTO() {
        OrderProductChangeStatusDTO orderProductChangeStatusDTO = new OrderProductChangeStatusDTO();
        List<OrderProductStatusDTO> productStatusDTOList = new ArrayList<>();
        OrderProductStatusDTO product1 = new OrderProductStatusDTO();
        OrderProductStatusDTO product2 = new OrderProductStatusDTO();
        product1.setOrderProductID(1L);
        product1.setOrderProductState(OrderProductState.PAID);
        product2.setOrderProductID(2L);
        product2.setOrderProductState(OrderProductState.PAID);
        productStatusDTOList.add(product1);
        productStatusDTOList.add(product2);
        orderProductChangeStatusDTO.setProductList(productStatusDTOList);
        return orderProductChangeStatusDTO;
    }

}
