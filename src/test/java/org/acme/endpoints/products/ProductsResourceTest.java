package org.acme.endpoints.products;

import io.quarkus.test.TestTransaction;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import io.quarkus.test.security.TestSecurity;
import io.restassured.http.ContentType;
import io.vertx.ext.web.handler.impl.HttpStatusException;
import org.acme.dto.products.ProductDTO;
import org.acme.services.products.ProductService;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import javax.ws.rs.core.Response;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.when;

@QuarkusTest
@TestTransaction
@TestSecurity(authorizationEnabled = false)
class ProductsResourceTest {

    @InjectMock
    ProductService productService;

    private static ProductDTO productDTO;

    @BeforeAll
    public static void setup() {
        productDTO = new ProductDTO();
        productDTO.setId(1L);
        productDTO.setCategoryID(1L);
        productDTO.setName("Test product name");
        productDTO.setDescription("Test product description");
        productDTO.setPrice(100);
        productDTO.setDeposit(100);
        productDTO.setInsurance(200);
    }

    @Test
    @DisplayName("Test 404 not found")
    void pageNotFoundProductsEndpointTest() {
        given()
                .when()
                .get("/produtcs")
                .then()
                .statusCode(404);
    }

    @Test
    @DisplayName("Get product by id")
    void getProductByIdTest() {
        when(productService.getById(1L)).thenReturn(productDTO);

        given()
                .when()
                .contentType(ContentType.JSON)
                .get("/products/1")
                .then()
                .body("id", equalTo(1))
                .body("categoryID", equalTo(1))
                .body("name", equalTo("Test product name"))
                .body("description", equalTo("Test product description"))
                .body("price", equalTo(100))
                .body("deposit", equalTo(100))
                .body("insurance", equalTo(200))
                .statusCode(Response.Status.OK.getStatusCode());
    }

    @Test
    @DisplayName("Delete proper product")
    void deleteProperProductTest() {
        when(productService.deleteProduct(1L)).thenReturn(true);

        given()
                .when()
                .delete("/products/1")
                .then()
                .statusCode(Response.Status.NO_CONTENT.getStatusCode());
    }

    @Test
    @DisplayName("Delete wrong product")
    void deleteWrongProductTest() {
        when(productService.deleteProduct(2L)).thenReturn(false);

        given()
                .when()
                .delete("/products/2")
                .then()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode());
    }

    @Test
    @DisplayName("Get not existing product")
    void getNotExistingProductTest() {
        when(productService.getById(1L)).thenThrow(new HttpStatusException(HttpStatus.SC_NOT_FOUND));

        given()
                .when()
                .get("/products/1")
                .then()
                .statusCode(Response.Status.NOT_FOUND.getStatusCode());
    }

}
