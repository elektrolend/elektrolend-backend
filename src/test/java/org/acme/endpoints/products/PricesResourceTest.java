package org.acme.endpoints.products;

import io.quarkus.test.TestTransaction;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import io.quarkus.test.security.TestSecurity;
import io.restassured.http.ContentType;
import org.acme.dto.products.PriceDTO;
import org.acme.services.products.PriceService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.testcontainers.shaded.com.google.common.net.HttpHeaders;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.when;

@QuarkusTest
@TestTransaction
@TestSecurity(authorizationEnabled = false)
class PricesResourceTest {

    @InjectMock
    PriceService priceService;

    private static PriceDTO priceDTO;
    private static final LocalDate today = LocalDate.now();

    @BeforeAll
    public static void setup() {
        priceDTO = new PriceDTO();
        priceDTO.setId(1L);
        priceDTO.setProductID(3L);
        priceDTO.setPricePerDayNetto(100);
        priceDTO.setPricePerDayBrutto(200);
        priceDTO.setBeginDate(today.plus(1, ChronoUnit.DAYS));
        priceDTO.setEndDate(today.plus(3, ChronoUnit.DAYS));
    }

    @Test
    @DisplayName("Test 404 not found")
    void pageNotFoundPricesEndpointTest() {
        given()
                .when()
                .get("/pirces")
                .then()
                .statusCode(Response.Status.NOT_FOUND.getStatusCode());
    }

    @Test
    @DisplayName("Post price")
    void postPriceTest() {
        when(priceService.addPrice(priceDTO)).thenReturn(priceDTO);

        given()
                .body(priceDTO)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/prices")
                .then()
                .statusCode(Response.Status.CREATED.getStatusCode())
                .header("location", "http://localhost:8081/prices/1");
    }

    @Test
    @DisplayName("Get all prices")
    void getCreatedPriceTest() {
        PriceDTO secondPriceDTO = new PriceDTO();
        secondPriceDTO.setId(2L);
        secondPriceDTO.setProductID(4L);
        secondPriceDTO.setPricePerDayNetto(1000);
        secondPriceDTO.setPricePerDayBrutto(2000);
        secondPriceDTO.setBeginDate(today.plus(5, ChronoUnit.DAYS));
        secondPriceDTO.setEndDate(today.plus(10, ChronoUnit.DAYS));

        when(priceService.getPrices()).thenReturn(List.of(priceDTO, secondPriceDTO));

        given()
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .contentType(ContentType.JSON)
                .when()
                .get("/prices")
                .then()
                .body("id", equalTo(List.of(1, 2)))
                .body("productID", equalTo(List.of(3, 4)))
                .body("pricePerDayNetto", equalTo(List.of(100, 1000)))
                .body("pricePerDayBrutto", equalTo(List.of(200, 2000)))
                //TODO don't know why not worikng
//                .body("beginDate", equalTo(List.of(
//                        today.plus(1, ChronoUnit.DAYS),
//                        today.plus(5, ChronoUnit.DAYS)
//                )))
//                .body("endDate", equalTo(List.of(
//                        today.plus(3, ChronoUnit.DAYS),
//                        today.plus(10, ChronoUnit.DAYS)
//                )))
                .statusCode(Response.Status.OK.getStatusCode());
    }

    @Test
    @DisplayName("Delete proper price")
    void deleteProperPriceTest() {
        when(priceService.deletePrice(1L)).thenReturn(true);

        given()
                .when()
                .delete("/prices/1")
                .then()
                .statusCode(Response.Status.NO_CONTENT.getStatusCode());
    }

    @Test
    @DisplayName("Delete wrong category")
    void deleteWrongPriceTest() {
        when(priceService.deletePrice(1L)).thenReturn(false);

        given()
                .when()
                .delete("/prices/1")
                .then()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode());

    }

}
