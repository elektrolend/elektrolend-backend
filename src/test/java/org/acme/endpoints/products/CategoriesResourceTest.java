package org.acme.endpoints.products;

import io.quarkus.test.TestTransaction;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import io.quarkus.test.security.TestSecurity;
import io.restassured.http.ContentType;
import io.vertx.ext.web.handler.impl.HttpStatusException;
import org.acme.dto.products.CategoryDTO;
import org.acme.services.products.CategoryService;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.testcontainers.shaded.com.google.common.net.HttpHeaders;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.when;

@QuarkusTest
@TestTransaction
@TestSecurity(authorizationEnabled = false)
class CategoriesResourceTest {

    @InjectMock
    CategoryService categoryService;

    private static CategoryDTO categoryDTO;

    @BeforeAll
    public static void setup() {
        categoryDTO = new CategoryDTO();
        categoryDTO.setId(1L);
        categoryDTO.setName("Test category name");
        categoryDTO.setDescription("Test category description");
    }

    @Test
    @DisplayName("Test 404 not found")
    void pageNotFoundCategoriesEndpointTest() {
        given()
                .when()
                .get("/categoryies")
                .then()
                .statusCode(Response.Status.NOT_FOUND.getStatusCode());
    }

    @Test
    @DisplayName("Post category")
    void postCategoryTest() {
        when(categoryService.addCategory(categoryDTO)).thenReturn(categoryDTO);

        given()
                .body(categoryDTO)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when()
                .post("/categories")
                .then()
                .statusCode(Response.Status.CREATED.getStatusCode())
                .header("location", "http://localhost:8081/categories/1");
    }

    @Test
    @DisplayName("Get all categories")
    void getAllCategoriesTest() {
        CategoryDTO secondPriceDTO = new CategoryDTO();
        secondPriceDTO.setId(2L);
        secondPriceDTO.setName("Test second category name");
        secondPriceDTO.setDescription("Test second category description");

        when(categoryService.getCategories()).thenReturn(List.of(categoryDTO, secondPriceDTO));

        given()
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .contentType(ContentType.JSON)
                .when()
                .get("/categories")
                .then()
                .body("id", equalTo(List.of(1, 2)))
                .body("name", equalTo(List.of("Test category name", "Test second category name")))
                .body("description", equalTo(List.of("Test category description", "Test second category description")))
                .statusCode(Response.Status.OK.getStatusCode());
    }

    @Test
    @DisplayName("Get one category by id")
    void getCategoryByIdTest() {
        when(categoryService.getById(1L)).thenReturn(categoryDTO);

        given()
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .contentType(ContentType.JSON)
                .when()
                .get("/categories/1")
                .then()
                .body("id", equalTo(1))
                .body("name", equalTo("Test category name"))
                .body("description", equalTo("Test category description"))
                .statusCode(Response.Status.OK.getStatusCode());
    }

    @Test
    @DisplayName("Delete proper category")
    void deleteProperCategoryTest() {
        when(categoryService.deleteCategory(1L)).thenReturn(true);

        given()
                .when()
                .delete("/categories/1")
                .then()
                .statusCode(Response.Status.NO_CONTENT.getStatusCode());
    }

    @Test
    @DisplayName("Delete wrong category")
    void deleteWrongCategoryTest() {
        when(categoryService.deleteCategory(1L)).thenReturn(false);

        given()
                .when()
                .delete("/categories/1")
                .then()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode());

    }

    @Test
    @DisplayName("Get not existing category")
    void getNotExistingCategoryTest() {
        when(categoryService.getById(1L)).thenThrow(new HttpStatusException(HttpStatus.SC_NOT_FOUND));

        given()
                .when()
                .get("/categories/1")
                .then()
                .statusCode(Response.Status.NOT_FOUND.getStatusCode());
    }

}
