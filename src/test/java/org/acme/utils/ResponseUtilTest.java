package org.acme.utils;

import org.junit.Assert;
import org.junit.Test;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class ResponseUtilTest {

    @Test
    public void createOkResponseWithOkMessage() {
        Response okResponse = ResponseUtil.createResponseWithMessage(Response.Status.OK, "Everything is good");
        Assert.assertEquals("Everything is good", okResponse.getEntity());
        Assert.assertEquals(Response.Status.OK, okResponse.getStatusInfo());
        Assert.assertEquals(200, okResponse.getStatus());
        Assert.assertEquals(MediaType.TEXT_PLAIN_TYPE, okResponse.getMediaType());
    }

}
