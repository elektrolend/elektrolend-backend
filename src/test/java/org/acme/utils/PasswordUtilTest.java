package org.acme.utils;

import org.acme.models.accounts.UserAccount;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

class PasswordUtilTest {

    @ParameterizedTest()
    @ValueSource(strings = {"no!upper!case", "Short", "ThisIsVeryLongPassword123!@#",
            "Pass123", "!@#$%^&*", "ONLYUPPER", "onlylower", "1234567", "A!a"})
    void isPasswordTooWeakPositiveTest(String password) {
        Assertions.assertTrue(PasswordUtil.isPasswordTooWeak(password));
    }

    @ParameterizedTest()
    @ValueSource(strings = {"Pass!@#", "!AAAbCCC@", "Abcd123!@#"})
    void isPasswordTooWeakNegativeTest(String password) {
        Assertions.assertFalse(PasswordUtil.isPasswordTooWeak(password));
    }

    @ParameterizedTest
    @MethodSource("createBoxPassword")
    @DisplayName("Correct box password")
    void notShouldChangeOrderProductState(String boxPassword) {
        Assertions.assertTrue(isUpperCase(boxPassword));
        Assertions.assertTrue(boxPassword.length() > 4);
        Assertions.assertTrue(boxPassword.length() < 13);
    }

    @Test
    void isTwoOtherSaltAndPasswordHash() {
        UserAccount userAccount1 = new UserAccount();
        UserAccount userAccount2 = new UserAccount();
        String password = "Password123!@#";

        PasswordUtil.hashPassword(userAccount1, password);
        PasswordUtil.hashPassword(userAccount2, password);

        Assertions.assertNotEquals(userAccount1.getPasswordHash(), userAccount2.getPasswordHash());
        Assertions.assertNotEquals(userAccount1.getSalt(), userAccount2.getSalt());
    }

    private static Stream<String> createBoxPassword() {
        return IntStream.range(1, 10)
                .mapToObj(i -> PasswordUtil.createBoxPassword())
                .collect(Collectors.toList())
                .stream();
    }

    private static boolean isUpperCase(String s) {
        for (int i = 0; i < s.length(); i++) {
            if (!Character.isUpperCase(s.charAt(i))) {
                return false;
            }
        }
        return true;
    }

}
