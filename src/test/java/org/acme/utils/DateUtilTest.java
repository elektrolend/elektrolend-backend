package org.acme.utils;

import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDate;

public class DateUtilTest {

    private static final LocalDate todayDate = LocalDate.now();

    private static final LocalDate endOfJanuary = LocalDate.of(2021, 1, 23);
    private static final LocalDate beginningOfFebruary = LocalDate.of(2021, 2, 10);
    private static final LocalDate endOfFebruary = LocalDate.of(2021, 2, 25);
    private static final LocalDate beginningOfMarch = LocalDate.of(2021, 3, 4);

    @Test
    public void correctChronologicalDatesTest() {
        Assert.assertTrue(DateUtil.checkIfDatesAreChronological(endOfJanuary, endOfFebruary));
    }

    @Test
    public void wrongNotChronologicalDatesTest() {
        Assert.assertFalse(DateUtil.checkIfDatesAreChronological(endOfFebruary, endOfJanuary));
    }

    @Test
    public void wrongIdenticalDatesTest() {
        Assert.assertFalse(DateUtil.checkIfDatesAreChronological(endOfJanuary, endOfJanuary));

    }

    @Test
    public void checkIfDatesAreEqualTest() {
        Assert.assertTrue(DateUtil.checkIfDatesAreChronologicalOrEqual(endOfJanuary, endOfJanuary));
    }

    @Test
    public void checkIfDatesAreChronologicalOrEqualTest() {
        Assert.assertTrue(DateUtil.checkIfDatesAreChronologicalOrEqual(endOfJanuary, endOfFebruary));
    }

    @Test
    public void notChronologicalDatesUsingCheckIfDatesAreChronologicalOrEqualTest() {
        Assert.assertFalse(DateUtil.checkIfDatesAreChronologicalOrEqual(endOfFebruary, endOfJanuary));
    }

    @Test
    public void isDateFromPastOrTodayWithTodayDateTest() {
        Assert.assertTrue(DateUtil.isDateFromPastOrToday(todayDate));
    }

    @Test
    public void isDateFromPastOrTodayWithPastDateTest() {
        LocalDate pastDate = todayDate.minusDays(1);
        Assert.assertTrue(DateUtil.isDateFromPastOrToday(pastDate));
    }

    @Test
    public void isDateFromPastOrTodayWithFutureDateTest() {
        LocalDate futureDate = todayDate.plusDays(1);
        Assert.assertFalse(DateUtil.isDateFromPastOrToday(futureDate));
    }

    @Test
    public void wrongSpansEndDateToCheckIsBeforeBeginDateToCheckTest() {
        Assert.assertFalse(DateUtil.timeSpansCorrect(
                beginningOfFebruary, endOfJanuary,
                endOfFebruary, beginningOfMarch)
        );
    }

    @Test
    public void wrongSpansBeginAndEndDateToCheckIsEqualTest() {
        Assert.assertFalse(DateUtil.timeSpansCorrect(
                endOfJanuary, endOfJanuary,
                endOfFebruary, beginningOfMarch)
        );
    }

    @Test
    public void correctSpansEndDateToCheckBeforeBeginFinalDateTest() {
        Assert.assertTrue(DateUtil.timeSpansCorrect(
                endOfJanuary, beginningOfFebruary,
                endOfFebruary, beginningOfMarch)
        );
    }

    @Test
    public void wrongSpansBeginFinalDateInsideDatesToCheckTest() {
        Assert.assertFalse(DateUtil.timeSpansCorrect(
                endOfJanuary, endOfFebruary,
                beginningOfFebruary, beginningOfMarch)
        );
    }

    @Test
    public void wrongSpansBeginDateToCheckInsideFinalDatesTest() {
        Assert.assertFalse(DateUtil.timeSpansCorrect(
                beginningOfFebruary, endOfFebruary,
                endOfJanuary, beginningOfMarch)
        );
    }

    @Test
    public void correctSpansBeginDateToCheckAfterEndFinalDateTest() {
        Assert.assertTrue(DateUtil.timeSpansCorrect(
                endOfFebruary, beginningOfMarch,
                endOfJanuary, beginningOfFebruary)
        );
    }

}
